package project.sabs.android.com.swarnaorganics.Search.SearchList;

public class SearchDataList {

    String name;
    String cropName;
    String location;
    String quantity;
    String mobile;

    public SearchDataList() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public SearchDataList(String name, String cropName, String location, String quantity, String mobile) {

        this.name = name;
        this.cropName = cropName;
        this.location = location;
        this.quantity = quantity;
        this.mobile = mobile;
    }
}
