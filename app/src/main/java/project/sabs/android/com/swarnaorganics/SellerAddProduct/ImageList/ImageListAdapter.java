package project.sabs.android.com.swarnaorganics.SellerAddProduct.ImageList;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.UserAddedProductImage;
import project.sabs.android.com.swarnaorganics.Volley.Constants;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class ImageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<UserAddedProductImage> modelList;
    public static ImageListAdapter objSearchListAdapter;
    // private ArrayList<UserAddedProductImage> addToCartModelList =new ArrayList<UserAddedProductImage>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;
    int pos=0;
    String Type;

    private OnItemClickListener mItemClickListener;


    public ImageListAdapter(Context context, ArrayList<UserAddedProductImage> modelList,String type) {
        this.mContext = context;
        this.modelList = modelList;
        this.modelList = modelList;
        this.gridview = gridview;
        this.Type = type;
        objSearchListAdapter = this;
    }


    public void updateList(ArrayList<UserAddedProductImage> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_image_itme, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final UserAddedProductImage model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;



            genericViewHolder.img_NewProduct.setImageBitmap(StringToBitMap(model.getByteArrayOfImage()));

          /*  if (Type.equalsIgnoreCase("FromServer")){
                String imgUrl =  Constants.IMAGE_URL+model.getImagePath();
                Picasso.with(mContext).load(imgUrl).into(genericViewHolder.img_NewProduct);
            }*/



           /* else {

                //genericViewHolder.img_NewProduct.setImageBitmap(model.getImage());

            }*/


//


            //setAnimation(holder.itemView, position);
            genericViewHolder.img_NewProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    pos = position;
                    dialog();
                }
            });  genericViewHolder.img_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    modelList.remove(position);
                    notifyDataSetChanged();
                }
            });

        }
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private UserAddedProductImage getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, UserAddedProductImage model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_NewProduct;
        private ImageView img_cancel;

        

        // @BindView(R.id.img_user)
        // ImageView imgUser;
        // @BindView(R.id.item_txt_title)
        // TextView itemTxtTitle;
        // @BindView(R.id.item_txt_message)
        // TextView itemTxtMessage;
        // @BindView(R.id.radio_list)
        // RadioButton itemTxtMessage;
        // @BindView(R.id.check_list)
        // CheckBox itemCheckList;
        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.img_NewProduct = (ImageView) itemView.findViewById(R.id.img_NewProduct);
            this.img_cancel = (ImageView) itemView.findViewById(R.id.img_cancel);

           



            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

    private void dialog(){

        final Dialog dialog = new Dialog(mContext);
        // Include dialog.xml file
        dialog.setContentView(R.layout.image_dialog);
        dialog.show();
        dialog.setCancelable(false);
        ImageView dialog_img=(ImageView) dialog.findViewById(R.id.dialog_img);
        ImageView cancel_img=(ImageView) dialog.findViewById(R.id.cancel_img);


        dialog_img.setImageBitmap(StringToBitMap(modelList.get(pos).getByteArrayOfImage()));
        cancel_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
    }

}

