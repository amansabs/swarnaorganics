package project.sabs.android.com.swarnaorganics.Search.SearchList;

import android.app.ProgressDialog;

import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Search.SearchList.BuyerSearchModel.BuyerSearchModel;
import project.sabs.android.com.swarnaorganics.Search.SearchList.BuyerSearchModel.SearchedProduct;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

public class SearchListActivity extends AppCompatActivity implements IApiResponse {
    private RecyclerView recyclerView;
    private SearchListAdapter mAdapter;
    public  ArrayList<SearchDataList> allUserList=new ArrayList<SearchDataList>();
           ArrayList<SearchedProduct> buyerSearchList =new ArrayList<SearchedProduct>();
    TextView text_toolbar;
    RelativeLayout rr_back;
    RelativeLayout rr_noRecord;
    ProgressDialog pd;
    Boolean isStopScrolling = false;
    Button btnGoToHome;
    LinearLayoutManager layoutManager;
    private boolean loading = true;
    int page = 1;
    RelativeLayout progressbarrel;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    String selectUser = "";
    String cropId = "";
    String subCropID = "";
    String countryID = "";
    String stateId = "";
    String districtId = "";
    String other = "";
    String orgtype = "";
    boolean nodataFound= false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_list);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        text_toolbar= (TextView)findViewById(R.id.text_toolbar);
        rr_back= (RelativeLayout) findViewById(R.id.rr_back);
        rr_noRecord= (RelativeLayout) findViewById(R.id.rr_noRecord);
        btnGoToHome= (Button) findViewById(R.id.btnGoToHome);
        progressbarrel = (RelativeLayout)findViewById(R.id.progressbarrel);

        pd = new ProgressDialog(SearchListActivity.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);

        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnGoToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
                /*Intent in = new Intent(SearchListActivity.this,SearchActivity.class);
                startActivity(in);
               // finish();*/

            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_list_anim);

        if( getIntent().getExtras() != null) {

             selectUser = getIntent().getExtras().getString("selectUser");
             cropId = getIntent().getExtras().getString("cropId");
             subCropID = getIntent().getExtras().getString("subCropID");
             countryID = getIntent().getExtras().getString("countryID");
             stateId = getIntent().getExtras().getString("stateId");
             districtId = getIntent().getExtras().getString("districtId");
            other = getIntent().getExtras().getString("other");
            orgtype = getIntent().getExtras().getString("orgtype");

            if (selectUser.equalsIgnoreCase("1")){
                text_toolbar.setText("Seller List");

            }
            else {
                text_toolbar.setText("Buyer List");

            }

            getSubCropData(selectUser,cropId,subCropID,countryID,stateId,districtId,other,orgtype);
            defaultMedthod();

        }



    }

     private void getSubCropData(String selectUser,String cropId,String subCropID,String countryID,String stateId,String districtId,String other,String orgtype) {
        HashMap<String, String> map = new HashMap<>();
        //pd.show();
        map.put("languageid","1");
        map.put("usertype",selectUser);
        map.put("crop",cropId);
        map.put("subcrop",subCropID);
        map.put("country",countryID);
        map.put("state",stateId);
        map.put("district",districtId);
        map.put("other",other);
        map.put("orgtype",orgtype);
        map.put("pageno", String.valueOf(page));
        map.put("noofrecordsperpage","10");

        ApiRequest apiRequest = new ApiRequest(SearchListActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.SEARCH_LIST, Constants.SEARCH_LIST,map, Request.Method.POST);
    }
    private void defaultMedthod(){

        //   product_list();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    // int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                    if (loading && !isStopScrolling)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            progressbarrel.setVisibility(View.VISIBLE);
                            page = page + 1 ;
                            loading = false;
                            getSubCropData(selectUser,cropId,subCropID,countryID,stateId,districtId,other,orgtype);
                            Log.v("...", "Last Item Wow !");
                            // Toast.makeText(AllProductListActivity.this, "pagination", Toast.LENGTH_SHORT).show();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

        mAdapter = new SearchListAdapter(SearchListActivity.this, buyerSearchList,selectUser);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);


    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if(tag_json_obj.equalsIgnoreCase(Constants.SEARCH_LIST)){

            if(response != null) {
                pd.dismiss();
                BuyerSearchModel finalArray = new Gson().fromJson(response, new TypeToken<BuyerSearchModel>() {}.getType());

                int code = finalArray.getCode();

                if (code == 200){
                    nodataFound = true;
                    buyerSearchList.addAll((ArrayList<SearchedProduct>) finalArray.getSearchedProduct());

                    pd.dismiss();
                    recyclerView.setVisibility(View.VISIBLE);
                    rr_noRecord.setVisibility(View.GONE);

                    if(buyerSearchList.size() > 0){
                        isStopScrolling = false;
                        loading = true;
                        mAdapter.notifyDataSetChanged();
                    }else{
                        isStopScrolling = true;
                        loading = false;
                    }
                    progressbarrel.setVisibility(View.GONE);

                }
                else {

                   /* rr_noRecord.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);*/
                   if (!nodataFound){
                       rr_noRecord.setVisibility(View.VISIBLE);
                       recyclerView.setVisibility(View.GONE);
                   }
                    progressbarrel.setVisibility(View.GONE);
                    Toast.makeText(this, ""+finalArray.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            else{
                
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
