package project.sabs.android.com.swarnaorganics.SellerProduct;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;

import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.BuyerAddProduct.BuyerProductListAdapter;
import project.sabs.android.com.swarnaorganics.BuyerAddProduct.ProductModel.BProductList;
import project.sabs.android.com.swarnaorganics.BuyerAddProduct.ProductModel.ProductListModel;
import project.sabs.android.com.swarnaorganics.Preferences;
import project.sabs.android.com.swarnaorganics.R;

import project.sabs.android.com.swarnaorganics.SellerAddProduct.SellerAddProductActivity;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

public class SellerProductListActivity extends AppCompatActivity implements IApiResponse,ProductListInterface {
    private RecyclerView selllerProduct_list_anim;
    private SellerProductListAdapter mAdapter;
    private BuyerProductListAdapter bAdapter;
    Button btnGoToHome;
    public ArrayList<BProductList> buyerUserList=new ArrayList<BProductList>();
    public ArrayList<project.sabs.android.com.swarnaorganics.SellerProduct.ProductList>allUserList =new ArrayList<project.sabs.android.com.swarnaorganics.SellerProduct.ProductList>();
    TextView text_toolbar;
    RelativeLayout rr_back;
    RelativeLayout rr_noRecord;
    String userID;
    String userType;
    ProgressDialog pd;
    public  static ProductListInterface productListInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_product_list);

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        userType =   Preferences.get(this,Preferences.USER_TYPE);

        text_toolbar= (TextView)findViewById(R.id.text_toolbar);
        btnGoToHome= (Button) findViewById(R.id.btnGoToHome);
        rr_back= (RelativeLayout) findViewById(R.id.rr_back);
        rr_noRecord= (RelativeLayout) findViewById(R.id.rr_noRecord);

        text_toolbar.setText("Product List");
        userID = Preferences.get(this,Constants.USER_ID);
        userType=Preferences.get(this,Preferences.USER_TYPE);
        pd = new ProgressDialog(SellerProductListActivity.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);

        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        selllerProduct_list_anim = (RecyclerView) findViewById(R.id.selllerProduct_list_anim);

        productListInterface = this;
        AddPRoduct();

        btnGoToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(SellerProductListActivity.this, SellerAddProductActivity.class);
                in.putExtra("UserKey",userType);
                startActivity(in);
                finish();


            }
        });


    }





    private void AddPRoduct() {

        HashMap<String, String> map = new HashMap<>();
        pd.show();
        map.put("languageid","1");
        map.put("usertype",userType);
        map.put("userid",userID);
        ApiRequest apiRequest = new ApiRequest(SellerProductListActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_PRODUCT, Constants.GET_PRODUCT,map, Request.Method.POST);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equalsIgnoreCase(Constants.GET_PRODUCT)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                if (userType.equalsIgnoreCase("1")){
                    SellerListModel stateArray = new Gson().fromJson(response, new TypeToken<SellerListModel>(){}.getType());
                    if (stateArray != null){

                        int code = stateArray.getCode();
                        String msg = stateArray.getMessage();
                        if (code == 200){
                            allUserList.clear();
                            allUserList = (ArrayList<project.sabs.android.com.swarnaorganics.SellerProduct.ProductList>) stateArray.getProductList();
                            mAdapter = new SellerProductListAdapter(SellerProductListActivity.this, allUserList,userType,userID);
                            selllerProduct_list_anim.setHasFixedSize(true);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                            selllerProduct_list_anim.setLayoutManager(layoutManager);
                            selllerProduct_list_anim.setAdapter(mAdapter);
                        }
                        else {
                            Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                            rr_noRecord.setVisibility(View.VISIBLE);
                            selllerProduct_list_anim.setVisibility(View.GONE);
                        }
                    }
                    else {

                        rr_noRecord.setVisibility(View.VISIBLE);
                        selllerProduct_list_anim.setVisibility(View.GONE);
                    }

                }

                else if (userType.equalsIgnoreCase("2")){

                    ProductListModel bstateArray = new Gson().fromJson(response, new TypeToken<ProductListModel>(){}.getType());

                    if (bstateArray != null){
                        int code = bstateArray.getCode();
                        String msg = bstateArray.getMessage();
                        if (code == 200){
                            buyerUserList.clear();
                            buyerUserList = (ArrayList<BProductList>) bstateArray.getProductList();
                            bAdapter = new BuyerProductListAdapter(SellerProductListActivity.this, buyerUserList,userType,userID);
                            selllerProduct_list_anim.setHasFixedSize(true);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                            selllerProduct_list_anim.setLayoutManager(layoutManager);
                            selllerProduct_list_anim.setAdapter(bAdapter);
                        }

                        else {
                            Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                            rr_noRecord.setVisibility(View.VISIBLE);
                            selllerProduct_list_anim.setVisibility(View.GONE);
                        }

                    }
                    else {
                        rr_noRecord.setVisibility(View.VISIBLE);
                        selllerProduct_list_anim.setVisibility(View.GONE);
                    }
                }
            }
            else {

                rr_noRecord.setVisibility(View.VISIBLE);
                selllerProduct_list_anim.setVisibility(View.GONE);

            }

        }
        else {
            Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void refreshActivity() {
        AddPRoduct();
    }
}
