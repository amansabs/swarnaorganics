
package project.sabs.android.com.swarnaorganics.SellerProduct;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductList {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("CropMasterId")
    @Expose
    private String cropMasterId;
    @SerializedName("CropMasterName")
    @Expose
    private String cropMasterName;
    @SerializedName("Subcrop")
    @Expose
    private String subcrop;
    @SerializedName("UnitPrice")
    @Expose
    private String unitPrice;
    @SerializedName("Unit")
    @Expose
    private String unit;
    @SerializedName("IsOrganic")
    @Expose
    private String isOrganic;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("Certificate")
    @Expose
    private String certificate;
    @SerializedName("Other")
    @Expose
    private String other;
    @SerializedName("ImagePath")
    @Expose
    private Object imagePath;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCropMasterId() {
        return cropMasterId;
    }

    public void setCropMasterId(String cropMasterId) {
        this.cropMasterId = cropMasterId;
    }

    public String getCropMasterName() {
        return cropMasterName;
    }

    public void setCropMasterName(String cropMasterName) {
        this.cropMasterName = cropMasterName;
    }

    public String getSubcrop() {
        return subcrop;
    }

    public void setSubcrop(String subcrop) {
        this.subcrop = subcrop;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIsOrganic() {
        return isOrganic;
    }

    public void setIsOrganic(String isOrganic) {
        this.isOrganic = isOrganic;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Object getImagePath() {
        return imagePath;
    }

    public void setImagePath(Object imagePath) {
        this.imagePath = imagePath;
    }

}
