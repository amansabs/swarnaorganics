package project.sabs.android.com.swarnaorganics.BuyerAddProduct;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.BuyerAddProduct.EditModel.EditProductModel;
import project.sabs.android.com.swarnaorganics.Preferences;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Search.SubCropData;
import project.sabs.android.com.swarnaorganics.Search.SubCropModel.CropDetail;
import project.sabs.android.com.swarnaorganics.Search.SubCropModel.GetSubCropModel;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.AddProductActivity;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.AddProductModel;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.ImageList.ImageListAdapter;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.ImageList.InserImageDataModel;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.UnitDataModel;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.UnitModel.GetUnitModel;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.UnitModel.Unit;
import project.sabs.android.com.swarnaorganics.SellerProduct.SellerProductListActivity;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;
import project.sabs.android.com.swarnaorganics.Volley.MultipartRequest;

public class BuyerAddProductActivity extends AppCompatActivity implements IApiResponse {

    TextView text_toolbar;
    RelativeLayout rr_back;
    Spinner spinnerSubCrop ;
    ///subCrop
    final ArrayList<CropDetail> subCrop_nameList=new ArrayList<CropDetail>();
    ArrayAdapter<CropDetail> SubCropDataAdapter=null;
    ArrayList<CropDetail> newSubCrop_nameList=new ArrayList<CropDetail>();
    String subCropID="";
    ProgressDialog pd;
    Spinner spinnerUnit ;
    private RadioGroup OrganicRadioGroup;
    //image from camera
    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    //Unit
    final ArrayList<Unit> unit_nameList=new ArrayList<Unit>();
    ArrayList<Unit> newUnit_nameList=new ArrayList<Unit>();
    ArrayAdapter<Unit> unitDataAdapter=null;
    String UnitId = "";
    //new Image
    private RecyclerView imageRecyclerView;
    private ImageListAdapter mAdapter;
    public ArrayList<InserImageDataModel> imageList=new ArrayList<InserImageDataModel>();
    RelativeLayout rr_addImage,rr_otherCrop;
    CardView card_AddProduct;
    String CropID="";
    String ProductID="";
    String IsOrganic = "";
    String otherCropName ="";
    RadioButton organicYes,noOrganic;
    EditText et_quantity,et_price,et_comments,et_OtherCropName;
    CheckBox chkIos;
    String userID;
    String userType;
    String productQuentity ="";
    String productPrice = "";
    String productComments = "";
    String showPrice ="";
    boolean isEdit = false;
    String cropDetailID ;
    String Unit;
    TextView next_txt_id ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_add_product);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        et_quantity= (EditText) findViewById(R.id.et_quantity);
        et_price= (EditText) findViewById(R.id.et_price);
        et_comments= (EditText) findViewById(R.id.et_comments);
        et_OtherCropName= (EditText) findViewById(R.id.et_OtherCropName);
        chkIos= (CheckBox) findViewById(R.id.chkIos);
        text_toolbar= (TextView)findViewById(R.id.text_toolbar);
        next_txt_id= (TextView)findViewById(R.id.next_txt_id);
        rr_back= (RelativeLayout) findViewById(R.id.rr_back);
        spinnerUnit = (Spinner)findViewById(R.id.spinnerUnit);
        imageRecyclerView = (RecyclerView) findViewById(R.id.imageRecyclerView);
        rr_addImage = (RelativeLayout) findViewById(R.id.rr_addImage);
        rr_otherCrop = (RelativeLayout) findViewById(R.id.rr_otherCrop);
        card_AddProduct = (CardView) findViewById(R.id.card_AddProduct);
        pd = new ProgressDialog(BuyerAddProductActivity.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);
        OrganicRadioGroup=(RadioGroup)findViewById(R.id.OrganicRadioGroup);
        organicYes = OrganicRadioGroup.findViewById(R.id.organicYes);
        noOrganic = OrganicRadioGroup.findViewById(R.id.noOrganic);
        spinnerSubCrop = (Spinner)findViewById(R.id.spinnerSubCrop);
        userID = Preferences.get(this,Constants.USER_ID);
        userType=Preferences.get(this,Preferences.USER_TYPE);
        if (getIntent().getExtras() != null) {
            isEdit = (getIntent().getExtras().getBoolean("isEdit"));
        }
        if (isEdit){
            next_txt_id.setText("Update Product");
            text_toolbar.setText("Update Product");
            if (getIntent().getExtras() != null) {

                CropID = (getIntent().getExtras().getString("CropID"));
                ProductID = (getIntent().getExtras().getString("productID"));

            }
            editProduct(ProductID);
        }
        else {
            next_txt_id.setText("Add Product");
            text_toolbar.setText("Add New Product");
            if (organicYes.isChecked()){

                IsOrganic = "1";
            }
            else {
                IsOrganic = "0";

            }
            if (getIntent().getExtras() != null) {
                CropID = (getIntent().getExtras().getString("CropID"));
            }

            subCropSelect();
            unitSelect();
        }
        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        //Oragnic yes or No
        OrganicRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                switch (checkedId) {
                    case R.id.organicYes:
                        IsOrganic = "1";
                        break;
                    case R.id.noOrganic:
                        IsOrganic = "0";
                        break;
                }
            }
        });

        rr_addImage.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (imageList.size()<=2){
                    selectImage();
                }
                else {
                    Toast.makeText(BuyerAddProductActivity.this, "You Can select only 3 photos..", Toast.LENGTH_SHORT).show();
                }
            }
        });
      //  insertImage();
        card_AddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                validation ();
                
                /*Intent in = new Intent(BuyerAddProductActivity.this, SellerProductListActivity.class);
                startActivity(in);*/
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    private void validation (){
        productQuentity =et_quantity.getText().toString();
        productComments =et_comments.getText().toString();
        otherCropName =et_OtherCropName.getText().toString();
        if (subCropID.equalsIgnoreCase("") || subCropID.equalsIgnoreCase("0")){
            Toast.makeText(this, "please select sub crop", Toast.LENGTH_SHORT).show();
        }
        else if (IsOrganic.equalsIgnoreCase("")){
            Toast.makeText(this, "please check organic", Toast.LENGTH_SHORT).show();
        }
        else if (productQuentity.equalsIgnoreCase("")){
            Toast.makeText(this, "please enter quantity", Toast.LENGTH_SHORT).show();
        } else if (UnitId.equalsIgnoreCase("") || UnitId.equalsIgnoreCase("0")) {
            Toast.makeText(this, "please select unit name", Toast.LENGTH_SHORT).show();
        }
        else{

            if (isEdit){
                updateProduct();
            }
            else {
                AddPRoduct();
            }
        }
        
    }
    private void subCropSelect(){
        subCrop_nameList.add(new CropDetail("0", "Select"));
        SubCropDataAdapter = new ArrayAdapter<CropDetail>(this,android.R.layout.simple_spinner_item, subCrop_nameList);
        SubCropDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSubCrop.setAdapter(SubCropDataAdapter);
        getSubCropData();
    }
    private void unitSelect(){
        unit_nameList.add(new Unit("0", "Select"));
        unitDataAdapter = new ArrayAdapter<Unit>(this,android.R.layout.simple_spinner_item, unit_nameList);
        unitDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUnit.setAdapter(unitDataAdapter);
        getUnitData();

    }
    public void insertImage(){
       /* mAdapter = new UserI(BuyerAddProductActivity.this, imageList);
        imageRecyclerView.setHasFixedSize(true);
        imageRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        imageRecyclerView.setAdapter(mAdapter);*/
    }
    // Select image from camera and gallery
    private void selectImage() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
               AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                        
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                Log.e("Activity", "Pick from Camera::>>> ");
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                destination = new File(Environment.getExternalStorageDirectory() + "/" +
                        getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                imgPath = destination.getAbsolutePath();
                imageList.add(new InserImageDataModel(bitmap));
                mAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            Uri selectedImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                Log.e("Activity", "Pick from Gallery::>>> ");

                imageList.add(new InserImageDataModel(bitmap));
                mAdapter.notifyDataSetChanged();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    private void getSubCropData() {
        HashMap<String, String> map = new HashMap<>();
        pd.show();
        map.put("languageid","1");
        map.put("cropmasterid",CropID);
        ApiRequest apiRequest = new ApiRequest(BuyerAddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_SUB_CROP_LIST, Constants.GET_SUB_CROP_LIST,map, Request.Method.POST);
    }
    private void getUnitData() {
        HashMap<String, String> map = new HashMap<>();
        pd.show();
        map.put("languageid","1");
        ApiRequest apiRequest = new ApiRequest(BuyerAddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_UNIT, Constants.GET_UNIT,map, Request.Method.POST);
    }

    private void AddPRoduct() {
        HashMap<String, String> map = new HashMap<>();
          pd.show();
        map.put("usertype",userType);
        map.put("userid",userID);
        map.put("cropdetailid",subCropID);
        map.put("organic",IsOrganic);
        map.put("registeredwith","");
        map.put("otherorganizationname","");
        map.put("registrationno","");
        map.put("quantity",productQuentity);
        map.put("unit",UnitId);
        map.put("price","");
        map.put("showprice","");
        map.put("wantustocontact","");
        map.put("other",otherCropName);
        map.put("comments",productComments);
        ApiRequest apiRequest = new ApiRequest(BuyerAddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.ADD_PRODUCT, Constants.ADD_PRODUCT,map, Request.Method.POST);
    }

    private void updateProduct() {
        HashMap<String, String> map = new HashMap<>();
          pd.show();
        map.put("usertype",userType);
        map.put("productid",ProductID);
        map.put("cropdetailid",subCropID);
        map.put("organic",IsOrganic);
        map.put("registeredwith","");
        map.put("otherorganizationname","");
        map.put("registrationno","");
        map.put("quantity",productQuentity);
        map.put("unit",UnitId);
        map.put("price","");
        map.put("showprice","");
        map.put("wantustocontact","");
        map.put("other",otherCropName);
        map.put("comments",productComments);
        ApiRequest apiRequest = new ApiRequest(BuyerAddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.UPDATE_PRODUCT, Constants.UPDATE_PRODUCT,map, Request.Method.POST);
    }



    private void editProduct(String ProductID) {
        HashMap<String, String> map = new HashMap<>();
        pd.show();
        map.put("usertype",userType);
        map.put("productid",ProductID);
        final Map<String, MultipartRequest.DataPart> params = new HashMap<>();
        ApiRequest apiRequest = new ApiRequest(BuyerAddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.EDIT_PRODUCT, Constants.EDIT_PRODUCT,map, Request.Method.POST);
    }


    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        //Sub Crop
        if (tag_json_obj.equalsIgnoreCase(Constants.GET_SUB_CROP_LIST)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                GetSubCropModel stateArray = new Gson().fromJson(response, new TypeToken<GetSubCropModel>(){}.getType());

                if (stateArray != null){
                    newSubCrop_nameList = (ArrayList<CropDetail>) stateArray.getCropDetails();

                    for (int i = 0; i <newSubCrop_nameList.size() ; i++) {
                        subCrop_nameList.add(new CropDetail(""+newSubCrop_nameList.get(i).getCropdetailId(),
                                ""+newSubCrop_nameList.get(i).getCropdetailName()));
                    }

                    SubCropDataAdapter.notifyDataSetChanged();
               if(isEdit) {
                    for (int i = 0; i <subCrop_nameList.size() ; i++) {
                        String sName = subCrop_nameList.get(i).getCropdetailId();
                        if (sName.equalsIgnoreCase(cropDetailID)) {
                            //   stateId = state_name.get(i).getState_id();
                            spinnerSubCrop.setSelection(i);
                        }
                    }
                }
                    spinnerSubCrop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                            Object state_item = parent.getItemAtPosition(pos);
                            subCropID = "";

                            subCropID= subCrop_nameList.get(pos).getCropdetailId();

                            if (CropID.equalsIgnoreCase("10")){

                                if (subCropID.equalsIgnoreCase("51")){
                                    rr_otherCrop.setVisibility(View.VISIBLE);
                                }
                                else {
                                    rr_otherCrop.setVisibility(View.GONE);
                                }
                            }
                            else {
                                rr_otherCrop.setVisibility(View.GONE);
                            }



                            if(subCropID != null && !subCropID.equalsIgnoreCase("0") && !subCropID.equalsIgnoreCase("")) {
                           /* district_name.clear();
                            district_name.add(new CropDetail("0", "Select"));
                            cityDataAdapter.notifyDataSetChanged();
                            getDistrictListData();
                            districtSpinner.setEnabled(true);*/
                            }
                        }
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
                else {
                    Toast.makeText(this, "No sub crop found", Toast.LENGTH_SHORT).show();
                }

            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_UNIT)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                GetUnitModel stateArray = new Gson().fromJson(response, new TypeToken<GetUnitModel>(){}.getType());
                newUnit_nameList = (ArrayList<Unit>) stateArray.getUnit();
                for (int i = 0; i <newUnit_nameList.size() ; i++) {
                    unit_nameList.add(new Unit(""+newUnit_nameList.get(i).getUnitId(),
                            ""+newUnit_nameList.get(i).getUnitName()));
                }
                unitDataAdapter.notifyDataSetChanged();
                if(isEdit) {
                    for (int i = 0; i <unit_nameList.size() ; i++) {
                        String sName = unit_nameList.get(i).getUnitId();
                        if (sName.equalsIgnoreCase(Unit)) {
                            //   stateId = state_name.get(i).getState_id();
                            spinnerUnit.setSelection(i);
                        }
                    }
                }
                spinnerUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        Object state_item = parent.getItemAtPosition(pos);
                        UnitId = "";

                        UnitId= unit_nameList.get(pos).getUnitId();
                        if(UnitId != null && !UnitId.equalsIgnoreCase("0") && !UnitId.equalsIgnoreCase("")) {
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        else if (tag_json_obj.equalsIgnoreCase(Constants.ADD_PRODUCT)){
            // State list api
            if(response != null) {
                pd.dismiss();
                AddProductModel stateArray = new Gson().fromJson(response, new TypeToken<AddProductModel>(){}.getType());


                int code = stateArray.getCode();
                String msg = stateArray.getMessage();
                if (code == 200){
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                    Intent in = new Intent(BuyerAddProductActivity.this,SellerProductListActivity.class);
                    startActivity(in);
                    finish();
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                }

            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
        else if (tag_json_obj.equalsIgnoreCase(Constants.UPDATE_PRODUCT)){
            // State list api
            if(response != null) {
                pd.dismiss();
                AddProductModel stateArray = new Gson().fromJson(response, new TypeToken<AddProductModel>(){}.getType());
                int code = stateArray.getCode();
                String msg = stateArray.getMessage();
                if (code == 200){

                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                    Intent in = new Intent(BuyerAddProductActivity.this,SellerProductListActivity.class);
                    startActivity(in);
                    finish();
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                }

            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
         else if (tag_json_obj.equalsIgnoreCase(Constants.EDIT_PRODUCT)){
            // State list api
            if(response != null) {
                pd.dismiss();
                EditProductModel stateArray = new Gson().fromJson(response, new TypeToken<EditProductModel>(){}.getType());
                int code = stateArray.getCode();
                String msg = stateArray.getMessage();
                if (code == 200){

                    cropDetailID = stateArray.getUserAddedProduct().get(0).getCropDetailId();
                    String IsOrganic = stateArray.getUserAddedProduct().get(0).getIsOrganic();
                    String Quantity = stateArray.getUserAddedProduct().get(0).getQuantity();
                    Unit = stateArray.getUserAddedProduct().get(0).getUnit();
                    String other = (String)stateArray.getUserAddedProduct().get(0).getOther();
                    String Comments = (String) stateArray.getUserAddedProduct().get(0).getComments();
                    subCropID = cropDetailID;
                    UnitId = Unit;

                    if (IsOrganic.equalsIgnoreCase("1")){

                        this.IsOrganic = "1";
                        organicYes.setChecked(true);
                    }
                    else if (IsOrganic.equalsIgnoreCase("0")){
                        this.IsOrganic = "0";
                        noOrganic.setChecked(true);
                    }

                    et_quantity.setText(Quantity);

                    if (Comments!=null){

                        et_comments.setText(Comments);

                    }
                   if (other!=null){
                        rr_otherCrop.setVisibility(View.VISIBLE);
                        et_OtherCropName.setText(other);
                    }
                    subCropSelect();
                    unitSelect();

                }
                else {
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                }

            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }



    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
