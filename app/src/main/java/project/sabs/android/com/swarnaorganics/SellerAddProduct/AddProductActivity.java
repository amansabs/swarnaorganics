package project.sabs.android.com.swarnaorganics.SellerAddProduct;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.Preferences;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Search.SubCropModel.CropDetail;
import project.sabs.android.com.swarnaorganics.Search.SubCropModel.GetSubCropModel;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.ImageList.ImageListAdapter;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.SellerEditModel.SellerEditModel;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.UnitModel.GetUnitModel;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.UnitModel.Unit;
import project.sabs.android.com.swarnaorganics.SellerProduct.SellerProductListActivity;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;
import project.sabs.android.com.swarnaorganics.Volley.MultipartRequest;

public class AddProductActivity extends AppCompatActivity implements IApiResponse{

    TextView text_toolbar;
    RelativeLayout rr_back;
    Spinner spinnerSubCrop ;
    Spinner spinnerUnit ;
    RelativeLayout rr_registerWith;
    RelativeLayout add_registrationNumber_layout;
    RelativeLayout rr_registredOrganic;
    RelativeLayout add_Otherorganization_layout;
    CardView card_AddProduct;


    ///subCrop
    final ArrayList<CropDetail> subCrop_nameList=new ArrayList<CropDetail>();
    ArrayAdapter<CropDetail> SubCropDataAdapter=null;
    ArrayList<CropDetail> newSubCrop_nameList=new ArrayList<CropDetail>();
    String subCropID="";
    ProgressDialog pd;

    //Unit
    final ArrayList<Unit> unit_nameList=new ArrayList<Unit>();
    ArrayList<Unit> newUnit_nameList=new ArrayList<Unit>();
    ArrayList<UserAddedProductImage> imageProductList=new ArrayList<UserAddedProductImage>();
    ArrayAdapter<Unit> unitDataAdapter=null;
    String UnitId = "";
    CardView card_Search;
    //Organic RadioGroup
    private RadioGroup OrganicRadioGroup;
    // private RadioButton radioOrganicButton;
    //RegisterOrganic RadioGroup
    private RadioGroup RegisterOrganicRadioGroup;
    //private RadioButton RegisterOrganicRadioButton;
    //REgisterWith
    private RadioGroup RegisterWithRadioGroup;
    private RadioButton organicYes,noOrganic;
    boolean isSelfCertified = false;
    boolean isOtherSelected = false;

    //new Image
    private RecyclerView imageRecyclerView;
    private ImageListAdapter mAdapter;
    public ArrayList<UserAddedProductImage> imageList=new ArrayList<UserAddedProductImage>();
    RelativeLayout rr_addImage,rr_otherCrop;

    //insertImage
    //image from camera
    private Bitmap certi_bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private String CropID = "";
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;



    boolean isCertificate = false;

    RadioButton regGovt,regPgs,regSelf,regOther;
    RadioButton registerYes,registerNo;
    EditText et_Otherorganization,et_registrationNumber,et_quantity,et_price,et_comments,et_OtherCropName;
    String OrganicID = "";
    String RegisterWithID = "";
    String ContactToRegID = "";
    CheckBox chkIos;

    String OtherOrgName= "";
    String RegistrationNum = "";
    String userID;
    String userType;

    String priceShow = "";
    String OthrRegName ="";
    String RegN ="";
    String qty ="";
    String price="";
    String comments ="";
    String otherCropName ="";
    RelativeLayout rr_RegistrationImage;
    ImageView img_certificate;
    ImageView cross_img;
    TextView next_txt_id;

    boolean isEdit = false;

    String ProductID="";

    //Edit
    String cropDetailID;
    String Unit;



    String ImageOne= "";
    String ImageTwo="";
    String ImageThree="";
    String imageSize ="";
    String img  = "";

    private static final int MY_CAMERA_REQUEST_CODE = 100;


    boolean certificateClick = false;

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }


        pd = new ProgressDialog(AddProductActivity.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);

        et_Otherorganization= (EditText) findViewById(R.id.et_Otherorganization);
        et_registrationNumber= (EditText) findViewById(R.id.et_registrationNumber);
        et_quantity= (EditText) findViewById(R.id.et_quantity);
        et_price= (EditText) findViewById(R.id.et_price);
        et_comments= (EditText) findViewById(R.id.et_comments);
        et_OtherCropName= (EditText) findViewById(R.id.et_OtherCropName);
        next_txt_id= (TextView) findViewById(R.id.next_txt_id);

        rr_RegistrationImage= (RelativeLayout) findViewById(R.id.rr_RegistrationImage);
        img_certificate= (ImageView) findViewById(R.id.img_certificate);
        cross_img= (ImageView) findViewById(R.id.cross_img);
        chkIos= (CheckBox) findViewById(R.id.chkIos);
        text_toolbar= (TextView)findViewById(R.id.text_toolbar);
        rr_back= (RelativeLayout) findViewById(R.id.rr_back);
        rr_registerWith= (RelativeLayout) findViewById(R.id.rr_registerWith);
        add_registrationNumber_layout= (RelativeLayout) findViewById(R.id.add_registrationNumber_layout);
        rr_otherCrop= (RelativeLayout) findViewById(R.id.rr_otherCrop);
        rr_registredOrganic= (RelativeLayout) findViewById(R.id.rr_registredOrganic);
        add_Otherorganization_layout= (RelativeLayout) findViewById(R.id.add_Otherorganization_layout);
        spinnerSubCrop = (Spinner)findViewById(R.id.spinnerSubCrop);
        spinnerUnit = (Spinner)findViewById(R.id.spinnerUnit);
        card_AddProduct = (CardView) findViewById(R.id.card_AddProduct);
        imageRecyclerView = (RecyclerView) findViewById(R.id.imageRecyclerView);
        rr_addImage = (RelativeLayout) findViewById(R.id.rr_addImage);
        OrganicRadioGroup=(RadioGroup)findViewById(R.id.OrganicRadioGroup);
        RegisterOrganicRadioGroup=(RadioGroup)findViewById(R.id.RegisterOrganicRadioGroup);
        RegisterWithRadioGroup=(RadioGroup)findViewById(R.id.RegisterWithRadioGroup);



        organicYes = (RadioButton) OrganicRadioGroup.findViewById(R.id.organicYes);
        noOrganic = (RadioButton) OrganicRadioGroup.findViewById(R.id.noOrganic);

        regGovt = (RadioButton) RegisterWithRadioGroup.findViewById(R.id.regGovt);
        regPgs = (RadioButton) RegisterWithRadioGroup.findViewById(R.id.regPgs);
        regSelf = (RadioButton) RegisterWithRadioGroup.findViewById(R.id.regSelf);
        regOther = (RadioButton) RegisterWithRadioGroup.findViewById(R.id.regOther);

        registerYes = (RadioButton) RegisterOrganicRadioGroup.findViewById(R.id.registerYes);
        registerNo = (RadioButton) RegisterOrganicRadioGroup.findViewById(R.id.registerNo);


        userID = Preferences.get(this,Constants.USER_ID);
        userType=Preferences.get(this,Preferences.USER_TYPE);
        if (getIntent().getExtras() != null) {
            isEdit = (getIntent().getExtras().getBoolean("isEdit"));
        }
        //Oragnic yes or No

        if (isEdit){
            next_txt_id.setText("Update Product");
            text_toolbar.setText("Update Product Detail");
            if (getIntent().getExtras() != null) {
                CropID = (getIntent().getExtras().getString("CropID"));
                ProductID = (getIntent().getExtras().getString("productID"));

                ProductImage(ProductID);
            }

            editProduct(ProductID);

        }
        else {
            next_txt_id.setText("Add Product");
            text_toolbar.setText("Add Product Detail");
            if (getIntent().getExtras() != null) {
                CropID = (getIntent().getExtras().getString("CropID"));
            }
            subCropSelect();
            unitSelect();
            //getSubCropData();

        }




        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        }


        OrganicRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                switch (checkedId) {
                    case R.id.organicYes:

                        OrganicID="1";
                        ContactToRegID = "";
                        RegisterWithID = "1";
                        rr_registerWith.setVisibility(View.VISIBLE);
                        // add_registrationNumber_layout.setVisibility(View.VISIBLE);
                        rr_registredOrganic.setVisibility(View.GONE);
                        add_Otherorganization_layout.setVisibility(View.GONE);
                        regGovt.setChecked(true);
                        //  registerYes.setChecked(false);
                        if (isSelfCertified){
                            add_registrationNumber_layout.setVisibility(View.GONE);
                            add_Otherorganization_layout.setVisibility(View.GONE);
                        }
                        else {
                            add_registrationNumber_layout.setVisibility(View.VISIBLE);
                        }


                        if (isOtherSelected){
                            add_registrationNumber_layout.setVisibility(View.VISIBLE);
                            add_Otherorganization_layout.setVisibility(View.VISIBLE);
                        }

                        break;

                    case R.id.noOrganic:

                        OrganicID="0";
                        // regGovt.setChecked(false);
                        registerYes.setChecked(true);
                        RegisterWithID = "";
                        ContactToRegID = "1";
                        rr_registerWith.setVisibility(View.GONE);
                        add_registrationNumber_layout.setVisibility(View.GONE);
                        add_Otherorganization_layout.setVisibility(View.GONE);
                        OtherOrgName="";
                        RegistrationNum="";
                        et_registrationNumber.getText().clear();
                        et_Otherorganization.getText().clear();
                        rr_registredOrganic.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });


        //Register Oragnic yes or No
        RegisterOrganicRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                switch (checkedId) {

                    case R.id.registerYes:

                        ContactToRegID = "1";
                        break;
                    case R.id.registerNo:
                        ContactToRegID = "0";
                        break;
                }
            }
        });


        //Register Oragnic yes or No
        RegisterWithRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                switch (checkedId) {
                    case R.id.regGovt:
                        RegisterWithID = "1";
                        add_registrationNumber_layout.setVisibility(View.VISIBLE);
                        add_Otherorganization_layout.setVisibility(View.GONE);
                        et_Otherorganization.getText().clear();
                        OtherOrgName="";
                        isSelfCertified = false;
                        isOtherSelected = false;
                        break;

                    case R.id.regPgs:
                        RegisterWithID = "2";
                        add_registrationNumber_layout.setVisibility(View.VISIBLE);
                        add_Otherorganization_layout.setVisibility(View.GONE);
                        et_Otherorganization.getText().clear();
                        OtherOrgName="";
                        isOtherSelected = false;
                        isSelfCertified = false;
                        break;

                    case R.id.regSelf:
                        isSelfCertified = true;
                        isOtherSelected = false;
                        RegisterWithID = "3";
                        OtherOrgName="";
                        add_registrationNumber_layout.setVisibility(View.GONE);
                        add_Otherorganization_layout.setVisibility(View.GONE);
                        et_Otherorganization.getText().clear();

                        RegistrationNum="";
                        et_registrationNumber.getText().clear();
                        et_Otherorganization.getText().clear();
                        //rr_registredOrganic.setVisibility(View.GONE);
                        break;

                    case R.id.regOther:

                        RegisterWithID = "4";
                        add_Otherorganization_layout.setVisibility(View.VISIBLE);
                        add_registrationNumber_layout.setVisibility(View.VISIBLE);
                        et_Otherorganization.getText().clear();
                        OtherOrgName ="";
                        isOtherSelected = true;
                        isSelfCertified = false;
                        break;
                }
            }
        });


        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        rr_RegistrationImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isCertificate = true;
                selectImage();


            }
        });




        insertImage();





        card_AddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validation();
            }
        });

        rr_addImage.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {

                if (imageProductList.size()<=2){
                    selectImage();
                    isCertificate = false;
                }
                else {
                    Toast.makeText(AddProductActivity.this, "You Can select only 3 photos..", Toast.LENGTH_SHORT).show();
                }


            }
        });

        cross_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                certi_bitmap = null;
                cross_img.setVisibility(View.GONE);
                img_certificate.setVisibility(View.GONE);
            }
        });


    }


    public void Validation(){
        OthrRegName = et_Otherorganization.getText().toString();
        RegN = et_registrationNumber.getText().toString();
        qty = et_quantity.getText().toString();
        price = et_price.getText().toString();
        comments =et_comments.getText().toString();
        comments =et_comments.getText().toString();
        otherCropName =et_OtherCropName.getText().toString();

        if (chkIos.isChecked()){
            priceShow = "1";
        }
        else{
            priceShow = "0";
        }

        if (subCropID.equalsIgnoreCase("")|| subCropID.equalsIgnoreCase("0")){
            Toast.makeText(this, "please select sub crop", Toast.LENGTH_SHORT).show();
        }

        else if (OrganicID.equalsIgnoreCase("")){
            Toast.makeText(this, "please check organic", Toast.LENGTH_SHORT).show();
        }

        else if (add_Otherorganization_layout.getVisibility() == View.VISIBLE){

            if (OthrRegName.equalsIgnoreCase("")){
                Toast.makeText(this, "please enter organization name", Toast.LENGTH_SHORT).show();
            }
            else if (RegN.equalsIgnoreCase("")){
                Toast.makeText(this, "please enter registration number", Toast.LENGTH_SHORT).show();
            }
            else{

                ValidatioPart();

            }
        }

        else if (add_registrationNumber_layout.getVisibility() == View.VISIBLE){

            if (RegN.equalsIgnoreCase("")){
                Toast.makeText(this, "please enter registration number", Toast.LENGTH_SHORT).show();
            }
            else{

                ValidatioPart();
            }

        }
        else{
            ValidatioPart();
        }



        //AddPRoduct();
    }


    private void ValidatioPart(){

        if (qty.equalsIgnoreCase("")){
            Toast.makeText(this, "please enter quantity", Toast.LENGTH_SHORT).show();
        }

        else if (UnitId.equalsIgnoreCase("") || UnitId.equalsIgnoreCase("0")){
            Toast.makeText(this, "please select unit name", Toast.LENGTH_SHORT).show();
        }

        else if (price.equalsIgnoreCase("")){
            Toast.makeText(this, "please enter price", Toast.LENGTH_SHORT).show();
        }
        else {

            if (isEdit){
                UpdateProduct();
            }else {
                AddPRoduct();
            }

        }
    }

    private void subCropSelect(){
        subCrop_nameList.clear();
        subCrop_nameList.add(new CropDetail("0", "Select"));
        SubCropDataAdapter = new ArrayAdapter<CropDetail>(this,android.R.layout.simple_spinner_item, subCrop_nameList);
        SubCropDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSubCrop.setAdapter(SubCropDataAdapter);
        getSubCropData();
    }
    private void unitSelect(){
        // spinnerUnit.setEnabled(true);
        unit_nameList.clear();
        unit_nameList.add(new Unit("0", "Select"));
        unitDataAdapter = new ArrayAdapter<Unit>(this,android.R.layout.simple_spinner_item, unit_nameList);
        unitDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUnit.setAdapter(unitDataAdapter);
        getUnitData();
    }

    public void insertImage(){


        mAdapter = new ImageListAdapter(AddProductActivity.this, imageProductList,"FromMobile");
        imageRecyclerView.setHasFixedSize(true);
        imageRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        imageRecyclerView.setAdapter(mAdapter);
    }


    // Select image from camera and gallery
    @SuppressLint("NewApi")
    private void selectImage() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());

            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;

        if (requestCode == PICK_IMAGE_CAMERA) {


            if (data!= null){


                try {
                    Uri selectedImage = data.getData();

                    Bitmap   bitmap = (Bitmap) data.getExtras().get("data");

                   // Bitmap  bitmap = scaleImage(this,selectedImage);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                   // bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                   // bitmap = getResizedBitmap(bitmap, 500);
                    Log.e("Activity", "Pick from Camera::>>> ");

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                    destination = new File(Environment.getExternalStorageDirectory() + "/" +
                            getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                    if (isCertificate){
                        isCertificate = false;
                        img_certificate.setVisibility(View.VISIBLE);
                        cross_img.setVisibility(View.VISIBLE);
                        img_certificate.setImageBitmap(bitmap);
                        certi_bitmap = bitmap;


                    }
                    else {
                        imgPath = destination.getAbsolutePath();


                        String  img = BitMapToString(bitmap);
                        imageProductList.add(new UserAddedProductImage("",img));

                        mAdapter.notifyDataSetChanged();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
            }


        } else if (requestCode == PICK_IMAGE_GALLERY) {

            if (data!= null){


                Uri selectedImage = data.getData();
                try {
                    //Bitmap  bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);

                    Bitmap bitmap = scaleImage(this,selectedImage);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    //bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    //bitmap = getResizedBitmap(bitmap, 1000);
                    Log.e("Activity", "Pick from Gallery::>>> ");

                    if (isCertificate){
                        isCertificate = false;
                        img_certificate.setVisibility(View.VISIBLE);
                        cross_img.setVisibility(View.VISIBLE);
                        img_certificate.setImageBitmap(bitmap);
                        certi_bitmap = bitmap;
                    }
                    else {

                        String  img = BitMapToString(bitmap);
                        imageProductList.add(new UserAddedProductImage("",img));
                        mAdapter.notifyDataSetChanged();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void getSubCropData() {
        HashMap<String, String> map = new HashMap<>();
        pd.show();
        map.put("languageid","1");
        map.put("cropmasterid",CropID);
        ApiRequest apiRequest = new ApiRequest(AddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_SUB_CROP_LIST, Constants.GET_SUB_CROP_LIST,map, Request.Method.POST);
    }
    private void getUnitData() {
        HashMap<String, String> map = new HashMap<>();
        pd.show();
        map.put("languageid","1");
        ApiRequest apiRequest = new ApiRequest(AddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_UNIT, Constants.GET_UNIT,map, Request.Method.POST);
    }

    private void editProduct(String ProductID) {
        HashMap<String, String> map = new HashMap<>();
        pd.show();
        map.put("usertype",userType);
        map.put("productid",ProductID);
        final Map<String, MultipartRequest.DataPart> params = new HashMap<>();
        ApiRequest apiRequest = new ApiRequest(AddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.EDIT_PRODUCT, Constants.EDIT_PRODUCT,map, Request.Method.POST);
    }

    private void ProductImage(String ProductID) {
        HashMap<String, String> map = new HashMap<>();
        // pd.show();
        map.put("productid",ProductID);
        final Map<String, MultipartRequest.DataPart> params = new HashMap<>();
        ApiRequest apiRequest = new ApiRequest(AddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_PRODUCT_IMAGE, Constants.GET_PRODUCT_IMAGE,map, Request.Method.POST);
    }

    private void AddPRoduct() {



        if (imageProductList.size()>0){

            imageSize = String.valueOf(imageProductList.size());
            for (int i = 0; i < imageProductList.size(); i++) {

                if (i == 0){
                    Bitmap   bitmap = StringToBitMap(imageProductList.get(i).getByteArrayOfImage());
                    ImageOne =  getFileDataFromDrawable1(bitmap);
                }
                else if (i ==1){
                    Bitmap    bitmap = StringToBitMap(imageProductList.get(i).getByteArrayOfImage());
                    ImageTwo =  getFileDataFromDrawable2(bitmap);
                }
                else if (i == 2){
                    Bitmap   bitmap = StringToBitMap(imageProductList.get(i).getByteArrayOfImage());
                    ImageThree =  getFileDataFromDrawable3(bitmap);
                }
            }

        }


        if (certi_bitmap != null){
            if (getFileDataFromDrawable4(certi_bitmap)!= null){
                img = getFileDataFromDrawable4(certi_bitmap);
            }
        }


        HashMap<String, String> map = new HashMap<>();
        pd.show();
        map.put("usertype",userType);
        map.put("userid",userID);
        map.put("cropdetailid",subCropID);
        map.put("organic",OrganicID);
        map.put("registeredwith",RegisterWithID);
        map.put("otherorganizationname",OthrRegName);
        map.put("registrationno",RegN);
        map.put("quantity",qty);
        map.put("unit",UnitId);
        map.put("price",price);
        map.put("showprice",priceShow);
        map.put("wantustocontact",ContactToRegID);
        map.put("comments",comments);
        map.put("other",otherCropName);
        map.put("certificate",img);
        map.put("productimage1",ImageOne);
        map.put("productimage2",ImageTwo);
        map.put("productimage3",ImageThree);
        map.put("ic",imageSize);

        // final Map<String, MultipartRequest.DataPart> params = new HashMap<>();

//        params.put("image", new MultipartRequest.DataPart( "ProfilePic.jpg",
//                getFileDataFromDrawable(bitmap), "image/jpeg"));

        ApiRequest apiRequest = new ApiRequest(AddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.ADD_PRODUCT, Constants.ADD_PRODUCT,map, Request.Method.POST);
    } private void UpdateProduct() {



        if (imageProductList.size()>0){

            imageSize = String.valueOf(imageProductList.size());
            for (int i = 0; i < imageProductList.size(); i++) {

                if (i == 0){
                    Bitmap   bitmap = StringToBitMap(imageProductList.get(i).getByteArrayOfImage());
                    ImageOne =  getFileDataFromDrawable1(bitmap);
                }
                else if (i ==1){
                    Bitmap    bitmap = StringToBitMap(imageProductList.get(i).getByteArrayOfImage());
                    ImageTwo =  getFileDataFromDrawable2(bitmap);
                }
                else if (i == 2){
                    Bitmap   bitmap = StringToBitMap(imageProductList.get(i).getByteArrayOfImage());
                    ImageThree =  getFileDataFromDrawable3(bitmap);
                }
            }

        }





        if (certi_bitmap != null){
            if (getFileDataFromDrawable4(certi_bitmap)!= null){
                img = getFileDataFromDrawable4(certi_bitmap);
            }
        }




        HashMap<String, String> map = new HashMap<>();
           pd.show();
        map.put("usertype",userType);
        map.put("productid",ProductID);
        map.put("cropdetailid",subCropID);
        map.put("organic",OrganicID);
        map.put("registeredwith",RegisterWithID);
        map.put("otherorganizationname",OthrRegName);
        map.put("registrationno",RegN);
        map.put("quantity",qty);
        map.put("unit",UnitId);
        map.put("price",price);
        map.put("showprice",priceShow);
        map.put("wantustocontact",ContactToRegID);
        map.put("comments",comments);
        map.put("other",otherCropName);
        map.put("certificate",img);
        map.put("productimage1",ImageOne);
        map.put("productimage2",ImageTwo);
        map.put("productimage3",ImageThree);
        map.put("ic",imageSize);

    /* final Map<String, MultipartRequest.DataPart> params = new HashMap<>();
       params.put("image", new MultipartRequest.DataPart( "ProfilePic.jpg",
                getFileDataFromDrawable(bitmap), "image/jpeg"));*/

        ApiRequest apiRequest = new ApiRequest(AddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.UPDATE_PRODUCT, Constants.UPDATE_PRODUCT,map, Request.Method.POST);
    }

    public String getFileDataFromDrawable1(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] imaBytes =byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imaBytes,Base64.DEFAULT);





    }public String getFileDataFromDrawable2(Bitmap bitmap) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] imaBytes =byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imaBytes,Base64.DEFAULT);

    }public String getFileDataFromDrawable3(Bitmap bitmap) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] imaBytes =byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imaBytes,Base64.DEFAULT);

    }public String getFileDataFromDrawable4(Bitmap bitmap) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] imaBytes =byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imaBytes,Base64.DEFAULT);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        //Sub Crop
        if (tag_json_obj.equalsIgnoreCase(Constants.GET_SUB_CROP_LIST)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                GetSubCropModel stateArray = new Gson().fromJson(response, new TypeToken<GetSubCropModel>(){}.getType());

                if (stateArray != null){
                    newSubCrop_nameList.clear();
                    newSubCrop_nameList = (ArrayList<CropDetail>) stateArray.getCropDetails();

                    for (int i = 0; i <newSubCrop_nameList.size() ; i++) {
                        subCrop_nameList.add(new CropDetail(""+newSubCrop_nameList.get(i).getCropdetailId(),
                                ""+newSubCrop_nameList.get(i).getCropdetailName()));
                    }

                    SubCropDataAdapter.notifyDataSetChanged();
                    if(isEdit) {
                        for (int i = 0; i <subCrop_nameList.size() ; i++) {
                            String sName = subCrop_nameList.get(i).getCropdetailId();
                            if (sName.equalsIgnoreCase(subCropID)) {
                                //   stateId = state_name.get(i).getState_id();
                                spinnerSubCrop.setSelection(i);
                            }
                        }
                    }
                    spinnerSubCrop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                            Object state_item = parent.getItemAtPosition(pos);
                            subCropID = "";

                            subCropID= subCrop_nameList.get(pos).getCropdetailId();
                            if (CropID.equalsIgnoreCase("10")){

                                if (subCropID.equalsIgnoreCase("51")){
                                    rr_otherCrop.setVisibility(View.VISIBLE);
                                }
                                else {
                                    rr_otherCrop.setVisibility(View.GONE);
                                }
                            }
                            else {
                                rr_otherCrop.setVisibility(View.GONE);
                            }

                            if(subCropID != null && !subCropID.equalsIgnoreCase("0") && !subCropID.equalsIgnoreCase("")) {
                           /* district_name.clear();
                            district_name.add(new CropDetail("0", "Select"));
                            cityDataAdapter.notifyDataSetChanged();
                            getDistrictListData();
                            districtSpinner.setEnabled(true);*/
                            }
                        }
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
                else {
                    Toast.makeText(this, "No sub crop found", Toast.LENGTH_SHORT).show();
                }

            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_UNIT)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                GetUnitModel stateArray = new Gson().fromJson(response, new TypeToken<GetUnitModel>(){}.getType());
                newUnit_nameList.clear();
                newUnit_nameList = (ArrayList<Unit>) stateArray.getUnit();

                for (int i = 0; i <newUnit_nameList.size() ; i++) {
                    unit_nameList.add(new Unit(""+newUnit_nameList.get(i).getUnitId(),
                            ""+newUnit_nameList.get(i).getUnitName()));
                }



                unitDataAdapter.notifyDataSetChanged();
                if(isEdit) {
                    for (int i = 0; i <unit_nameList.size() ; i++) {
                        String sName = unit_nameList.get(i).getUnitId();
                        if (sName.equalsIgnoreCase(UnitId)) {
                            //stateId = state_name.get(i).getState_id();
                            spinnerUnit.setSelection(i);
                        }
                    }
                }
                spinnerUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        Object state_item = parent.getItemAtPosition(pos);
                        UnitId = "";

                        UnitId= unit_nameList.get(pos).getUnitId();
                        if(UnitId != null && !UnitId.equalsIgnoreCase("0") && !UnitId.equalsIgnoreCase("")) {
                           /* district_name.clear();
                            district_name.add(new CropDetail("0", "Select"));
                            cityDataAdapter.notifyDataSetChanged();
                            getDistrictListData();
                            districtSpinner.setEnabled(true);*/
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
//Add product
        else if (tag_json_obj.equalsIgnoreCase(Constants.ADD_PRODUCT)){
            // State list api
            if(response != null) {
                pd.dismiss();
                AddProductModel stateArray = new Gson().fromJson(response, new TypeToken<AddProductModel>(){}.getType());
                int code = stateArray.getCode();
                String msg = stateArray.getMessage();
                if (code == 200){

                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                    Intent in = new Intent(AddProductActivity.this,SellerProductListActivity.class);
                    startActivity(in);
                    finish();


                }
                else {
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }

        }
        //GETIMAGE
        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_PRODUCT_IMAGE)){
            // State list api
            if(response != null) {
                pd.dismiss();
                ProductImageModel stateArray = new Gson().fromJson(response, new TypeToken<ProductImageModel>(){}.getType());
                int code = stateArray.getCode();
                String msg = stateArray.getMessage();

                if (code == 200){

                    imageProductList = (ArrayList<UserAddedProductImage>) stateArray.getUserAddedProductImage();


                    if (imageProductList.size()>0){

                        mAdapter = new ImageListAdapter(AddProductActivity.this, imageProductList,"FromServer");
                        imageRecyclerView.setHasFixedSize(true);
                        imageRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
                        imageRecyclerView.setAdapter(mAdapter);

                    }



                }
                else {
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }

        }
        //Update Product


        else if (tag_json_obj.equalsIgnoreCase(Constants.UPDATE_PRODUCT)){
            // State list api
            if(response != null) {
                pd.dismiss();
                AddProductModel stateArray = new Gson().fromJson(response, new TypeToken<AddProductModel>(){}.getType());
                int code = stateArray.getCode();
                String msg = stateArray.getMessage();
                if (code == 200){
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                    Intent in = new Intent(AddProductActivity.this,SellerProductListActivity.class);
                    startActivity(in);
                    finish();


                }
                else {
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        else if (tag_json_obj.equalsIgnoreCase(Constants.EDIT_PRODUCT)){
            // State list api


            if(response != null) {
                pd.dismiss();
                SellerEditModel stateArray = new Gson().fromJson(response, new TypeToken<SellerEditModel>(){}.getType());
                int code = stateArray.getCode();
                String msg = stateArray.getMessage();
                if (code == 200){

                    cropDetailID = stateArray.getUserAddedProduct().get(0).getCropDetailId();
                    String IsOrganic = stateArray.getUserAddedProduct().get(0).getIsOrganic();
                    String Quantity = stateArray.getUserAddedProduct().get(0).getQuantity();
                    Unit = stateArray.getUserAddedProduct().get(0).getUnit();
                    String Comments = (String) stateArray.getUserAddedProduct().get(0).getComments();
                    String unitPrice = stateArray.getUserAddedProduct().get(0).getUnitPrice();
                    String showPrice = stateArray.getUserAddedProduct().get(0).getShowPrice();
                    String registerWith  = stateArray.getUserAddedProduct().get(0).getRegisteredWith();
                    String otherOrganizationName = stateArray.getUserAddedProduct().get(0).getOtherOrg();
                    /// String wantToRegWithOrg = (String) stateArray.getUserAddedProduct().get(0).getWantToRegisterAsOrganic();
                    String wantUsToContact = (String) stateArray.getUserAddedProduct().get(0).getWantUsToContact();
                    String other = (String) stateArray.getUserAddedProduct().get(0).getOther();
                    String regNumber = (String) stateArray.getUserAddedProduct().get(0).getRegistrationNumber();
                    String  certificate = (String) stateArray.getUserAddedProduct().get(0).getCertificate();
                    String  imgBytearray = (String) stateArray.getUserAddedProduct().get(0).getByteArrayOfCertificate();




                    subCropID = cropDetailID;
                    UnitId = Unit;
                    if (IsOrganic.equalsIgnoreCase("1")){
                        OrganicID = "1";
                        organicYes.setChecked(true);
                        add_registrationNumber_layout.setVisibility(View.VISIBLE);
                    }
                    else if (IsOrganic.equalsIgnoreCase("0")){
                        OrganicID = "0";
                        rr_registredOrganic.setVisibility(View.VISIBLE);
                        noOrganic.setChecked(true);
                        if (wantUsToContact.equalsIgnoreCase("1")){
                            registerYes.setChecked(true);
                        }
                        else {
                            registerNo.setChecked(true);
                        }

                    }
                    et_quantity.setText(Quantity);
                    if (Comments!=null){

                        et_comments.setText(Comments);

                    }

                    if (showPrice.equalsIgnoreCase("1")){

                        chkIos.setChecked(true);
                        priceShow = "1";
                    }
                    else {
                        chkIos.setChecked(false);
                        priceShow = "0";
                    }

                    if (other!=null){
                        rr_otherCrop.setVisibility(View.VISIBLE);
                        et_OtherCropName.setText(other);
                    }



                    if (certificate!=null){
                        Bitmap bitmap = StringToBitMap(imgBytearray);
                        img_certificate.setVisibility(View.VISIBLE);
                        cross_img.setVisibility(View.VISIBLE);
                        img_certificate.setImageBitmap(bitmap);

                        certi_bitmap = bitmap;

                        // Picasso.with(AddProductActivity.this).load(certificate).into(img_certificate);
                    }
                    et_price.setText(unitPrice);
                    EditReg(registerWith,regNumber,otherOrganizationName);
                    subCropSelect();
                    unitSelect();
                }
                else {
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        Toast.makeText(this, ""+error, Toast.LENGTH_SHORT).show();

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void EditReg(String registerWith,String RegistrationNo,String  OtherOrg){

        if (registerWith.equalsIgnoreCase("1")){
            regGovt.setChecked(true);
            RegisterWithID = "1";
            et_registrationNumber.setText(RegistrationNo);
        }
        else if (registerWith.equalsIgnoreCase("2")){
            regPgs.setChecked(true);
            RegisterWithID = "2";
            et_registrationNumber.setText(RegistrationNo);
        }
        else if (registerWith.equalsIgnoreCase("3")){
            regSelf.setChecked(true);
            RegisterWithID = "3";
        } else if (registerWith.equalsIgnoreCase("4")){
            regOther.setChecked(true);
            RegisterWithID = "4";
            et_registrationNumber.setText(RegistrationNo);
            et_Otherorganization.setText(OtherOrg);
            add_Otherorganization_layout.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
               // Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp=Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
    public static Bitmap scaleImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > 512 || rotatedHeight > 512) {
            float widthRatio = ((float) rotatedWidth) / ((float) 512);
            float heightRatio = ((float) rotatedHeight) / ((float) 512);
            float maxRatio = Math.max(widthRatio, heightRatio);

            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

        /*
         * if the orientation is not 0 (or -1, which means we don't know), we
         * have to do a rotation.
         */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        }

        String type = context.getContentResolver().getType(photoUri);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (type.equals("image/png")) {
            srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        } else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
            srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }
        byte[] bMapArray = baos.toByteArray();
        baos.close();
        return BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);
    }

    public static int getOrientation(Context context, Uri photoUri) {
        /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[] { MediaStore.Images.ImageColumns.ORIENTATION }, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }
}
