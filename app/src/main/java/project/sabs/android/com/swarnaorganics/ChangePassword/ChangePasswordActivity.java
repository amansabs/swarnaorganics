package project.sabs.android.com.swarnaorganics.ChangePassword;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;

import android.os.Bundle;

import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import project.sabs.android.com.swarnaorganics.ForgotPassword.ForgotPasswordActivity;
import project.sabs.android.com.swarnaorganics.Otp.GetOtpActivity;
import project.sabs.android.com.swarnaorganics.Preferences;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Register.RegisterActivity;
import project.sabs.android.com.swarnaorganics.Register.SignupModel;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

public class ChangePasswordActivity extends AppCompatActivity implements IApiResponse {
    TextView text_toolbar;
    RelativeLayout rr_back;

    TextView forgotPass;
    CardView card_changePass;

    //old Pass
    EditText Current_password;
    ImageView showCurrent_pass_img;
    String currentPassword = "";
    boolean currentPassword_isShow = false;
    //new Pass
    EditText new_password;
    ImageView showNew_pass_img;
    String showNewPassword = "";
    boolean showNewPassword_isShow = false;
    //repeat new pass
    EditText repeatNew_password;
    ImageView repeatNew_show_pass_img;
    String repeatNewpassword = "";
    String userMobile = "";
    boolean repeatNewPass_isShow = false;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }


        findViews();

        pd = new ProgressDialog(ChangePasswordActivity.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);


        text_toolbar.setText("Change Password");
        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        showCurrent_pass_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPassword =Current_password.getText().toString();
                if (!currentPassword.equalsIgnoreCase("")){
                    if (currentPassword_isShow){
                        currentPassword_isShow = false;
                        showCurrent_pass_img.setBackgroundResource(R.drawable.hide_pass);
                        Current_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                    }else {
                        currentPassword_isShow = true;
                        showCurrent_pass_img.setBackgroundResource(R.drawable.show_pass);
                        Current_password.setInputType(InputType.TYPE_CLASS_TEXT);
                    }
                }
                else {
                    Toast.makeText(ChangePasswordActivity.this, "please enter password", Toast.LENGTH_SHORT).show();
                }
            }
        });

        showNew_pass_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNewPassword =new_password.getText().toString();
                if (!showNewPassword.equalsIgnoreCase("")){
                    if (showNewPassword_isShow){
                        showNewPassword_isShow = false;
                        showNew_pass_img.setBackgroundResource(R.drawable.hide_pass);
                        new_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                    }else {
                        showNewPassword_isShow = true;
                        showNew_pass_img.setBackgroundResource(R.drawable.show_pass);
                        new_password.setInputType(InputType.TYPE_CLASS_TEXT);
                    }
                }
                else {
                    Toast.makeText(ChangePasswordActivity.this, "please enter password", Toast.LENGTH_SHORT).show();
                }
            }
        });

        repeatNew_show_pass_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeatNewpassword =repeatNew_password.getText().toString();
                if (!repeatNewpassword.equalsIgnoreCase("")){
                    if (repeatNewPass_isShow){
                        repeatNewPass_isShow = false;
                        repeatNew_show_pass_img.setBackgroundResource(R.drawable.hide_pass);
                        repeatNew_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                    }else {
                        repeatNewPass_isShow = true;
                        repeatNew_show_pass_img.setBackgroundResource(R.drawable.show_pass);
                        repeatNew_password.setInputType(InputType.TYPE_CLASS_TEXT);
                    }
                }
                else {
                    Toast.makeText(ChangePasswordActivity.this, "please enter password", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if( !Preferences.get(this,Preferences.USER_TYPE).equalsIgnoreCase(null)) {
            userMobile = Preferences.get(this,Preferences.KEY_MOBILE);
        }


        card_changePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                velidation();

            }
        });
        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(ChangePasswordActivity.this, ForgotPasswordActivity.class);
                in.putExtra("isFromChangePass",true);
                startActivity(in);

            }
        });

    }

    private void findViews() {
        forgotPass= (TextView)findViewById(R.id.txt_getNew);
        text_toolbar= (TextView)findViewById(R.id.text_toolbar);
        rr_back= (RelativeLayout) findViewById(R.id.rr_back);
        card_changePass= (CardView) findViewById(R.id.card_changePass);
        Current_password= (EditText) findViewById(R.id.Current_password);
        new_password= (EditText) findViewById(R.id.new_password);
        repeatNew_password= (EditText) findViewById(R.id.repeatNew_password);
        showCurrent_pass_img= (ImageView) findViewById(R.id.showCurrent_pass_img);
        showNew_pass_img= (ImageView) findViewById(R.id.showNew_pass_img);
        repeatNew_show_pass_img= (ImageView) findViewById(R.id.repeatNew_show_pass_img);
    }
    private void velidation(){

        currentPassword = Current_password.getText().toString();
        showNewPassword= new_password.getText().toString();
        repeatNewpassword = repeatNew_password.getText().toString();

        if(currentPassword.equalsIgnoreCase("")){
            Toast.makeText(ChangePasswordActivity.this, "Enter current password", Toast.LENGTH_SHORT).show();
        }
        else if (showNewPassword.equalsIgnoreCase("")){
            Toast.makeText(ChangePasswordActivity.this, "Enter new password", Toast.LENGTH_SHORT).show();
        }
        else if (repeatNewpassword.equalsIgnoreCase("")){
            Toast.makeText(ChangePasswordActivity.this, "Enter repeat password", Toast.LENGTH_SHORT).show();
        }
        else if(!showNewPassword.equalsIgnoreCase(repeatNewpassword)){
            Toast.makeText(ChangePasswordActivity.this, "Password not match", Toast.LENGTH_SHORT).show();
        }
        else{
            ChangePassword(userMobile,currentPassword,repeatNewpassword);
        }

    }

    private void ChangePassword(String userMobile,String currentPassword,String repeatNewpassword){
        pd.show();
        String  userType = Preferences.get(this,Preferences.USER_TYPE);
        HashMap<String, String> map = new HashMap<>();
        map.put("mobile",userMobile);
        map.put("usertype",userType);
        map.put("oldpassword",currentPassword);
        map.put("newpassword",repeatNewpassword);
        ApiRequest apiRequest = new ApiRequest(ChangePasswordActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.CHANGE_PASSWORD, Constants.CHANGE_PASSWORD,map, Request.Method.POST);

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if(tag_json_obj.equalsIgnoreCase(Constants.CHANGE_PASSWORD)) {
            if(response != null) {
                ChangePAsswordModel finalArray = new Gson().fromJson(response,new TypeToken<ChangePAsswordModel>(){}.getType());
                int status=finalArray.getCode();
                if (status == 200){
                    String msg=finalArray.getMessage();
                    pd.dismiss();
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                    repeatNew_password.getText().clear();
                    new_password.getText().clear();
                    Current_password.getText().clear();
                    finish();
                }
                else {
                    pd.dismiss();
                    repeatNew_password.getText().clear();
                    new_password.getText().clear();
                    Current_password.getText().clear();
                    String msg=finalArray.getMessage();
                    Toast.makeText(this, " "+msg, Toast.LENGTH_SHORT).show();

                }
            }
            else {
                pd.dismiss();
                Toast.makeText(this, "No internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            pd.dismiss();
            Toast.makeText(this, "No internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        Toast.makeText(this, "Some thing went rong", Toast.LENGTH_SHORT).show();
    }
}
