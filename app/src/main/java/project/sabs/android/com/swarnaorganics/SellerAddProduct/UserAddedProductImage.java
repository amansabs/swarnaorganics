
package project.sabs.android.com.swarnaorganics.SellerAddProduct;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAddedProductImage {



    @SerializedName("ImagePath")
    @Expose
    private String imagePath;
    @SerializedName("ByteArrayOfImage")
    @Expose
    private String byteArrayOfImage;

    public UserAddedProductImage(String imagePath, String byteArrayOfImage) {
        this.imagePath = imagePath;
        this.byteArrayOfImage = byteArrayOfImage;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getByteArrayOfImage() {
        return byteArrayOfImage;
    }

    public void setByteArrayOfImage(String byteArrayOfImage) {
        this.byteArrayOfImage = byteArrayOfImage;
    }

}
