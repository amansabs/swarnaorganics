package project.sabs.android.com.swarnaorganics.SellerAddProduct;

import android.app.ProgressDialog;
import android.os.Build;

import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.Preferences;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Search.CropModel.CropMaster;
import project.sabs.android.com.swarnaorganics.Search.CropModel.GetCropModel;
import project.sabs.android.com.swarnaorganics.Search.SearchActivity;
import project.sabs.android.com.swarnaorganics.Search.SearchList.SearchDataList;
import project.sabs.android.com.swarnaorganics.Search.SearchList.SearchListActivity;
import project.sabs.android.com.swarnaorganics.Search.SearchList.SearchListAdapter;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

public class SellerAddProductActivity extends AppCompatActivity implements IApiResponse {
    TextView text_toolbar;
    RelativeLayout rr_back;
    RecyclerView recycler_view_crop_list;
    private CropListAdapter mAdapter;
   // public ArrayList<CropListData> cropList=new ArrayList<CropListData>();
    final ArrayList<CropMaster> crop_nameList=new ArrayList<CropMaster>();
    ArrayList<CropMaster> newCrop_nameList=new ArrayList<CropMaster>();
    String userType= null;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_add_product);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        if(!Preferences.get(this,Constants.USER_TYPE).equalsIgnoreCase("0")) {
            userType = Preferences.get(this,Constants.USER_TYPE);
        }

        pd = new ProgressDialog(SellerAddProductActivity.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);


        text_toolbar= (TextView)findViewById(R.id.text_toolbar);
        rr_back= (RelativeLayout) findViewById(R.id.rr_back);
        recycler_view_crop_list= (RecyclerView) findViewById(R.id.recycler_view_crop_list);

        text_toolbar.setText("Add Product");
        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //CropList();
        getMasterCropListData();
    }

    private void CropList()
    {
    /* cropList.clear();
        cropList.add(new CropListData("Grain","1",R.drawable.grain_img));
        cropList.add(new CropListData("Medicine","2",R.drawable.ayurvedic_medicine_img));
        cropList.add(new CropListData("Lentils","3",R.drawable.lentils_img));
        cropList.add(new CropListData("Small Winpages","1",R.drawable.winpage_img));
        cropList.add(new CropListData("Tourism","2",R.drawable.tourism_img));
        cropList.add(new CropListData("Vegetable/Fruit","3",R.drawable.vegitable_img));
        cropList.add(new CropListData("OilSeed","3",R.drawable.oil_seed_img));
        cropList.add(new CropListData("Forest","3",R.drawable.forest_img));
        cropList.add(new CropListData("Other","3",R.drawable.other_img));*/
/*
        mAdapter = new CropListAdapter(SellerAddProductActivity.this, cropList,userType);
        recycler_view_crop_list.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_view_crop_list.setLayoutManager(layoutManager);
        recycler_view_crop_list.setAdapter(mAdapter);*/


    }

    private void getMasterCropListData() {
        pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        ApiRequest apiRequest = new ApiRequest(SellerAddProductActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_MASTER_CROP_LIST, Constants.GET_MASTER_CROP_LIST, map, Request.Method.POST);

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if(tag_json_obj.equalsIgnoreCase(Constants.GET_MASTER_CROP_LIST)){

            if(response != null) {

                GetCropModel finalArray = new Gson().fromJson(response, new TypeToken<GetCropModel>() {}.getType());

                newCrop_nameList = (ArrayList<CropMaster>) finalArray.getCropMaster();
                pd.dismiss();
               // Toast.makeText(this, "Get Data", Toast.LENGTH_LONG).show();
                mAdapter = new CropListAdapter(SellerAddProductActivity.this, newCrop_nameList,userType);
                recycler_view_crop_list.setHasFixedSize(true);
                LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                recycler_view_crop_list.setLayoutManager(layoutManager);
                recycler_view_crop_list.setAdapter(mAdapter);
            }
            else{
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
