
package project.sabs.android.com.swarnaorganics.DeleteModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteProductModel {

    @SerializedName("User Product")
    @Expose
    private Integer userProduct;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getUserProduct() {
        return userProduct;
    }

    public void setUserProduct(Integer userProduct) {
        this.userProduct = userProduct;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
