package project.sabs.android.com.swarnaorganics.Search.SearchList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.Login.LoginActivity;
import project.sabs.android.com.swarnaorganics.Preferences;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Search.SearchList.BuyerSearchModel.SearchedProduct;
import project.sabs.android.com.swarnaorganics.Volley.Constants;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class SearchListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<SearchedProduct> modelList;
    public static SearchListAdapter objSearchListAdapter;
    // private ArrayList<SearchedProduct> addToCartModelList =new ArrayList<SearchedProduct>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;

    String selectUser;
    public SearchListAdapter(Context context, ArrayList<SearchedProduct> modelList, String selectUser) {
        this.mContext = context;
        this.modelList = modelList;
        this.modelList = modelList;
        this.gridview = gridview;
        this.selectUser = selectUser;
        objSearchListAdapter = this;
    }


    public void updateList(ArrayList<SearchedProduct> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_user_search_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final SearchedProduct model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;

            String  shipId= Preferences.get(mContext,Preferences.Skip_ID);
            if (shipId.equalsIgnoreCase("0")){

                genericViewHolder.address_txt.setVisibility(View.VISIBLE);
                genericViewHolder.mobile_txt.setText(model.getMobile());
                genericViewHolder.call_img.setVisibility(View.VISIBLE);
                genericViewHolder.rr_contact.setVisibility(View.GONE);

                if (selectUser.equalsIgnoreCase("1")){
                    genericViewHolder.cropPrice.setVisibility(View.VISIBLE);
                    genericViewHolder.cropPriceView.setVisibility(View.VISIBLE);
                    genericViewHolder.cropPriceView.setText(model.getUnitPrice()+" ₹ /"+model.getUOM());
                }
                else {
                    genericViewHolder.cropPrice.setVisibility(View.GONE);
                    genericViewHolder.cropPriceView.setVisibility(View.GONE);

                }

            }
            else   if (shipId.equalsIgnoreCase("1")){

                genericViewHolder.address_txt.setVisibility(View.GONE);
                genericViewHolder.rr_contact.setVisibility(View.VISIBLE);
                genericViewHolder.call_img.setVisibility(View.GONE);
                genericViewHolder.mobile_txt.setText("**********");

                if (selectUser.equalsIgnoreCase("1")){
                    genericViewHolder.cropPrice.setVisibility(View.VISIBLE);
                    genericViewHolder.cropPriceView.setVisibility(View.VISIBLE);
                    genericViewHolder.cropPriceView.setText("**********");
                }
                else {
                    genericViewHolder.cropPrice.setVisibility(View.GONE);
                    genericViewHolder.cropPriceView.setVisibility(View.GONE);

                }

            }



            String subCrop = model.getSubCrop();


            if (model.getImagePath() != null){

              //  Picasso.with(mContext).load("https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500").placeholder(R.drawable.swarna_logo);

                Picasso.with(mContext).load("https://swarna-kreta-vikreta.com/"+model.getImagePath()).placeholder(R.drawable.swarna_logo)
                        .error(R.drawable.swarna_logo)
                        .into( genericViewHolder.img_cropImg);

            }else {
                genericViewHolder.img_cropImg.setImageResource(R.drawable.swarna_logo);
            }



            genericViewHolder.user_name_title.setText(model.getUserName());
            String OtherCrop = model.getOther();
            if ( OtherCrop != null && !OtherCrop.equalsIgnoreCase("")){
                genericViewHolder.cropName.setText(model.getOther() + " -"+subCrop );
            }
            else {
                genericViewHolder.cropName.setText(model.getCropMasterName() + " -"+subCrop );
            }

            // genericViewHolder.cropName.setText(model.getCropMasterName() + " -"+subCrop );
            genericViewHolder.cropQuantity.setText(model.getQuantity() +" "+model.getUOM());
            genericViewHolder.address_txt.setText(model.getAddress1());


            genericViewHolder.call_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String dail = model.getMobile();
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + dail));
                    mContext.startActivity(intent);

                }
            });

            genericViewHolder.rr_contact.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(mContext, LoginActivity.class);
                    mContext.startActivity(intent);
                    ((Activity)mContext).finishAffinity();


                }
            });


            setAnimation(holder.itemView, position);
        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private SearchedProduct getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, SearchedProduct model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_cropImg;
        private ImageView call_img;


        private TextView user_name_title;
        private TextView cropName;
        private TextView cropQuantity;
        private TextView address_txt;
        private TextView mobile_txt;
        private TextView cropPrice;
        private TextView cropPriceView;
        private RelativeLayout rr_contact;

        // @BindView(R.id.img_user)
        // ImageView imgUser;
        // @BindView(R.id.item_txt_title)
        // TextView itemTxtTitle;
        // @BindView(R.id.item_txt_message)
        // TextView itemTxtMessage;
        // @BindView(R.id.radio_list)
        // RadioButton itemTxtMessage;
        // @BindView(R.id.check_list)
        // CheckBox itemCheckList;
        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.img_cropImg = (ImageView) itemView.findViewById(R.id.img_cropImg);
            this.call_img = (ImageView) itemView.findViewById(R.id.call_img);
            this.user_name_title = (TextView) itemView.findViewById(R.id.user_name_title);
            this.cropName = (TextView) itemView.findViewById(R.id.cropName);
            this.cropQuantity = (TextView) itemView.findViewById(R.id.cropQuantity);
            this.address_txt = (TextView) itemView.findViewById(R.id.address_txt);
            this.mobile_txt = (TextView) itemView.findViewById(R.id.mobile_txt);
            this.cropPrice = (TextView) itemView.findViewById(R.id.cropPrice);
            this.cropPriceView = (TextView) itemView.findViewById(R.id.cropPriceView);
            this.rr_contact = (RelativeLayout) itemView.findViewById(R.id.rr_contact);



            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

}

