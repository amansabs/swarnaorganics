package project.sabs.android.com.swarnaorganics;

import android.app.Application;
import android.content.Context;

import project.sabs.android.com.swarnaorganics.Helper.LocaleHelper;

public class MainApplication extends Application{

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base,"en"));
    }
}
