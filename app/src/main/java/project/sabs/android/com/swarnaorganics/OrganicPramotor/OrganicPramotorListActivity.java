package project.sabs.android.com.swarnaorganics.OrganicPramotor;


import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Search.SearchActivity;
import project.sabs.android.com.swarnaorganics.Search.SearchList.BuyerSearchModel.BuyerSearchModel;
import project.sabs.android.com.swarnaorganics.Search.SearchList.BuyerSearchModel.SearchedProduct;
import project.sabs.android.com.swarnaorganics.Search.SearchList.SearchListActivity;
import project.sabs.android.com.swarnaorganics.Search.SearchList.SearchListAdapter;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

public class OrganicPramotorListActivity extends AppCompatActivity implements IApiResponse{

    TextView text_toolbar;
    RelativeLayout rr_back;
    OrganicPramotorListAdapter organicPramotorListAdapter;
    RecyclerView recycler_view_dealerlist;
    ArrayList<Dealer>  dealerArrayList =new ArrayList<Dealer>();
    String city ="";
    RelativeLayout rr_noRecord;
    ProgressDialog pd;
    CardView cc_recy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organic_pramotor_list);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        text_toolbar= (TextView)findViewById(R.id.text_toolbar);
        cc_recy= (CardView)findViewById(R.id.cc_recy);
        rr_back= (RelativeLayout) findViewById(R.id.rr_back);
        rr_noRecord= (RelativeLayout) findViewById(R.id.rr_noRecord);
        recycler_view_dealerlist= (RecyclerView) findViewById(R.id.recycler_view_dealerlist);
        text_toolbar.setText("Jaivik Suvidha Kendra");

        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

        pd = new ProgressDialog(OrganicPramotorListActivity.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);


        if (getIntent().getExtras() != null) {
            city = (getIntent().getStringExtra("city"));
            getDealerList();
        }


    }
    private void getDealerList(){
             pd.show();
            //int userType = Integer.parseInt(selectUser);
            HashMap<String, String> map = new HashMap<>();
            map.put("address",city);
            ApiRequest apiRequest = new ApiRequest(OrganicPramotorListActivity.this,this);
            apiRequest.postRequest(Constants.BASE_URL + Constants.GET_DEALER, Constants.GET_DEALER,map, Request.Method.POST);

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        if(tag_json_obj.equalsIgnoreCase(Constants.GET_DEALER)){

            if(response != null) {

                DealerModel finalArray = new Gson().fromJson(response, new TypeToken<DealerModel>() {}.getType());

                int code = finalArray.getCode();

                if (code == 200){
                    pd.dismiss();
                    dealerArrayList.addAll((ArrayList<Dealer>) finalArray.getDealers());

                    rr_noRecord.setVisibility(View.GONE);
                    cc_recy.setVisibility(View.VISIBLE);
                    organicPramotorListAdapter = new OrganicPramotorListAdapter(OrganicPramotorListActivity.this, dealerArrayList);
                    recycler_view_dealerlist.setHasFixedSize(true);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                    recycler_view_dealerlist.setLayoutManager(layoutManager);
                    recycler_view_dealerlist.setAdapter(organicPramotorListAdapter);

                }
                else {
                    pd.dismiss();
                    rr_noRecord.setVisibility(View.VISIBLE);
                    cc_recy.setVisibility(View.GONE);
                    Toast.makeText(this, ""+finalArray.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            else{

                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
