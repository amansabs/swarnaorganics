package project.sabs.android.com.swarnaorganics;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import project.sabs.android.com.swarnaorganics.Home.MainActivity;

public class FullScreenImage extends AppCompatActivity {
    ImageView img;
    String pos;
    RelativeLayout rr_left_img;
    RelativeLayout rr_right_img;
    RelativeLayout rr_cancel;
    int position = 0;

     int listSize ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        img = (ImageView) findViewById(R.id.img);
        rr_left_img = (RelativeLayout) findViewById(R.id.rr_left_img);
        rr_cancel = (RelativeLayout) findViewById(R.id.rr_cancel);
        rr_right_img = (RelativeLayout) findViewById(R.id.rr_right_img);

        Intent intent = getIntent();
        pos = intent.getStringExtra("imgPos");
        position = Integer.parseInt(pos);
        img.setImageResource(  MainActivity.advantagesModelsList.get(Integer.parseInt(pos)).getImg());

        listSize = MainActivity.advantagesModelsList.size();

        if (position == 0){
            rr_left_img.setVisibility(View.INVISIBLE);
        }

        if (position == listSize-1){
            rr_right_img .setVisibility(View.INVISIBLE);
        }

        rr_left_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (position >0){
                    int pos = position -1;
                    position =pos;
                    img.setImageResource(  MainActivity.advantagesModelsList.get(pos).getImg());
                    if (position == 0){

                            rr_left_img.setVisibility(View.INVISIBLE);

                    }

                    rr_right_img .setVisibility(View.VISIBLE);
                }


            }
        });

        rr_right_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                if (position<listSize){

                    if (position == listSize-1){

                        img.setImageResource(  MainActivity.advantagesModelsList.get(position).getImg());
                        rr_right_img .setVisibility(View.INVISIBLE);
                    }
                    else {
                        int pos = position +1;
                        position = pos;
                        img.setImageResource(  MainActivity.advantagesModelsList.get(pos).getImg());
                    }

                }
                else {
                    rr_right_img .setVisibility(View.INVISIBLE);
                }

                 if (position>0){
                     rr_left_img.setVisibility(View.VISIBLE);
                 }

            }
        });

        rr_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
}
