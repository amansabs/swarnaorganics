package project.sabs.android.com.swarnaorganics.SellerAddProduct.ImageList;

import android.graphics.Bitmap;

public class InserImageDataModel {
    Bitmap Image;

    public InserImageDataModel() {
    }

    public InserImageDataModel(Bitmap image) {
        Image = image;
    }

    public Bitmap getImage() {
        return Image;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }
}
