
package project.sabs.android.com.swarnaorganics.SellerAddProduct;

public class UnitDataModel {

   String unitName;
    String unit_id;



    @Override
    public String toString() {
        return unitName;
    }

    public UnitDataModel(String unitName, String unit_id) {
        this.unitName = unitName;
        this.unit_id = unit_id;
    }

    public String getunitName() {
        return unitName;
    }

    public void setunitName(String unitName) {
        this.unitName = unitName;
    }

    public String getunit_id() {
        return unit_id;
    }

    public void setunit_id(String unit_id) {
        this.unit_id = unit_id;
    }
}
