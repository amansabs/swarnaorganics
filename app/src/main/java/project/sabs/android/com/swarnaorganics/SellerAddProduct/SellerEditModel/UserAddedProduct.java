
package project.sabs.android.com.swarnaorganics.SellerAddProduct.SellerEditModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAddedProduct {

    @SerializedName("CropDetailId")
    @Expose
    private String cropDetailId;
    @SerializedName("IsOrganic")
    @Expose
    private String isOrganic;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("Unit")
    @Expose
    private String unit;
    @SerializedName("UnitPrice")
    @Expose
    private String unitPrice;
    @SerializedName("ShowPrice")
    @Expose
    private String showPrice;
    @SerializedName("OtherOrg")
    @Expose
    private String otherOrg;
    @SerializedName("Other")
    @Expose
    private String other;
    @SerializedName("RegisteredWith")
    @Expose
    private String registeredWith;
    @SerializedName("RegistrationNumber")
    @Expose
    private String registrationNumber;
    @SerializedName("WantUsToContact")
    @Expose
    private String wantUsToContact;
    @SerializedName("Comments")
    @Expose
    private String comments;
    @SerializedName("Certificate")
    @Expose
    private String certificate;
    @SerializedName("ByteArrayOfCertificate")
    @Expose
    private String byteArrayOfCertificate;

    public String getCropDetailId() {
        return cropDetailId;
    }

    public void setCropDetailId(String cropDetailId) {
        this.cropDetailId = cropDetailId;
    }

    public String getIsOrganic() {
        return isOrganic;
    }

    public void setIsOrganic(String isOrganic) {
        this.isOrganic = isOrganic;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getShowPrice() {
        return showPrice;
    }

    public void setShowPrice(String showPrice) {
        this.showPrice = showPrice;
    }

    public String getOtherOrg() {
        return otherOrg;
    }

    public void setOtherOrg(String otherOrg) {
        this.otherOrg = otherOrg;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getRegisteredWith() {
        return registeredWith;
    }

    public void setRegisteredWith(String registeredWith) {
        this.registeredWith = registeredWith;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getWantUsToContact() {
        return wantUsToContact;
    }

    public void setWantUsToContact(String wantUsToContact) {
        this.wantUsToContact = wantUsToContact;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getByteArrayOfCertificate() {
        return byteArrayOfCertificate;
    }

    public void setByteArrayOfCertificate(String byteArrayOfCertificate) {
        this.byteArrayOfCertificate = byteArrayOfCertificate;
    }

}
