package project.sabs.android.com.swarnaorganics.Home;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.R;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class HomeSliderAdapter extends RecyclerView.Adapter<HomeSliderAdapter.MyViewHolder> {

    private List<BannerModel> list;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_user;
        public MyViewHolder(View view) {
            super(view);
            img_user = view.findViewById(R.id.img_user1);


        }
    }



    public HomeSliderAdapter(List<BannerModel> moviesList, Context context) {
        this.list = moviesList;
        this.context=context;
    }

    @Override
    public HomeSliderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_list, parent, false);

        return new HomeSliderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HomeSliderAdapter.MyViewHolder holder, final int position) {

        holder.img_user.setImageResource(list.get(position).getImage());

       /* if (list.get(position).get() !=null ){
            Picasso.with(context).load(String.valueOf(list.get(position).getImages().get(0).getSrc()))
                    .into(holder.img_user);
        }
*/
      /*  Picasso.with(context).load(list.get(position).getOfferImage())
                .placeholder(R.drawable.logoshirmalbazar)
                .into(holder.img_user);*/

      /* Glide.with(context)
                .load(list.get(position).getOfferImage())
                .asGif()
                .placeholder(R.drawable.placeholder)
                .into(holder.img_user);*/


        /*Glide.with(context).load(list.get(position).getOfferImage())
                .thumbnail(Glide.with(context).load(R.drawable.placeholder))
                .fitCenter()
                .crossFade()
                .into(imageView);*/
 /*
        holder.relMaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, GroseryActivity.class);
                context.startActivity(i);
            }
        });*/

        holder.img_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


          /*      BannerModel BannerModel_id = list.get(position).getBannerModelIdFK();
                Intent i = new Intent(context, BannerModelDetailActivity.class);
                i.putExtra("ScreenName","best_selling_BannerModel");
                i.putExtra("BannerModel_id",BannerModel_id);
                context.startActivity(i);*/
                /*Intent i = new Intent(context, BannerModelDetailActivity.class);
                i.putExtra("BannerModel_id",list.get(position).getId());
                context.startActivity(i);


*/            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}


