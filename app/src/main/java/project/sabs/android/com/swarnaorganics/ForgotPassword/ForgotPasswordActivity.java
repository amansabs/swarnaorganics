package project.sabs.android.com.swarnaorganics.ForgotPassword;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import project.sabs.android.com.swarnaorganics.ChangePassword.ChangePAsswordModel;
import project.sabs.android.com.swarnaorganics.ChangePassword.ChangePasswordActivity;
import project.sabs.android.com.swarnaorganics.Otp.GetOtpActivity;
import project.sabs.android.com.swarnaorganics.Preferences;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

public class ForgotPasswordActivity extends AppCompatActivity implements IApiResponse{
    RelativeLayout rr_back;
    RelativeLayout rr_service;
    TextView text_toolbar;
    boolean isFromChangePass = false;
    EditText etxt_mobile;
    ProgressDialog pd;
    private RadioGroup SelectUserRadio;
    String selectUser = "";
    String mobileNum = "";
    CardView card_send_msg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        findViews();
        text_toolbar.setText("Forgot Password");
        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        if (getIntent().getExtras() != null) {
            isFromChangePass = (getIntent().getExtras().getBoolean("isFromChangePass"));
        }

        pd = new ProgressDialog(ForgotPasswordActivity.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);

        if (isFromChangePass){

            rr_service.setVisibility(View.GONE);
            selectUser =   Preferences.get(this,Preferences.USER_TYPE);
        }
        else {

            rr_service.setVisibility(View.VISIBLE);
        }

        card_send_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                validation();

            }
        });
    }

    private void findViews() {
        text_toolbar = (TextView) findViewById(R.id.text_toolbar);
        rr_back = (RelativeLayout) findViewById(R.id.rr_back);
        rr_service = (RelativeLayout) findViewById(R.id.rr_service);
        etxt_mobile = (EditText) findViewById(R.id.etxt_mobile);
        SelectUserRadio=(RadioGroup)findViewById(R.id.SelectUserRadio);
        card_send_msg=(CardView) findViewById(R.id.card_send_msg);

        SelectUserRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                switch (checkedId) {
                    case R.id.BuyerRadioButton:

                        selectUser = "2";

                        // Toast.makeText(LoginActivity.this, ""+checkedRadioButton.getText(), Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.SelllerRadioButton2:
                        selectUser = "1";
                        // Toast.makeText(LoginActivity.this, ""+checkedRadioButton.getText(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });



    }

    private  void validation(){

        mobileNum = etxt_mobile.getText().toString();
        if(mobileNum.length() < 10){
            Toast.makeText(ForgotPasswordActivity.this, "Please enter correct mobile number.", Toast.LENGTH_SHORT).show();
        }
        else if(!isFromChangePass){

            if (selectUser.equalsIgnoreCase("")){
                Toast.makeText(ForgotPasswordActivity.this, "Please select user type..", Toast.LENGTH_SHORT).show();
            }
            else {

                pd = new ProgressDialog(ForgotPasswordActivity.this,R.style.AppCompatAlertDialogStyle);
                pd.setMessage("loading");
                pd.setCancelable(false);
                pd.show();
                ForgotPassword(mobileNum,selectUser);
            }
        }
        else {

            pd = new ProgressDialog(ForgotPasswordActivity.this,R.style.AppCompatAlertDialogStyle);
            pd.setMessage("loading");
            pd.setCancelable(false);
            pd.show();
            ForgotPassword(mobileNum,selectUser);
        }
    }
    private void ForgotPassword(String userMobile,String selectUser){

        HashMap<String, String> map = new HashMap<>();
        map.put("mobile",userMobile);
        map.put("usertype",selectUser);
        ApiRequest apiRequest = new ApiRequest(ForgotPasswordActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.FORGOT_PASSWORD, Constants.FORGOT_PASSWORD,map, Request.Method.POST);

    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if(tag_json_obj.equalsIgnoreCase(Constants.FORGOT_PASSWORD)) {
            if(response != null) {
                ForgotPassModel finalArray = new Gson().fromJson(response,new TypeToken<ForgotPassModel>(){}.getType());
                int status=finalArray.getCode();
                if (status == 200){

                    String msg=finalArray.getMessage();
                    Toast.makeText(this, ""+msg, Toast.LENGTH_LONG).show();
                    Intent in = new Intent(ForgotPasswordActivity.this, GetOtpActivity.class);
                    in.putExtra("ForgotPass",true);
                    in.putExtra("mobile",mobileNum);
                    in.putExtra("usertype",selectUser);
                    in.putExtra("Otp","200");
                    startActivity(in);
                    pd.dismiss();

                }
                else {

                    String msg=finalArray.getMessage();
                    Toast.makeText(this, " "+msg, Toast.LENGTH_LONG).show();
                    Intent in = new Intent(ForgotPasswordActivity.this, GetOtpActivity.class);
                    in.putExtra("mobile",mobileNum);
                    in.putExtra("ForgotPass",true);
                  //in.putExtra("password",password);
                    in.putExtra("usertype",selectUser);
                    in.putExtra("Otp","400");
                    startActivity(in);
                    pd.dismiss();

                }
            }
            else {
                pd.dismiss();
                Toast.makeText(this, "No internet Connection", Toast.LENGTH_LONG).show();
            }
        }
        else {
            pd.dismiss();
            Toast.makeText(this, "No internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "No internet Connection"+error, Toast.LENGTH_LONG).show();
    }
}
