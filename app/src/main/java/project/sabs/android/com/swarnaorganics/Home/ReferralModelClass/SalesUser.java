
package project.sabs.android.com.swarnaorganics.Home.ReferralModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesUser {

    @SerializedName("SalesUserId")
    @Expose
    private String salesUserId;
    @SerializedName("Sales User Name")
    @Expose
    private String salesUserName;

    public SalesUser(String salesUserId, String salesUserName) {
        this.salesUserId = salesUserId;
        this.salesUserName = salesUserName;
    }

    public String getSalesUserId() {
        return salesUserId;
    }

    public void setSalesUserId(String salesUserId) {
        this.salesUserId = salesUserId;
    }

    public String getSalesUserName() {
        return salesUserName;
    }

    public void setSalesUserName(String salesUserName) {
        this.salesUserName = salesUserName;
    }
    @Override
    public String toString() {
        return getSalesUserName();
    }
}
