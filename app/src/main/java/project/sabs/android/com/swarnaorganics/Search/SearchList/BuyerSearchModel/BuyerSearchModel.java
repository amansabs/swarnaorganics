
package project.sabs.android.com.swarnaorganics.Search.SearchList.BuyerSearchModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BuyerSearchModel {

    @SerializedName("Searched Product")
    @Expose
    private List<SearchedProduct> searchedProduct = null;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;

    public List<SearchedProduct> getSearchedProduct() {
        return searchedProduct;
    }

    public void setSearchedProduct(List<SearchedProduct> searchedProduct) {
        this.searchedProduct = searchedProduct;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
