
package project.sabs.android.com.swarnaorganics.Search.CropModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("CropmasterId")
    @Expose
    private String cropmasterId;
    @SerializedName("CropMasterName")
    @Expose
    private String cropMasterName;

    public User(String cropmasterId, String cropMasterName) {
        this.cropmasterId = cropmasterId;
        this.cropMasterName = cropMasterName;
    }

    public String getCropmasterId() {
        return cropmasterId;
    }

    public void setCropmasterId(String cropmasterId) {
        this.cropmasterId = cropmasterId;
    }

    public String getCropMasterName() {
        return cropMasterName;
    }

    public void setCropMasterName(String cropMasterName) {
        this.cropMasterName = cropMasterName;
    }
    @Override
    public String toString() {
        return getCropMasterName();
    }


}
