
package project.sabs.android.com.swarnaorganics.Search.SubCropModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetSubCropModel {

    @SerializedName("Crop Details")
    @Expose
    private List<CropDetail> cropDetails = null;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;

    public List<CropDetail> getCropDetails() {
        return cropDetails;
    }

    public void setCropDetails(List<CropDetail> cropDetails) {
        this.cropDetails = cropDetails;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
