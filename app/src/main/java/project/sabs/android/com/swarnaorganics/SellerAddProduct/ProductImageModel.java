
package project.sabs.android.com.swarnaorganics.SellerAddProduct;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductImageModel {

    @SerializedName("User Added Product Image")
    @Expose
    private List<UserAddedProductImage> userAddedProductImage = null;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;

    public List<UserAddedProductImage> getUserAddedProductImage() {
        return userAddedProductImage;
    }

    public void setUserAddedProductImage(List<UserAddedProductImage> userAddedProductImage) {
        this.userAddedProductImage = userAddedProductImage;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
