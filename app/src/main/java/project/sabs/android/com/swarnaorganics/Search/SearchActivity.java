package project.sabs.android.com.swarnaorganics.Search;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;

import android.os.Bundle;

import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import project.sabs.android.com.swarnaorganics.Home.CountyModel.Country;
import project.sabs.android.com.swarnaorganics.Home.CountyModel.GetCountryModel;
import project.sabs.android.com.swarnaorganics.Home.DistrictModel.District;
import project.sabs.android.com.swarnaorganics.Home.DistrictModel.GetDistrictModel;
import project.sabs.android.com.swarnaorganics.Home.StateModel.GetStateModel;
import project.sabs.android.com.swarnaorganics.Home.StateModel.State;
import project.sabs.android.com.swarnaorganics.Preferences;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Search.CropModel.CropMaster;
import project.sabs.android.com.swarnaorganics.Search.CropModel.GetCropModel;
import project.sabs.android.com.swarnaorganics.Search.SearchList.SearchListActivity;
import project.sabs.android.com.swarnaorganics.Search.SubCropModel.CropDetail;
import project.sabs.android.com.swarnaorganics.Search.SubCropModel.GetSubCropModel;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

public class SearchActivity extends AppCompatActivity implements IApiResponse{
    TextView text_toolbar;
    RelativeLayout rr_back,rr_otherCrop;


    Spinner spinnerSubCrop ;
    Spinner countrySpinner ;
    Spinner stateSpinner ;
    Spinner districtSpinner ;

    //country
    ArrayAdapter<Country> dataAdapter = null;
    ArrayList<Country> newCountryList=new ArrayList<Country>();
    ArrayList<Country> country_name=new ArrayList<Country>();
    String countryID= "";

    //state
    final ArrayList<State> state_name=new ArrayList<State>();
    ArrayList<State> newStateList=new ArrayList<State>();
    ArrayAdapter<State> stateDataAdapter=null;
    String stateId="";
    //district
    String districtId="";
    final ArrayList<District> district_name=new ArrayList<District>();
    ArrayList<District> newDistrict_name=new ArrayList<District>();
    ArrayAdapter<District> cityDataAdapter=null;
    //crop
    Spinner spinnerCrop ;
    final ArrayList<CropMaster> crop_nameList=new ArrayList<CropMaster>();
    ArrayList<CropMaster> newCrop_nameList=new ArrayList<CropMaster>();
    ArrayAdapter<CropMaster> cropDataAdapter=null;
    String cropId="";
    ///subCrop
    final ArrayList<CropDetail> subCrop_nameList=new ArrayList<CropDetail>();
    ArrayList<CropDetail> newSubCrop_nameList=new ArrayList<CropDetail>();
    ArrayAdapter<CropDetail> SubCropDataAdapter=null;
    CardView card_Search;
    String subCropID="";
    ProgressDialog pd;
    //State
    private RadioGroup SelectUserRadio,SelectCropTypeRadio;
    String selectUser = "";
    String IsOrganic = "";

    EditText et_OtherCropName;
    String otherCropName ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        findViews();

        text_toolbar= (TextView)findViewById(R.id.text_toolbar);
        rr_back= (RelativeLayout) findViewById(R.id.rr_back);
        et_OtherCropName= (EditText) findViewById(R.id.et_OtherCropName);
        rr_otherCrop = (RelativeLayout) findViewById(R.id.rr_otherCrop);
        text_toolbar.setText("Search Product");
        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        pd = new ProgressDialog(SearchActivity.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);


        ///Country
        //Country
        countrySpinner.setEnabled(true);
        country_name.add(new Country("0","Select"));
        dataAdapter = new ArrayAdapter<Country>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,country_name);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countrySpinner.setAdapter(dataAdapter);

        ///State

        //GetState
        state_name.add(new State("0","Select"));
        stateDataAdapter = new ArrayAdapter<State>(this,android.R.layout.simple_spinner_item, state_name);
        stateDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stateSpinner.setAdapter(stateDataAdapter);
        //stateSpinner.setEnabled(false);


        //district
        district_name.add(new District("0", "Select"));
        cityDataAdapter = new ArrayAdapter<District>(this,android.R.layout.simple_spinner_item, district_name);
        cityDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        districtSpinner.setAdapter(cityDataAdapter);

        cropSelect();
        subCropSelect();
        getCountryListData();

        RadioButton buyerButton = (RadioButton) SelectUserRadio.findViewById(R.id.BuyerRadioButton);
        RadioButton sellerButton = (RadioButton) SelectUserRadio.findViewById(R.id.SelllerRadioButton2);

        String userType =   Preferences.get(this,Preferences.USER_TYPE);

        if (userType.equalsIgnoreCase("1")){
            buyerButton.setChecked(true);
            sellerButton.setChecked(false);
            selectUser = "2";
        }
        else if (userType.equalsIgnoreCase("2")){
            sellerButton.setChecked(true);
            buyerButton.setChecked(false);
            selectUser = "1";
        }


        SelectUserRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                switch (checkedId) {
                    case R.id.BuyerRadioButton:
                        selectUser = "2";
                        // Toast.makeText(LoginActivity.this, ""+checkedRadioButton.getText(), Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.SelllerRadioButton2:
                        selectUser = "1";
                        // Toast.makeText(LoginActivity.this, ""+checkedRadioButton.getText(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        SelectCropTypeRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                switch (checkedId) {
                    case R.id.OrganicRadioButton:
                        IsOrganic = "1";
                        // Toast.makeText(LoginActivity.this, ""+checkedRadioButton.getText(), Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nonOrganicRadioButton2:
                        IsOrganic = "0";
                        // Toast.makeText(LoginActivity.this, ""+checkedRadioButton.getText(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });


        card_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Velidation();
            }
        });


    }

    public void Velidation(){


        otherCropName =et_OtherCropName.getText().toString();

        if (selectUser.equalsIgnoreCase("")){

            Toast.makeText(this, "please select user type", Toast.LENGTH_SHORT).show();
        }


        else if(cropId.equalsIgnoreCase("") || cropId.equalsIgnoreCase("0")){
            //getStateListData();
            Toast.makeText(SearchActivity.this, "Please select crop", Toast.LENGTH_SHORT).show();
        }
       /* else if(subCropID.equalsIgnoreCase("") || subCropID.equalsIgnoreCase("0")){
            //getStateListData();
            Toast.makeText(SearchActivity.this, "Please select sub crop", Toast.LENGTH_SHORT).show();
        }*/
        else {

            Intent in = new Intent(SearchActivity.this, SearchListActivity.class);
            in.putExtra("selectUser",selectUser);
            in.putExtra("cropId",cropId);
            in.putExtra("subCropID",subCropID);
            in.putExtra("countryID",countryID);
            in.putExtra("stateId",stateId);
            in.putExtra("districtId",districtId);
            in.putExtra("other",otherCropName);
            in.putExtra("orgtype",IsOrganic);
            startActivity(in);

        }

    }


    private void findViews() {
        spinnerCrop = (Spinner)findViewById(R.id.spinnerCrop);
        spinnerSubCrop = (Spinner)findViewById(R.id.spinnerSubCrop);
        countrySpinner = (Spinner)findViewById(R.id.spinnerCountry);
        stateSpinner = (Spinner)findViewById(R.id.spinnerState);
        districtSpinner = (Spinner)findViewById(R.id.spinnerDistrict);
        card_Search = (CardView)findViewById(R.id.card_Search);
        SelectUserRadio=(RadioGroup)findViewById(R.id.SelectUserRadio);
        SelectCropTypeRadio=(RadioGroup)findViewById(R.id.SelectCropTypeRadio);
        spinnerSubCrop.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                if(cropId.equalsIgnoreCase("") || cropId.equalsIgnoreCase("0") ){
                    //getStateListData();
                    spinnerSubCrop.setEnabled(false);
                    Toast.makeText(SearchActivity.this, "Please select crop first", Toast.LENGTH_SHORT).show();

                }
                return false;
            }

        });

        stateSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                if(countryID.equalsIgnoreCase("") || countryID.equalsIgnoreCase("0") ){
                    //getStateListData();
                    stateSpinner.setEnabled(false);
                    Toast.makeText(SearchActivity.this, "Please select country first", Toast.LENGTH_SHORT).show();

                }
                return false;
            }

        });

        districtSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(countryID.equalsIgnoreCase("") || countryID.equalsIgnoreCase("0")){
                    //getStateListData();
                    districtSpinner.setEnabled(false);
                    Toast.makeText(SearchActivity.this, "Please select country first", Toast.LENGTH_SHORT).show();
                }
                else if (stateId.equalsIgnoreCase("")|| stateId.equalsIgnoreCase("0"))

                {
                    districtSpinner.setEnabled(false);
                    Toast.makeText(SearchActivity.this, "Please select state", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });


    }


    private void cropSelect(){

        //spinnerCrop.setEnabled(true);
        crop_nameList.add(new CropMaster("0", "Select"));
        cropDataAdapter = new ArrayAdapter<CropMaster>(this,android.R.layout.simple_spinner_item, crop_nameList);
        cropDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCrop.setAdapter(cropDataAdapter);

        getMasterCropListData();
    }


    private void subCropSelect(){

        subCrop_nameList.add(new CropDetail("0", "Select"));
        SubCropDataAdapter = new ArrayAdapter<CropDetail>(this,android.R.layout.simple_spinner_item, subCrop_nameList);
        SubCropDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSubCrop.setAdapter(SubCropDataAdapter);

    }

    private void getMasterCropListData() {
        pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        ApiRequest apiRequest = new ApiRequest(SearchActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_MASTER_CROP_LIST, Constants.GET_MASTER_CROP_LIST, map, Request.Method.POST);

    }

    private void getSubCropData() {
        HashMap<String, String> map = new HashMap<>();
        pd.show();
        map.put("languageid","1");
        map.put("cropmasterid",cropId);
        ApiRequest apiRequest = new ApiRequest(SearchActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_SUB_CROP_LIST, Constants.GET_SUB_CROP_LIST,map, Request.Method.POST);
    }

    private void getCountryListData() {
        pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        ApiRequest apiRequest = new ApiRequest(SearchActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_COUNTRY_LIST, Constants.GET_COUNTRY_LIST,map, Request.Method.POST);

    }

    private void getStateListData() {
        HashMap<String, String> map = new HashMap<>();
        pd.show();
        map.put("languageid","1");
        map.put("countryid",countryID);
        ApiRequest apiRequest = new ApiRequest(SearchActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_State_LIST, Constants.GET_State_LIST,map, Request.Method.POST);
    }

    private void getDistrictListData() {
        pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        map.put("stateid",stateId);
        ApiRequest apiRequest = new ApiRequest(SearchActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_DISTRICT_LIST, Constants.GET_DISTRICT_LIST, map, Request.Method.POST);
    }



    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if(tag_json_obj.equalsIgnoreCase(Constants.GET_MASTER_CROP_LIST)){

            if(response != null) {

                GetCropModel finalArray = new Gson().fromJson(response, new TypeToken<GetCropModel>() {}.getType());

                newCrop_nameList = (ArrayList<CropMaster>) finalArray.getCropMaster();

                for (int i = 0; i <newCrop_nameList.size() ; i++) {
                    String country_id=newCrop_nameList.get(i).getCropmasterId();
                    crop_nameList.add(new CropMaster(""+newCrop_nameList.get(i).getCropmasterId(),""+newCrop_nameList.get(i).getCropMasterName() ));
                }
                cropDataAdapter.notifyDataSetChanged();
                pd.dismiss();

               /* if(IsEdit) {
                    for (int i = 0; i <country_name.size() ; i++) {
                        String cName = country_name.get(i).getName();
                        if (cName.equalsIgnoreCase(countryName)) {
                            //countryID = country_name.get(i).getId();
                            countrySpinner.setSelection(i);
                        }
                    }
                }*/
                spinnerCrop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        Object country_item = parent.getItemAtPosition(pos);
                        cropId ="";
                        // subCropId="";

                        cropId =crop_nameList.get(pos).getCropmasterId();
                        if(cropId != null && !cropId.equalsIgnoreCase("0") && !cropId.equalsIgnoreCase("")){

                            subCrop_nameList.clear();
                            subCrop_nameList.add(new CropDetail("0", "Select"));
                            SubCropDataAdapter.notifyDataSetChanged();
                            getSubCropData();
                            spinnerSubCrop.setEnabled(true);

                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


            }
            else{
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        //Sub Crop
        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_SUB_CROP_LIST)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                GetSubCropModel stateArray = new Gson().fromJson(response, new TypeToken<GetSubCropModel>(){}.getType());
                newSubCrop_nameList = (ArrayList<CropDetail>) stateArray.getCropDetails();

                for (int i = 0; i <newSubCrop_nameList.size() ; i++) {
                    subCrop_nameList.add(new CropDetail(""+newSubCrop_nameList.get(i).getCropdetailId(),
                            ""+newSubCrop_nameList.get(i).getCropdetailName()));
                }

                SubCropDataAdapter.notifyDataSetChanged();
                /*if(IsEdit) {
                    for (int i = 0; i <state_name.size() ; i++) {
                        String sName = state_name.get(i).getStateName();
                        if (sName.equalsIgnoreCase(stateName)) {
                            //   stateId = state_name.get(i).getState_id();
                            stateSpinner.setSelection(i);
                        }
                    }
                }*/

                spinnerSubCrop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        Object state_item = parent.getItemAtPosition(pos);
                        subCropID = "";

                        subCropID= subCrop_nameList.get(pos).getCropdetailId();

                        if (cropId.equalsIgnoreCase("10")){

                            if (subCropID.equalsIgnoreCase("51")){
                                rr_otherCrop.setVisibility(View.VISIBLE);
                            }
                            else {
                                rr_otherCrop.setVisibility(View.GONE);
                            }
                        }
                        else {
                            rr_otherCrop.setVisibility(View.GONE);
                        }


                        if(subCropID != null && !subCropID.equalsIgnoreCase("0") && !subCropID.equalsIgnoreCase("")) {
                           /* district_name.clear();
                            district_name.add(new CropDetail("0", "Select"));
                            cityDataAdapter.notifyDataSetChanged();
                            getDistrictListData();
                            districtSpinner.setEnabled(true);*/
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
        //Country
        else if(tag_json_obj.equalsIgnoreCase(Constants.GET_COUNTRY_LIST)){

            if(response != null) {
                pd.dismiss();
                GetCountryModel finalArray = new Gson().fromJson(response, new TypeToken<GetCountryModel>() {}.getType());

                newCountryList = (ArrayList<Country>) finalArray.getCountries();

                for (int i = 0; i <newCountryList.size() ; i++) {
                    String country_id=newCountryList.get(i).getCountryId();
                    country_name.add(new Country(""+newCountryList.get(i).getCountryId(),""+newCountryList.get(i).getCountryName() ));
                    dataAdapter.notifyDataSetChanged();
                }


               /* if(IsEdit) {
                    for (int i = 0; i <country_name.size() ; i++) {
                        String cName = country_name.get(i).getName();
                        if (cName.equalsIgnoreCase(countryName)) {
                            //countryID = country_name.get(i).getId();
                            countrySpinner.setSelection(i);
                        }
                    }
                }*/
                countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        Object country_item = parent.getItemAtPosition(pos);
                        countryID ="";
                        stateId="";
                        districtId ="";

                        countryID=country_name.get(pos).getCountryId();
                        if(countryID != null && !countryID.equalsIgnoreCase("0") && !countryID.equalsIgnoreCase("")){

                            state_name.clear();
                            state_name.add(new State("0", "Select"));
                            stateDataAdapter.notifyDataSetChanged();
                            getStateListData();
                            stateSpinner.setEnabled(true);


                            district_name.clear();
                            district_name.add(new District("0", "Select"));
                            cityDataAdapter.notifyDataSetChanged();
                            getDistrictListData();
                            districtSpinner.setEnabled(true);


                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


            }
            else{
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }


        //State
        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_State_LIST)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                GetStateModel stateArray = new Gson().fromJson(response, new TypeToken<GetStateModel>(){}.getType());
                newStateList = (ArrayList<State>) stateArray.getStates();
                for (int i = 0; i <newStateList.size() ; i++) {
                    state_name.add(new State(""+newStateList.get(i).getStateId(),""+newStateList.get(i).getStateName()));
                }
                stateDataAdapter.notifyDataSetChanged();
                /*if(IsEdit) {
                    for (int i = 0; i <state_name.size() ; i++) {
                        String sName = state_name.get(i).getStateName();
                        if (sName.equalsIgnoreCase(stateName)) {
                            //   stateId = state_name.get(i).getState_id();
                            stateSpinner.setSelection(i);
                        }
                    }
                }*/
                stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        Object state_item = parent.getItemAtPosition(pos);
                        stateId = "";
                        districtId = "";
                        stateId= state_name.get(pos).getStateId();
                        if(stateId != null && !stateId.equalsIgnoreCase("0") && !stateId.equalsIgnoreCase("")) {
                            district_name.clear();
                            district_name.add(new District("0", "Select"));
                            cityDataAdapter.notifyDataSetChanged();
                            getDistrictListData();
                            districtSpinner.setEnabled(true);
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_DISTRICT_LIST)){
            // city list api
            if(response != null) {
                pd.dismiss();
                GetDistrictModel cityArray = new Gson().fromJson(response, new TypeToken<GetDistrictModel>(){}.getType());
                newDistrict_name = (ArrayList<District>) cityArray.getDistrict();
                // final ArrayList<CityData> city_name=new ArrayList<CityData>();

                //  ArrayAdapter<CityData> cityDataAdapter=null;
                if(newDistrict_name != null) {
                    for (int i = 0; i < newDistrict_name.size(); i++) {
                        district_name.add(new District("" + newDistrict_name.get(i).getDistrictId(), "" + newDistrict_name.get(i).getDistrictName()));
                    }
                }
                cityDataAdapter.notifyDataSetChanged();
               /* if(IsEdit) {
                    for (int i = 0; i <city_name.size() ; i++) {
                        String ctyName = city_name.get(i).getCityName();
                        if (ctyName.equalsIgnoreCase(cityName)) {
                            // cityId = city_name.get(i).getCity_id();
                            citySpinner.setSelection(i);
                        }
                    }
                }*/

                /*cityDataAdapter = new ArrayAdapter<CityData>(this,android.R.layout.simple_spinner_item, city_name);
                cityDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                citySpinner.setAdapter(cityDataAdapter);*/

                districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        Object id_num = parent.getItemIdAtPosition(pos);
                        if (countryID.equalsIgnoreCase("0"))
                        {
                            Toast.makeText(SearchActivity.this, "please enter city", Toast.LENGTH_SHORT).show();
                        }
                        districtId= district_name.get(pos).getDistrictId();
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

            }else{
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
