package project.sabs.android.com.swarnaorganics.Login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import io.paperdb.Paper;
import project.sabs.android.com.swarnaorganics.ForgotPassword.ForgotPasswordActivity;
import project.sabs.android.com.swarnaorganics.Helper.LocaleHelper;
import project.sabs.android.com.swarnaorganics.Home.MainActivity;
import project.sabs.android.com.swarnaorganics.Preferences;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Register.RegisterActivity;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.AddProductActivity;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.loaderspack.loaders.CircularSticksLoader;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements IApiResponse{
    ImageView show_pass_img;
    boolean isShow = false;
    String password;
    TextView newAccount;
    TextView txtSkip;
    CardView card_login;
    LinearLayout ll_forgotpass;
    EditText mobile_etxt;
    EditText password_etxt;
    private RadioGroup SelectUserRadio;
    String selectUser = null;
    String mobileNum = "";
    String passwordEt = "";
    ProgressDialog pd;
    RelativeLayout rr_loading;
    String IsProfileComplete = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String language = Preferences.get(this,Preferences.LANGUAGE);
        //setLocale(language);
        setContentView(R.layout.activity_login);

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        findViews();
        //setLocale("hi");

        show_pass_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                password=password_etxt.getText().toString();
                if (!password.equalsIgnoreCase("")){
                    if (isShow){
                        isShow = false;
                        show_pass_img.setBackgroundResource(R.drawable.hide_pass);
                        password_etxt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                    }else {
                        isShow = true;
                        show_pass_img.setBackgroundResource(R.drawable.show_pass);
                        password_etxt.setInputType(InputType.TYPE_CLASS_TEXT);
                    }
                }
                else {
                    Toast.makeText(LoginActivity.this, "please enter password", Toast.LENGTH_SHORT).show();
                }
            }
        });

        newAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(in);

            }
        });


        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.save(LoginActivity.this,
                        Preferences.Skip_ID, "1");
                Intent in = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(in);
                finish();
            }
        });

        ll_forgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent in = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(in);
            }
        });

        SelectUserRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                switch (checkedId) {
                    case R.id.BuyerRadioButton:

                        selectUser = "2";

                        // Toast.makeText(LoginActivity.this, ""+checkedRadioButton.getText(), Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.SelllerRadioButton2:
                        selectUser = "1";
                        // Toast.makeText(LoginActivity.this, ""+checkedRadioButton.getText(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
        card_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });
    }
      private void findViews() {
        password_etxt= (EditText) findViewById(R.id.password);
        mobile_etxt= (EditText) findViewById(R.id.mobile_etxt);
        show_pass_img= (ImageView) findViewById(R.id.show_pass_img);
        newAccount=(TextView) findViewById(R.id.newAccount);
        txtSkip=(TextView) findViewById(R.id.txtSkip);
        card_login=(CardView) findViewById(R.id.card_login);
        ll_forgotpass=(LinearLayout) findViewById(R.id.ll_forgotpass);
        SelectUserRadio=(RadioGroup)findViewById(R.id.SelectUserRadio);
        rr_loading=(RelativeLayout) findViewById(R.id.rr_loading);

    }
    public void setLocale(String lang) {
        Locale  myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        Configuration config = new Configuration();
        config.locale =myLocale;
        this.getResources().updateConfiguration(config,LoginActivity.this.getResources().getDisplayMetrics());
         }

    private  void validation(){

        mobileNum = mobile_etxt.getText().toString();
        passwordEt  = password_etxt.getText().toString();
        if(mobileNum.length() < 10){
            Toast.makeText(LoginActivity.this, "Please enter correct mobile number.", Toast.LENGTH_SHORT).show();
        }else if(passwordEt.equalsIgnoreCase("")){
            Toast.makeText(LoginActivity.this, "Please enter password.", Toast.LENGTH_SHORT).show();
        }
        else if (selectUser==null){
            Toast.makeText(LoginActivity.this, "Please select user type..", Toast.LENGTH_SHORT).show();
        }
        else {
            Login();
            pd = new ProgressDialog(LoginActivity.this,R.style.AppCompatAlertDialogStyle);
            pd.setMessage("loading");
            pd.setCancelable(false);
            pd.show();
        }
    }

    private void Login(){
        //int userType = Integer.parseInt(selectUser);
        HashMap<String, String> map = new HashMap<>();
        map.put("mobile",mobileNum);
        map.put("password",passwordEt);
        map.put("usertype",selectUser);
        ApiRequest apiRequest = new ApiRequest(LoginActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_LOGIN, Constants.GET_LOGIN,map, Request.Method.POST);
    }
    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if(tag_json_obj.equalsIgnoreCase(Constants.GET_LOGIN)) {
            if(response != null) {
                LoginModel finalArray = new Gson().fromJson(response,new TypeToken<LoginModel>(){}.getType());

                int status=finalArray.getCode();
                if (status == 200){
                    String user_id  = null;
                    user_id           = finalArray.getLoginUserDetails().get(0).getId();
                    String msg        =finalArray.getMessage();
                    String userName   =finalArray.getLoginUserDetails().get(0).getName();
                    String userMobile =finalArray.getLoginUserDetails().get(0).getMobile();
                   // String userEmail  =finalArray.getLoginUserDetails().get(0).getEmail();

                    // saving value of user id
                    Preferences.save(LoginActivity.this, Constants.USER_ID, user_id);
                    Preferences.save(LoginActivity.this, Constants.USER_TYPE, selectUser);
                    Preferences.save(LoginActivity.this, Constants.USER_CITY, finalArray.getLoginUserDetails().get(0).getCity());
                    Preferences.save(LoginActivity.this,Preferences.KEY_USER_NAME, userName);
                    Preferences.save(LoginActivity.this,Preferences.KEY_MOBILE, userMobile);
                    //Preferences.save(LoginActivity.this,Preferences.KEY_USER_EMAIL, userEmail);
                    Preferences.save(LoginActivity.this,Preferences.Skip_ID, "0");
                    Preferences.save(LoginActivity.this,Preferences.USER_TYPE, finalArray.getLoginUserDetails().get(0).getUserType());
                    String id =   Preferences.get(LoginActivity.this,Preferences.USER_TYPE);




                    Preferences.save(LoginActivity.this,Preferences.IS_PROFILECOMPLETE, finalArray.getLoginUserDetails().get(0).getIsProfileComplete());
                    Preferences.save(LoginActivity.this,Preferences.UserProfile, finalArray.getLoginUserDetails().get(0).getPIBytearray());
                    pd.dismiss();
                    Toast.makeText(this, "Login successfully", Toast.LENGTH_SHORT).show();
                    Intent in=new Intent(LoginActivity.this, MainActivity.class);
                    in.putExtra("isFromNavigation",true);
                    in.putExtra("userMobile",userMobile);

                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(in);
                    finish();
                    Preferences.save(LoginActivity.this, Constants.USER_ID, user_id);
                    String language = Preferences.get(this,Preferences.LANGUAGE);
                }
                else {
                    pd.dismiss();
                    String msg=finalArray.getMessage();
                    //  Toast.makeText(this, "Something wrong..", Toast.LENGTH_SHORT).show();
                    Toast.makeText(this, " "+msg, Toast.LENGTH_SHORT).show();
                }
            }
            else {
                pd.dismiss();
                Toast.makeText(this, "No internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            pd.dismiss();
            Toast.makeText(this, "No internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
