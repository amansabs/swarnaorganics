package project.sabs.android.com.swarnaorganics.UserProfile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;
import project.sabs.android.com.swarnaorganics.Home.CountyModel.Country;
import project.sabs.android.com.swarnaorganics.Home.CountyModel.GetCountryModel;
import project.sabs.android.com.swarnaorganics.Home.DistrictModel.District;
import project.sabs.android.com.swarnaorganics.Home.DistrictModel.GetDistrictModel;
import project.sabs.android.com.swarnaorganics.Home.MainActivity;
import project.sabs.android.com.swarnaorganics.Home.ReferralModelClass.GetReferralModel;
import project.sabs.android.com.swarnaorganics.Home.ReferralModelClass.SalesUser;
import project.sabs.android.com.swarnaorganics.Home.StateModel.GetStateModel;
import project.sabs.android.com.swarnaorganics.Home.StateModel.State;
import project.sabs.android.com.swarnaorganics.Home.UserProfile.UpdateModel;
import project.sabs.android.com.swarnaorganics.Home.UserProfile.User;
import project.sabs.android.com.swarnaorganics.Home.UserProfile.UserProfileModel;
import project.sabs.android.com.swarnaorganics.Preferences;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

import static project.sabs.android.com.swarnaorganics.SellerAddProduct.AddProductActivity.getOrientation;


public class EditUserProfile extends AppCompatActivity implements IApiResponse{
    TextView text_toolbar;
    RelativeLayout rr_back;
    String USER_ID;

    TextView txt_heading;
    //Country
    ArrayAdapter<Country> dataAdapter;
    ArrayList<Country> country_name=new ArrayList<Country>();
    ArrayList<Country> newCountryList=new ArrayList<Country>();
    Spinner countrySpinner ;
    String countryID ="";
    Boolean IsEdit= false;
    boolean IsRef = false;
    Locale myLocale;
    //State
    Spinner stateSpinner ;
    String stateId="";
    final ArrayList<State> state_name = new ArrayList<State>();
    ArrayAdapter<State> stateDataAdapter = null;
    ArrayList<State> newStateList = new ArrayList<State>();

    //District
    Spinner districtSpinner ;
    String districtId="";
    final ArrayList<District> district_name=new ArrayList<District>();
    ArrayList<District> newDistrict_name=new ArrayList<District>();
    ArrayAdapter<District> cityDataAdapter=null;

    Spinner referral_spinner1;
    final ArrayList<SalesUser> referral_name=new ArrayList<SalesUser>();
    ArrayList<SalesUser> newReferral_name=new ArrayList<SalesUser>();
    ArrayAdapter<SalesUser> referralDataAdapter=null;
    String referralId="";

    MenuItem nav_add_product,nav_manageProduct,nav_manageProfile,nav_logout,nav_login,nav_changePass;
    String shipId=null;
    String userType = null;
    TextView textViewUserType;
    TextView TxtMail;
    TextView txt_note;
    TextView txtUserName;
    Button cancel_btn,logout_btn,lang_cancel,lang_ok;
    CheckBox english,hindi;
    int isEnglishclick =0;
    int isHindiclick =0;
    WebView webView;
    ProgressDialog pd;


    EditText et_first_name;
    EditText et_address_name;
    EditText et__add_Two;
    EditText et_city;
    EditText et_pincode;
    EditText et_mobile;
    EditText et_email;
    String  userMobile;
    RelativeLayout save_layout;
    ArrayList<User> userProfile=new ArrayList<User>();
    ImageView edit_Profile;

    boolean Update = false;
    boolean IsGetValue = false;


    boolean isState = false;
    boolean isDistrict = false;
    boolean isRefrel = false;

    private static final int MY_CAMERA_REQUEST_CODE = 100;
    CircleImageView user_profile_image;
    CircleImageView mImageView_edit;
    CircleImageView mImageView_cancel;

    //insertImage
    //image from camera
    private Bitmap profile_bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;

    View header;
    Bitmap   bitmap;
    String Image = "";
    ImageView imageView;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_profile);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        text_toolbar= (TextView)findViewById(R.id.text_toolbar);
        rr_back= (RelativeLayout) findViewById(R.id.rr_back);

        text_toolbar.setText("Profile");

        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        USER_ID = Preferences.get(this, Constants.USER_ID);

        findViews();


        if(!Preferences.get(this,Preferences.USER_TYPE).equalsIgnoreCase("0")) {
            userType =   Preferences.get(this,Preferences.USER_TYPE);
           /* textViewUserType.setVisibility(View.VISIBLE);
            textViewUserType.setVisibility(View.VISIBLE);
            if (userType.equalsIgnoreCase("1")){
                textViewUserType.setText(getResources().getString(R.string.seller));
            }
            else  if (userType.equalsIgnoreCase("2")){
                textViewUserType.setText(getResources().getString(R.string.buyer));
            }*/
        }/*else{
            textViewUserType.setVisibility(View.VISIBLE);
            textViewUserType.setText("");
        }*/



        RelativeLayout user_profile = (RelativeLayout) findViewById(R.id.user_profile);
        user_profile.setVisibility(View.VISIBLE);
        save_layout.setVisibility(View.GONE);
      //  txt_heading.setText(getResources().getString(R.string.edit_profile));
        pd = new ProgressDialog(EditUserProfile.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);

        if( !Preferences.get(this,Preferences.USER_TYPE).equalsIgnoreCase(null)) {
            userMobile = Preferences.get(this,Preferences.KEY_MOBILE);
            et_mobile.setText(userMobile);
        }

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        }


        // getCountryListData();
        //  getStateListData();
        //  getDistrictListData();
        //MasterData();
        getUserProfileData(userMobile);
        if (IsEdit){


            //getCountryListData();

        }
        edit_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                IsEdit = false;
                IsRef = false;
                mImageView_edit.setVisibility(View.VISIBLE);
                mImageView_cancel.setVisibility(View.VISIBLE);
                referral_spinner1.setEnabled(true);
                districtSpinner.setEnabled(true);
                stateSpinner.setEnabled(true);
                countrySpinner.setEnabled(true);

                et_first_name.setEnabled(true);
                et_first_name.setClickable(true);

                et_address_name.setEnabled(true);
                et_address_name.setClickable(true);

                et__add_Two.setEnabled(true);
                et__add_Two.setClickable(true);

                et_city.setEnabled(true);
                et_city.setClickable(true);

                et_pincode.setEnabled(true);
                et_pincode.setClickable(true);

                et_email.setEnabled(true);
                et_email.setClickable(true);

                mImageView_edit.setEnabled(true);
                mImageView_edit.setClickable(true);

                mImageView_cancel.setEnabled(true);
                mImageView_cancel.setClickable(true);

                //  mImageView_edit.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.MULTIPLY);

                save_layout.setVisibility(View.VISIBLE);
                edit_Profile.setVisibility(View.GONE);

            }
        });


        save_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    /*referral_spinner1.setEnabled(false);
                    districtSpinner.setEnabled(false);
                    stateSpinner.setEnabled(false);
                    countrySpinner.setEnabled(false);

                    et_first_name.setEnabled(false);
                    et_first_name.setClickable(false);

                    et_address_name.setEnabled(false);
                    et_address_name.setClickable(false);

                    et__add_Two.setEnabled(false);
                    et__add_Two.setClickable(false);

                    et_city.setEnabled(false);
                    et_city.setClickable(false);

                    et_pincode.setEnabled(false);
                    et_pincode.setClickable(false);
                    edit_Profile.setVisibility(View.VISIBLE);*/

                Velidation();
            }
        });
        //refferalSelect();





    }

    private void findViews() {
       // txt_heading =(TextView)findViewById(R.id.txt_heading);
        txt_note =(TextView)findViewById(R.id.txt_note);
        countrySpinner = (Spinner) findViewById(R.id.spinnerCountry);
        stateSpinner = (Spinner) findViewById(R.id.spinnerState);
        districtSpinner = (Spinner) findViewById(R.id.spinnerDistrict);
        referral_spinner1 = (Spinner) findViewById(R.id.referral_spinner1);
        edit_Profile = (ImageView) findViewById(R.id.edit_Profile);
        save_layout = (RelativeLayout) findViewById(R.id.save_layout);


        et_first_name =(EditText)findViewById(R.id.et_first_name);
        et_address_name =(EditText)findViewById(R.id.et_address_name);
        et__add_Two =(EditText)findViewById(R.id.et__add_Two);
        et_city =(EditText)findViewById(R.id.et_city);
        et_pincode =(EditText)findViewById(R.id.et_pincode);
        et_mobile =(EditText)findViewById(R.id.et_mobile);
        et_email =(EditText)findViewById(R.id.et_email);
        user_profile_image =(CircleImageView)findViewById(R.id.user_profile_image);
        mImageView_edit =(CircleImageView)findViewById(R.id.mImageView_edit);
        mImageView_cancel =(CircleImageView)findViewById(R.id.mImageView_cancel);

        stateSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                if(countryID.equalsIgnoreCase("") || countryID.equalsIgnoreCase("0") ){
                    //getStateListData();
                    stateSpinner.setEnabled(false);
                    Toast.makeText(EditUserProfile.this, "Please select country first", Toast.LENGTH_SHORT).show();

                }
                return false;
            }

        });


        districtSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(countryID.equalsIgnoreCase("") || countryID.equalsIgnoreCase("0")){
                    //getStateListData();
                    districtSpinner.setEnabled(false);
                    Toast.makeText(EditUserProfile.this, "Please select country first", Toast.LENGTH_SHORT).show();
                }
                else if (stateId.equalsIgnoreCase("")|| stateId.equalsIgnoreCase("0"))

                {
                    districtSpinner.setEnabled(false);
                    Toast.makeText(EditUserProfile.this, "Please select state", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });


        mImageView_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectImage();

            }
        });

        mImageView_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bitmap = null;

                mImageView_cancel.setVisibility(View.GONE);
                user_profile_image.setImageResource(R.drawable.place_holder);


            }
        });
    }

    public void Velidation(){

        String userName = et_first_name.getText().toString();
        String userAdd = et_address_name.getText().toString();
        String userAddTwo = et__add_Two.getText().toString();
        String userCity = et_city.getText().toString();
        String userEmail = et_email.getText().toString();
        String userPinCode = et_pincode.getText().toString();

      /*  countryID
                districtId
        stateId
                referralId*/



        if (userName.equalsIgnoreCase("")){

            Toast.makeText(this, "Please Enter Name", Toast.LENGTH_SHORT).show();
        }
        else if(countryID.equalsIgnoreCase("") || countryID.equalsIgnoreCase("0")){
            //getStateListData();
            Toast.makeText(EditUserProfile.this, "Please Select Country First", Toast.LENGTH_SHORT).show();
        }
        else if(stateId.equalsIgnoreCase("") || stateId.equalsIgnoreCase("0")){
            //getStateListData();
            Toast.makeText(EditUserProfile.this, "Please Select State First", Toast.LENGTH_SHORT).show();
        }
        else if(districtId.equalsIgnoreCase("") || districtId.equalsIgnoreCase("0")){
            //getStateListData();
            Toast.makeText(EditUserProfile.this, "Please Select District First", Toast.LENGTH_SHORT).show();
        }
        else if (userAdd.equalsIgnoreCase("")){
            Toast.makeText(this, "Please Enter Address1", Toast.LENGTH_SHORT).show();
        }
        else if(userCity.equalsIgnoreCase("")){
            Toast.makeText(this, "Please Enter City", Toast.LENGTH_SHORT).show();
        }else if(userPinCode.length()<6){
            Toast.makeText(this, "Please Enter Correct Pincode", Toast.LENGTH_SHORT).show();
        }
        else if (!userEmail.equalsIgnoreCase("")){

            if(!isValidEmail(userEmail)){

                Toast.makeText(this, "Please Enter Correct Email", Toast.LENGTH_SHORT).show();
            }
            else if(referralId.equalsIgnoreCase("")){
                //getStateListData();
                Toast.makeText(EditUserProfile.this, "Please select referral", Toast.LENGTH_SHORT).show();
            }
            else {
                updateProfileData(userMobile,userName,countryID,stateId,districtId,userAdd,userCity,userPinCode,referralId,userEmail,userAddTwo);

            }

        }
        else if(referralId.equalsIgnoreCase("")){
            //getStateListData();
            Toast.makeText(EditUserProfile.this, "Please select referral", Toast.LENGTH_SHORT).show();
        }
        else {
            updateProfileData(userMobile,userName,countryID,stateId,districtId,userAdd,userCity,userPinCode,referralId,userEmail,userAddTwo);

        }

    }
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
    private void getCountryListData() {

        if (!IsEdit){
            pd.show();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        ApiRequest apiRequest = new ApiRequest(EditUserProfile.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_COUNTRY_LIST, Constants.GET_COUNTRY_LIST,map, Request.Method.POST);

    }

    private void getStateListData() {
        HashMap<String, String> map = new HashMap<>();
        if (!IsEdit){
            pd.show();
        }
        map.put("languageid","1");
        map.put("countryid",countryID);
        ApiRequest apiRequest = new ApiRequest(EditUserProfile.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_State_LIST, Constants.GET_State_LIST,map, Request.Method.POST);
    }

    private void getDistrictListData() {
        pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        map.put("stateid",stateId);
        ApiRequest apiRequest = new ApiRequest(EditUserProfile.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_DISTRICT_LIST, Constants.GET_DISTRICT_LIST, map, Request.Method.POST);
    }

    private void getReferralListData() {
        pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        ApiRequest apiRequest = new ApiRequest(EditUserProfile.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_REFERRAL_LIST, Constants.GET_REFERRAL_LIST, map, Request.Method.POST);
    }
    private void getUserProfileData(String userMobile) {
        pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("mobile",userMobile);
        map.put("usertype",userType);
        ApiRequest apiRequest = new ApiRequest(EditUserProfile.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_USER_PROFILE, Constants.GET_USER_PROFILE, map, Request.Method.POST);
    }
    private void updateProfileData(String userMobile,String  userName,String countryID,String stateId,String districtId,String userAdd,String userCity,String pin,String referralId,String userEmail,String userAddTwo) {
        pd.show();
        String   img="";
        if ( bitmap !=null){
            img = getFileDataFromDrawable1(bitmap);

        }




        HashMap<String, String> map = new HashMap<>();
        map.put("mobile",userMobile);
        map.put("usertype",userType);
        map.put("Name",userName);
        map.put("Country",countryID);
        map.put("State",stateId);
        map.put("District",districtId);
        map.put("Address1",userAdd);
        map.put("Email",userEmail);
        map.put("Address2",userAddTwo);
        map.put("City",userCity);
        map.put("Pin",pin);
        map.put("Referral",referralId);
        map.put("profileimage",img);
        map.put("userid",USER_ID);



        ApiRequest apiRequest = new ApiRequest(EditUserProfile.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.UPDATE_PROFILE, Constants.UPDATE_PROFILE, map, Request.Method.POST);


    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        //Country
        if(tag_json_obj.equalsIgnoreCase(Constants.GET_COUNTRY_LIST)){

            if(response != null) {
                pd.dismiss();
                GetCountryModel finalArray = new Gson().fromJson(response, new TypeToken<GetCountryModel>() {}.getType());
                newCountryList.clear();
                newCountryList = (ArrayList<Country>) finalArray.getCountries();
                for (int i = 0; i <newCountryList.size() ; i++) {
                    String country_id=newCountryList.get(i).getCountryId();
                    country_name.add(new Country(""+newCountryList.get(i).getCountryId(),""+newCountryList.get(i).getCountryName() ));
                }

                dataAdapter.notifyDataSetChanged();

                if(IsEdit) {
                    for (int i = 0; i < country_name.size(); i++) {
                        String cName = country_name.get(i).getCountryId();
                        if (cName.equalsIgnoreCase(countryID)) {
                            //countryID = country_name.get(i).getId();
                            countrySpinner.setSelection(i);
                            countryID = cName;
                            // dataAdapter.notifyDataSetChanged();
                        }
                    }
                }
                countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                        Object country_item = parent.getItemAtPosition(pos);

                        if (!IsEdit){
                            countryID ="";
                            stateId="";
                            districtId ="";
                            countryID=country_name.get(pos).getCountryId();
                        }



                        if(countryID != null && !countryID.equalsIgnoreCase("0") && !countryID.equalsIgnoreCase("")){
                            state_name.clear();
                            state_name.add(new State("0", "Select"));
                            stateDataAdapter.notifyDataSetChanged();
                            getStateListData();

                            if (!IsEdit) {
                                stateSpinner.setEnabled(true);
                                district_name.clear();
                                district_name.add(new District("0", "Select"));
                                cityDataAdapter.notifyDataSetChanged();
                                getDistrictListData();
                                districtSpinner.setEnabled(true);
                            }

                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

            }
            else{
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        //State
        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_State_LIST)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                GetStateModel stateArray = new Gson().fromJson(response, new TypeToken<GetStateModel>(){}.getType());

                int code = stateArray.getCode();
                if (code == 200){
                    newStateList.clear();
                    newStateList = (ArrayList<State>) stateArray.getStates();
                    for (int i = 0; i <newStateList.size() ; i++) {
                        state_name.add(new State(""+newStateList.get(i).getStateId(),""+newStateList.get(i).getStateName()));
                    }
                    stateDataAdapter.notifyDataSetChanged();

                    if(IsEdit) {
                        for (int i = 0; i <state_name.size() ; i++) {
                            String sName = state_name.get(i).getStateId();
                            if (sName.equalsIgnoreCase(stateId)) {
                                //   stateId = state_name.get(i).getState_id();
                                stateSpinner.setSelection(i);
                            }
                        }
                    }


                    stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                            Object state_item = parent.getItemAtPosition(pos);
                            if (!IsEdit){
                                stateId = "";
                                districtId = "";
                                stateId= state_name.get(pos).getStateId();
                            }


                            if(stateId != null && !stateId.equalsIgnoreCase("0") && !stateId.equalsIgnoreCase("")) {

                                district_name.clear();
                                district_name.add(new District("0", "Select"));
                                cityDataAdapter.notifyDataSetChanged();
                                if (!IsEdit){
                                    districtSpinner.setEnabled(true);
                                }
                                getDistrictListData();


                            }
                        }
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }


            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
        //City
        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_DISTRICT_LIST)){
            // city list api
            if(response != null) {
                GetDistrictModel cityArray = new Gson().fromJson(response, new TypeToken<GetDistrictModel>(){}.getType());
                pd.dismiss();
                int code = cityArray.getCode();
                if (code == 200){

                    newDistrict_name.clear();
                    newDistrict_name = (ArrayList<District>) cityArray.getDistrict();
                    // final ArrayList<CityData> city_name=new ArrayList<CityData>();

                    //  ArrayAdapter<CityData> cityDataAdapter=null;
                    if(newDistrict_name != null) {
                        for (int i = 0; i < newDistrict_name.size(); i++) {
                            district_name.add(new District("" + newDistrict_name.get(i).getDistrictId(), "" + newDistrict_name.get(i).getDistrictName()));
                        }
                    }
                    cityDataAdapter.notifyDataSetChanged();

                    if(IsEdit) {
                        for (int i = 0; i <district_name.size() ; i++) {
                            String ctyName = district_name.get(i).getDistrictId();
                            if (ctyName.equalsIgnoreCase(districtId)) {
                                // cityId = city_name.get(i).getCity_id();
                                districtSpinner.setSelection(i);

                                cityDataAdapter.notifyDataSetChanged();


                            }
                        }
                    }
                    cityDataAdapter.notifyDataSetChanged();
                /*cityDataAdapter = new ArrayAdapter<CityData>(this,android.R.layout.simple_spinner_item, city_name);
                cityDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                citySpinner.setAdapter(cityDataAdapter);*/

                    districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                            Object id_num = parent.getItemIdAtPosition(pos);

                            if (countryID.equalsIgnoreCase("0"))
                            {
                                Toast.makeText(EditUserProfile.this, "please enter city", Toast.LENGTH_SHORT).show();
                            }
                            if (!IsEdit){
                                districtId= district_name.get(pos).getDistrictId();
                            }

                            // Toast.makeText(MainActivity.this, ""+districtId, Toast.LENGTH_SHORT).show();
                        }
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                }



            }else{
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
        else if(tag_json_obj.equalsIgnoreCase(Constants.GET_REFERRAL_LIST)){

            if(response != null) {
                pd.dismiss();
                GetReferralModel finalArray = new Gson().fromJson(response, new TypeToken<GetReferralModel>() {}.getType());
                newReferral_name.clear();
                newReferral_name = (ArrayList<SalesUser>) finalArray.getSalesUser();

                for (int i = 0; i <newReferral_name.size() ; i++) {
                    String country_id=newReferral_name.get(i).getSalesUserId();
                    referral_name.add(new SalesUser(""+newReferral_name.get(i).getSalesUserId(),""+newReferral_name.get(i).getSalesUserName() ));
                }


                if(IsRef) {
                    for (int i = 0; i <referral_name.size() ; i++) {
                        String cName = referral_name.get(i).getSalesUserId();
                        if (cName.equalsIgnoreCase(referralId)) {
                            //countryID = country_name.get(i).getId();
                            referral_spinner1.setSelection(i);
                        }
                    }
                }
                referralDataAdapter.notifyDataSetChanged();

                referral_spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        Object country_item = parent.getItemAtPosition(pos);
                        if (!IsRef){
                            referralId=referral_name.get(pos).getSalesUserId();
                        }

                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


            }
            else{
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
        //State
        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_USER_PROFILE)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                UserProfileModel finalArray = new Gson().fromJson(response, new TypeToken<UserProfileModel>(){}.getType());
                userProfile = (ArrayList<User>) finalArray.getUser();

                int code = finalArray.getCode();
                if (code==200){

                    IsEdit = true;
                    IsRef = true;
                    String IsProfileComplete = finalArray.getUser().get(0).getIsProfileComplete();

                    String img = null;

                    if (IsProfileComplete.equalsIgnoreCase("1"))
                    {
                        et_first_name.setText(finalArray.getUser().get(0).getName());
                        et_address_name.setText(finalArray.getUser().get(0).getAddress1());
                        et__add_Two.setText(finalArray.getUser().get(0).getAddress2());
                        et_city.setText(finalArray.getUser().get(0).getCity());
                        et_pincode.setText(finalArray.getUser().get(0).getPinCode());
                        et_email.setText(finalArray.getUser().get(0).getEmail());
                        img = (String) finalArray.getUser().get(0).getPIBytearray();
                    }

                    if (finalArray.getUser().get(0).getCountry() == null || finalArray.getUser().get(0).getState() ==null ||  finalArray.getUser().get(0).getDistrict() ==null ||  finalArray.getUser().get(0).getAddress1()==null){

                        /*nav_manageProduct.setVisible(false);
                        nav_add_product.setVisible(false);*/
                        txt_note.setVisibility(View.VISIBLE);

                    }
                    else {

                        /*nav_manageProduct.setVisible(true);
                        nav_add_product.setVisible(true);*/
                        txt_note.setVisibility(View.GONE);
                    }



                    if (img!=null){
                        Bitmap bitmap1 = StringToBitMap(img);

                        user_profile_image.setImageBitmap(bitmap1);
                        bitmap = bitmap1;
                       // imageView.setImageBitmap(bitmap1);
                        // Picasso.with(AddProductActivity.this).load(certificate).into(img_certificate);
                    }
                    else {
                        user_profile_image.setImageResource(R.drawable.place_holder);
                       // imageView.setImageResource(R.drawable.place_holder);
                    }

                    et_mobile.setText(finalArray.getUser().get(0).getMobile());
                    // String country_id =
                    // stateId =
                    // districtId =  finalArray.getUser().get(0).getDistrict();
                    // et_email.setText(finalArray.getUser().get(0).get);
                    //Toast.makeText(this, "GetData", Toast.LENGTH_SHORT).show();

                    countryID =finalArray.getUser().get(0).getCountry();

                    if (countryID==null){
                        countryID="0";
                    }
                    else {

                    }
                    stateId = finalArray.getUser().get(0).getState();
                    if (stateId==null){
                        stateId="0";
                    }
                    else {

                    }
                    districtId =  finalArray.getUser().get(0).getDistrict();
                    if (districtId==null){
                        districtId="0";
                    }
                    else {

                    }
                    referralId = finalArray.getUser().get(0).getRefId();

                    if (referralId==null){
                        referralId="0";
                    }
                    else {

                    }

                    MasterData();
                    getReferralListData();

                    getCountryListData();

                    /*for (int i = 0; i <district_name.size() ; i++) {
                        String dName = district_name.get(i).getDistrictId();

                        if (dName.equalsIgnoreCase(dName)) {
                            //countryID = country_name.get(i).getId();
                            districtSpinner.setSelection(i);
                            districtId = dName;
                            cityDataAdapter.notifyDataSetChanged();

                        }

                    }*/

                }

                else {

                    Toast.makeText(this, "please edit your profile", Toast.LENGTH_SHORT).show();
                }



            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        //State
        else if (tag_json_obj.equalsIgnoreCase(Constants.UPDATE_PROFILE)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                UpdateModel finalArray = new Gson().fromJson(response, new TypeToken<UpdateModel>(){}.getType());


                int code = finalArray.getCode();
                String msg = finalArray.getMessage();
                if (code==200){
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();

                    IsEdit =true;
                    IsRef = true;

                    getUserProfileData(userMobile);


                    // getReferralListData();
                    // getCountryListData();
                    referral_spinner1.setEnabled(false);
                    districtSpinner.setEnabled(false);
                    stateSpinner.setEnabled(false);
                    countrySpinner.setEnabled(false);
                    et_first_name.setEnabled(false);
                    et_first_name.setClickable(false);
                    et_address_name.setEnabled(false);
                    et_address_name.setClickable(false);
                    et__add_Two.setEnabled(false);
                    et__add_Two.setClickable(false);
                    et_city.setEnabled(false);
                    et_city.setClickable(false);
                    et_pincode.setEnabled(false);
                    et_pincode.setClickable(false);

                    et_email.setEnabled(false);
                    et_email.setClickable(false);

                    mImageView_edit.setVisibility(View.GONE);
                    mImageView_edit.setEnabled(false);
                    mImageView_edit.setClickable(false);

                    mImageView_cancel.setVisibility(View.GONE);
                    mImageView_cancel.setEnabled(false);
                    mImageView_cancel.setClickable(false);

                    // mImageView_edit.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.cb_dark_grey), android.graphics.PorterDuff.Mode.MULTIPLY);


                    edit_Profile.setVisibility(View.VISIBLE);
                    save_layout.setVisibility(View.GONE);

                }

                else {

                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                }

            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        else {
            Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    private void MasterData(){
        //Country
        countrySpinner.setEnabled(true);
        country_name.clear();
        country_name.add(new Country("0","Select"));
        dataAdapter = new ArrayAdapter<Country>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,country_name);
        // drinkAdapter = new ArrayAdapter<Drink>(getActivity(), android.R.layout.simple_spinner_dropdown_item, drinks);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countrySpinner.setAdapter(dataAdapter);
        countrySpinner.setEnabled(false);

        //GetState
        state_name.clear();
        state_name.add(new State("0","Select"));
        stateDataAdapter = new ArrayAdapter<State>(this,android.R.layout.simple_spinner_item, state_name);
        stateDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stateSpinner.setAdapter(stateDataAdapter);
        stateSpinner.setEnabled(false);

        //district
        district_name.clear();
        district_name.add(new District("0", "Select"));
        cityDataAdapter = new ArrayAdapter<District>(this,android.R.layout.simple_spinner_item, district_name);
        cityDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        districtSpinner.setAdapter(cityDataAdapter);
        districtSpinner.setEnabled(false);
        //Referral
        referral_name.clear();
        referral_name.add(new SalesUser("0","Self"));
        referralDataAdapter = new ArrayAdapter<SalesUser>(this,android.R.layout.simple_spinner_item, referral_name);
        referralDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        referral_spinner1.setAdapter(referralDataAdapter);
        referral_spinner1.setEnabled(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }


    // Select image from camera and gallery
    @SuppressLint("NewApi")
    private void selectImage() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());

            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;

        if (requestCode == PICK_IMAGE_CAMERA) {


            if (data!= null){
                try {
                    Uri selectedImage = data.getData();
                    bitmap = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    // bitmap = getResizedBitmap(bitmap, 200);
                    Log.e("Activity", "Pick from Camera::>>> ");

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                    destination = new File(Environment.getExternalStorageDirectory() + "/" +
                            getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    user_profile_image.setImageBitmap(bitmap);
                    // certi_bitmap = bitmap;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
            }


        } else if (requestCode == PICK_IMAGE_GALLERY) {

            if (data!= null){

                Uri selectedImage = data.getData();
                try {
                    bitmap = scaleImage(this,selectedImage);
                    //  bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);


                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                    //  bitmap = getResizedBitmap(bitmap, 200);

                    Log.e("Activity", "Pick from Gallery::>>> ");

                    user_profile_image.setImageBitmap(bitmap);
                    //certi_bitmap = bitmap;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String getFileDataFromDrawable1(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
        byte[] imaBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imaBytes, Base64.DEFAULT);
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static Bitmap scaleImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > 512 || rotatedHeight > 512) {
            float widthRatio = ((float) rotatedWidth) / ((float) 512);
            float heightRatio = ((float) rotatedHeight) / ((float) 512);
            float maxRatio = Math.max(widthRatio, heightRatio);

            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

        /*
         * if the orientation is not 0 (or -1, which means we don't know), we
         * have to do a rotation.
         */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        }

        String type = context.getContentResolver().getType(photoUri);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (type.equals("image/png")) {
            srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        } else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
            srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }
        byte[] bMapArray = baos.toByteArray();
        baos.close();
        return BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);
    }

}
