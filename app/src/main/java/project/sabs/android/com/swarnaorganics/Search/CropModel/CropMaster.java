
package project.sabs.android.com.swarnaorganics.Search.CropModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CropMaster {

    @SerializedName("CropmasterId")
    @Expose
    private String cropmasterId;

    public CropMaster(String cropmasterId, String cropMasterName) {
        this.cropmasterId = cropmasterId;
        this.cropMasterName = cropMasterName;
    }

    @SerializedName("CropMasterName")
    @Expose

    private String cropMasterName;

    public String getCropmasterId() {
        return cropmasterId;
    }

    public void setCropmasterId(String cropmasterId) {
        this.cropmasterId = cropmasterId;
    }

    public String getCropMasterName() {
        return cropMasterName;
    }

    public void setCropMasterName(String cropMasterName) {
        this.cropMasterName = cropMasterName;
    }


    public String toString() {
        return getCropMasterName();
    }
}
