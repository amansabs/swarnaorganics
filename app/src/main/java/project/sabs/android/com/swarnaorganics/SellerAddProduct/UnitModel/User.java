
package project.sabs.android.com.swarnaorganics.SellerAddProduct.UnitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("UnitId")
    @Expose
    private String unitId;
    @SerializedName("Unit Name")
    @Expose
    private String unitName;

    public User(String unitId, String unitName) {
        this.unitId = unitId;
        this.unitName = unitName;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    @Override
    public String toString() {
        return getUnitName();
    }



}
