
package project.sabs.android.com.swarnaorganics.BuyerAddProduct.ProductModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductListModel {

    @SerializedName("Product List")
    @Expose
    private List<BProductList> productList = null;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;

    public List<BProductList> getProductList() {
        return productList;
    }

    public void setProductList(List<BProductList> productList) {
        this.productList = productList;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
