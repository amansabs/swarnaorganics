
package project.sabs.android.com.swarnaorganics.Search.SubCropModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CropDetail {

    @SerializedName("CropdetailId")
    @Expose
    private String cropdetailId;

    public CropDetail(String cropdetailId, String cropdetailName) {
        this.cropdetailId = cropdetailId;
        this.cropdetailName = cropdetailName;
    }

    @SerializedName("CropdetailName")
    @Expose


    private String cropdetailName;

    public String getCropdetailId() {
        return cropdetailId;
    }

    public void setCropdetailId(String cropdetailId) {
        this.cropdetailId = cropdetailId;
    }

    public String getCropdetailName() {
        return cropdetailName;
    }

    public void setCropdetailName(String cropdetailName) {
        this.cropdetailName = cropdetailName;
    }
    @Override
    public String toString() {
        return getCropdetailName();
    }
}
