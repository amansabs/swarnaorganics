
package project.sabs.android.com.swarnaorganics.Login;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginModel {

    @SerializedName("Login User Details")
    @Expose
    private List<LoginUserDetail> loginUserDetails = null;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;

    public List<LoginUserDetail> getLoginUserDetails() {
        return loginUserDetails;
    }

    public void setLoginUserDetails(List<LoginUserDetail> loginUserDetails) {
        this.loginUserDetails = loginUserDetails;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
