package project.sabs.android.com.swarnaorganics;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import io.paperdb.Paper;
import project.sabs.android.com.swarnaorganics.Helper.LocaleHelper;
import project.sabs.android.com.swarnaorganics.Home.MainActivity;
import project.sabs.android.com.swarnaorganics.Login.LoginActivity;
import project.sabs.android.com.swarnaorganics.Volley.Constants;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;
    Context mContext;
    String language;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mContext = this;
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        /*language = Preferences.get(SplashActivity.this,Preferences.LANGUAGE);
        if (language.equalsIgnoreCase("0") ||language.equalsIgnoreCase("") ){
            language = "hi";
            Preferences.save(SplashActivity.this,  Preferences.LANGUAGE, language);

        }
        else {

            setLocale(language);
        }*/
        mContext = this;

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

                String userid = Preferences.get(SplashActivity.this, Constants.USER_ID);

                if (userid == null || userid.equalsIgnoreCase("")||userid.equalsIgnoreCase("0")) {

                    Intent intent = new Intent(mContext, LoginActivity.class);
                    startActivity(intent);
                    finish();
                   // setLocale(language);
                }else {
                    Intent intent = new Intent(mContext, MainActivity.class);
                    startActivity(intent);
                    finish();
                  //  setLocale(language);
                }
                // close this activity
                //finish();
            }
        }, SPLASH_TIME_OUT);
    }

   public void setLocale(String lang) {
    Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        // Intent refresh = new Intent(this, MainActivity.class);
        // startActivity(refresh);
        // finish();


    }
}
