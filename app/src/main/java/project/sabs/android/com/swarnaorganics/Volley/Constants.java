package project.sabs.android.com.swarnaorganics.Volley;

/**
 * Created by HP on 25-02-2016.
 */
public class Constants {

    //-------------Shrimal bazar code---------
    public static String USER_ID = "userid";
    public static String USER_TYPE = "usertype";
    public static String USER_CITY = "usercity";
    public static String BASE_URL = "https://swarna-kreta-vikreta.com/api/";
    // public static String BASE_URL = "http://www.swarna-kreta-vikreta.com/testapi/";
    public static String IMAGE_URL = "https://swarna-kreta-vikreta.com/";
    public static String GET_LOGIN = "user/login";
    public static String USER_SINGUP = "user/getOTP";
    public static String USER_VERIFY = "User/verifyOTP";
    public static String GET_COUNTRY_LIST = "common/getCountries";
    public static String GET_State_LIST = "common/getStates";
    public static String GET_DISTRICT_LIST = "common/getDistrict";
    public static String GET_REFERRAL_LIST = "common/getSalesUser";
    public static String GET_MASTER_CROP_LIST = "common/getCropMaster";
    public static String GET_SUB_CROP_LIST = "common/getCropDetails";
    public static String GET_UNIT = "common/getUnit";
    public static String GET_USER_PROFILE = "User/getUserProfile";
    public static String UPDATE_PROFILE = "User/updateProfile";
    //public static String SEARCH_LIST = "search/searchProduct";
    public static String SEARCH_LIST = "search1/searchProduct";
    public static String CHANGE_PASSWORD = "User/resetPassword";
    public static String FORGOT_PASSWORD = "User/getOTPForPassword";
    public static String FORGOT_OTPVERIFY = "User/verifyOTPForPassword";
    public static String ADD_PRODUCT = "product/addProduct";
    public static String GET_PRODUCT = "product/getProductList";
    public static String EDIT_PRODUCT = "product/getProduct";
    public static String DELETE_PRODUCT = "product/deleteProduct";
    public static String UPDATE_PRODUCT = "product/updateProduct";
    public static String GET_PRODUCT_IMAGE = "product/getProductImage";
    public static String GET_DEALER = "common/getDealers";



    public static String LANGUAGE = "hi";
    public static String GET_USER_DETAIL = "getUserDetail";
    public static String UPDATE_USER_DETAIL = "updateProfile";
    public static String GET_LANGUAGE = "getLanguages";
    public static String SELECT_LANGUAGE = "selectLanguage";
    public static String GET_HOMEDATA = "getHomeData";
    public static String ADD_FAVOURITE = "addFavourite";
    public static String GET_FAVOURITE = "getFavouriteList";
    public static String GET_CATEGORY = "getCategoryList";
    public static String CATEGORY_ID = "categoryid";
    public static String CATEGORY_NAME = "categoryname";
    public static String TYPE = "type";
    public static String LANGUAGE_ID = "languageid";
    public static String LANGUAGE_NOT_SELECTED = "languagenotselected";
    public static String GET_VIMEO = "/config";
    public static String ADD_VIEW_COUNT = "addViewCount";
    public static String PAY_FOR_CHANNEL = "http://139.59.69.59/ebaba/webservice/letsPayForChannel";


}
