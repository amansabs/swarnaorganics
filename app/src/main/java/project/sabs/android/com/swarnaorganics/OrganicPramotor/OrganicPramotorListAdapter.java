package project.sabs.android.com.swarnaorganics.OrganicPramotor;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.R;

import static org.apache.commons.lang3.StringUtils.capitalize;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class OrganicPramotorListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<Dealer> modelList;
    public static OrganicPramotorListAdapter objSearchListAdapter;
    // private ArrayList<Dealer> addToCartModelList =new ArrayList<Dealer>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;


    public OrganicPramotorListAdapter(Context context, ArrayList<Dealer> modelList ) {
        this.mContext = context;
        this.modelList = modelList;
        this.modelList = modelList;
        this.gridview = gridview;
        objSearchListAdapter = this;
    }


    public void updateList(ArrayList<Dealer> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_pramotor_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final Dealer model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;

            String add =  WordUtils.capitalizeFully(model.getAddress()) ;
            genericViewHolder.address_txt. setText( add +","+model.getPincode());


            genericViewHolder.mobile_txt.setText(model.getMobile());

            String name =  WordUtils.capitalizeFully(model.getDealerName()) ;
            genericViewHolder.user_name_title.setText(name);


            if (model.getEmail()!= null && ! model.getEmail().equals("")){
                genericViewHolder.txt_email.setVisibility(View.VISIBLE);
                genericViewHolder.txt_email.setText((CharSequence) model.getEmail());
            }

            if (model.getOPName()!= null && ! model.getOPName().equals("")){
                String company = capitalize(model.getOPName());
                genericViewHolder.txt_shopName.setText(company);
            }

            genericViewHolder.call_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String dail = model.getMobile();
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + dail));
                    mContext.startActivity(intent);

                }
            });


            genericViewHolder.rr_contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    LatLng sfas =  (getLocationFromAddress(mContext,model.getAddress()));

                    String shop = model.getOPName();


                    if (sfas!= null && !sfas.equals("")){

                        double lat= Double.parseDouble(String.valueOf(sfas.latitude));
                        double longe = Double.parseDouble(String.valueOf(sfas.longitude));

                        String geoUri = "http://maps.google.com/maps?q=loc:" + lat + "," + longe + " (" + shop + ")";
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                        mContext.startActivity(intent);


                        /*Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("http://maps.google.com/maps?daddr="+lat +","+longe+""+shop));
                        mContext.startActivity(intent);*/
                    }
                    else {
                        Toast.makeText(mContext, "No address found", Toast.LENGTH_SHORT).show();
                    }

                }
            });



            setAnimation(holder.itemView, position);
        }
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }




    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private Dealer getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, Dealer model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView call_img;
        private TextView user_name_title;
        private TextView mobile_txt;
        private TextView address_txt;
        private TextView txt_shopName;
        private TextView txt_email;
        private RelativeLayout rr_contact;
        public ViewHolder(final View itemView) {
            super(itemView);

            this.call_img = (ImageView) itemView.findViewById(R.id.call_img);
            this.user_name_title = (TextView) itemView.findViewById(R.id.user_name_title);
            this.address_txt = (TextView) itemView.findViewById(R.id.address_txt);
            this.mobile_txt = (TextView) itemView.findViewById(R.id.mobile_txt);
            this.txt_shopName = (TextView) itemView.findViewById(R.id.txt_shopName);
            this.txt_email = (TextView) itemView.findViewById(R.id.txt_email);
            this.rr_contact = (RelativeLayout) itemView.findViewById(R.id.rr_contact);
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));
                }
            });*/

        }

    }

}

