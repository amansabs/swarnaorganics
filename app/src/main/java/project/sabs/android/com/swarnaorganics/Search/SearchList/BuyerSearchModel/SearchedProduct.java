
package project.sabs.android.com.swarnaorganics.Search.SearchList.BuyerSearchModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchedProduct {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("CropMasterName")
    @Expose
    private String cropMasterName;
    @SerializedName("SubCrop")
    @Expose
    private String subCrop;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("UnitPrice")
    @Expose
    private String unitPrice;
    @SerializedName("Comments")
    @Expose
    private String comments;
    @SerializedName("IsOrganic")
    @Expose
    private String isOrganic;
    @SerializedName("WantUsToContact")
    @Expose
    private String wantUsToContact;
    @SerializedName("RegisteredWith")
    @Expose
    private String registeredWith;
    @SerializedName("RegistrationNumber")
    @Expose
    private String registrationNumber;
    @SerializedName("OtherOrg")
    @Expose
    private String otherOrg;
    @SerializedName("ShowPrice")
    @Expose
    private String showPrice;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("ImagePath")
    @Expose
    private Object imagePath;
    @SerializedName("UOM")
    @Expose
    private String uOM;
    @SerializedName("Other")
    @Expose
    private String other;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCropMasterName() {
        return cropMasterName;
    }

    public void setCropMasterName(String cropMasterName) {
        this.cropMasterName = cropMasterName;
    }

    public String getSubCrop() {
        return subCrop;
    }

    public void setSubCrop(String subCrop) {
        this.subCrop = subCrop;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getIsOrganic() {
        return isOrganic;
    }

    public void setIsOrganic(String isOrganic) {
        this.isOrganic = isOrganic;
    }

    public String getWantUsToContact() {
        return wantUsToContact;
    }

    public void setWantUsToContact(String wantUsToContact) {
        this.wantUsToContact = wantUsToContact;
    }

    public String getRegisteredWith() {
        return registeredWith;
    }

    public void setRegisteredWith(String registeredWith) {
        this.registeredWith = registeredWith;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getOtherOrg() {
        return otherOrg;
    }

    public void setOtherOrg(String otherOrg) {
        this.otherOrg = otherOrg;
    }

    public String getShowPrice() {
        return showPrice;
    }

    public void setShowPrice(String showPrice) {
        this.showPrice = showPrice;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Object getImagePath() {
        return imagePath;
    }

    public void setImagePath(Object imagePath) {
        this.imagePath = imagePath;
    }

    public String getUOM() {
        return uOM;
    }

    public void setUOM(String uOM) {
        this.uOM = uOM;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

}
