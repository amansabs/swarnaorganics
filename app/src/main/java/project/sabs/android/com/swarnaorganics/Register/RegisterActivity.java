package project.sabs.android.com.swarnaorganics.Register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;

import android.os.Bundle;

import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import project.sabs.android.com.swarnaorganics.Login.LoginActivity;
import project.sabs.android.com.swarnaorganics.Otp.GetOtpActivity;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.TermsAndCondition;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

public class RegisterActivity extends AppCompatActivity implements IApiResponse{
    TextView txt_login;
    TextView text_toolbar;
    RelativeLayout rr_back;
    ImageView repeat_show_pass_img;
    EditText repeat_password;
    String password;
    boolean isShow = false;
    CardView card_getOtp;
    boolean isRepeatShow = false;
    String Repeatpassword;
    EditText password_etxt;
    EditText mobile_etxt;
    ImageView show_pass_img;
    String  mobileNum;
    private RadioGroup registerUserTypeRadioGroup;
    String selectUser = null;
    ProgressDialog pd;
    TextView terms_pdf_txt;
    CheckBox terms_privecy_checkbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        findViews();
        text_toolbar.setText("Register");
        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(in);
                finish();
            }
        });

        repeat_show_pass_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Repeatpassword =repeat_password.getText().toString();
                if (!Repeatpassword.equalsIgnoreCase("")){
                    if (isShow){
                        isShow = false;
                        repeat_show_pass_img.setBackgroundResource(R.drawable.hide_pass);
                        repeat_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                    }else {
                        isShow = true;
                        repeat_show_pass_img.setBackgroundResource(R.drawable.show_pass);
                        repeat_password.setInputType(InputType.TYPE_CLASS_TEXT);
                    }
                }
                else {
                    Toast.makeText(RegisterActivity.this, "please enter password", Toast.LENGTH_SHORT).show();
                }
            }
        });

        show_pass_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                password =password_etxt.getText().toString();
                if (!password.equalsIgnoreCase("")){
                    if (isRepeatShow){
                        isRepeatShow = false;
                        show_pass_img.setBackgroundResource(R.drawable.hide_pass);
                        password_etxt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                    }else {
                        isRepeatShow = true;
                        show_pass_img.setBackgroundResource(R.drawable.show_pass);
                        password_etxt.setInputType(InputType.TYPE_CLASS_TEXT);
                    }
                }
                else {
                    Toast.makeText(RegisterActivity.this, "please enter password", Toast.LENGTH_SHORT).show();
                }
            }
        });


        registerUserTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                switch (checkedId) {
                    case R.id.BuyerTypeRadioButton:
                        selectUser = "2";
                        // Toast.makeText(LoginActivity.this, ""+checkedRadioButton.getText(), Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.SellerTypeRadioButton2:
                        selectUser = "1";
                        // Toast.makeText(LoginActivity.this, ""+checkedRadioButton.getText(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });



        terms_pdf_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(RegisterActivity.this, TermsAndCondition.class);
                startActivity(in);

            }
        });

        card_getOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });

    }

    private void findViews() {

        repeat_password= (EditText) findViewById(R.id.repeat_password);
        terms_privecy_checkbox= (CheckBox) findViewById(R.id.terms_privecy_checkbox);
        txt_login= (TextView)findViewById(R.id.txt_login);
        text_toolbar= (TextView)findViewById(R.id.text_toolbar);
        terms_pdf_txt= (TextView)findViewById(R.id.terms_pdf_txt);
        rr_back= (RelativeLayout) findViewById(R.id.rr_back);
        repeat_show_pass_img= (ImageView) findViewById(R.id.repeat_show_pass_img);
        show_pass_img= (ImageView) findViewById(R.id.show_pass_img);
        password_etxt= (EditText) findViewById(R.id.password);
        mobile_etxt= (EditText) findViewById(R.id.email_etxt);
        card_getOtp= (CardView) findViewById(R.id.card_getOtp);
        registerUserTypeRadioGroup=(RadioGroup)findViewById(R.id.registerUserTypeRadioGroup);

    }
    private  void validation(){
        mobileNum = mobile_etxt.getText().toString();
        //passwordEt  = password_etxt.getText().toString();
        password =password_etxt.getText().toString();
        Repeatpassword =repeat_password.getText().toString();

        if(mobileNum.length() < 10){
            Toast.makeText(RegisterActivity.this, "Please enter correct mobile number.", Toast.LENGTH_SHORT).show();
        }else if(password.equalsIgnoreCase("")){
            Toast.makeText(RegisterActivity.this, "Please enter password.", Toast.LENGTH_SHORT).show();
        }else if(Repeatpassword.equalsIgnoreCase("")){
            Toast.makeText(RegisterActivity.this, "Please repeat password.", Toast.LENGTH_SHORT).show();
        }else if(!password.equalsIgnoreCase(Repeatpassword)){
            Toast.makeText(RegisterActivity.this, "Password not match", Toast.LENGTH_SHORT).show();
        }
        else if (selectUser==null){
            Toast.makeText(RegisterActivity.this, "Please select user type..", Toast.LENGTH_SHORT).show();
        }
        else if (terms_privecy_checkbox.isChecked()==false){

            Toast.makeText(RegisterActivity.this, "Please accept terms and condition", Toast.LENGTH_SHORT).show();
        }
        else {
            Register();
            pd = new ProgressDialog(RegisterActivity.this,R.style.AppCompatAlertDialogStyle);
            pd.setMessage("loading");
            pd.setCancelable(false);
            pd.show();
           /* Intent in = new Intent(RegisterActivity.this, GetOtpActivity.class);
            startActivity(in);*/
        }
    }
    private void Register(){
        HashMap<String, String> map = new HashMap<>();
        map.put("mobile",mobileNum);
        map.put("password",password);
        map.put("usertype",selectUser);
        ApiRequest apiRequest = new ApiRequest(RegisterActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.USER_SINGUP, Constants.USER_SINGUP,map, Request.Method.POST);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if(tag_json_obj.equalsIgnoreCase(Constants.USER_SINGUP)) {
            if(response != null) {
                SignupModel finalArray = new Gson().fromJson(response,new TypeToken<SignupModel>(){}.getType());
                int status=finalArray.getCode();
                if (status == 200){
                    String msg        =finalArray.getMessage();
                    // saving value of user id
                    pd.dismiss();
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                    Intent in = new Intent(RegisterActivity.this, GetOtpActivity.class);
                    in.putExtra("mobile",mobileNum);
                    in.putExtra("password",password);
                    in.putExtra("usertype",selectUser);
                    in.putExtra("Otp","200");
                    startActivity(in);
                }
                else {
                    pd.dismiss();
                    String msg=finalArray.getMessage();
                    if (msg.equalsIgnoreCase("OTP already sent in last 20 mins")){
                        Intent in = new Intent(RegisterActivity.this, GetOtpActivity.class);
                        in.putExtra("mobile",mobileNum);
                        in.putExtra("password",password);
                        in.putExtra("usertype",selectUser);
                        in.putExtra("Otp","400");
                        startActivity(in);
                    }
                    //  Toast.makeText(this, "Something wrong..", Toast.LENGTH_SHORT).show();
                    Toast.makeText(this, " "+msg, Toast.LENGTH_SHORT).show();
                }
            }
            else {
                pd.dismiss();
                Toast.makeText(this, "No internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            pd.dismiss();
            Toast.makeText(this, "No internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, ""+error, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        mobileNum ="";
        password ="";
        super.onRestart();
    }
}
