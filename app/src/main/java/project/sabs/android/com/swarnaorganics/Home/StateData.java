
package project.sabs.android.com.swarnaorganics.Home;

public class StateData {

   String stateName;
    String state_id;



    @Override
    public String toString() {
        return stateName;
    }

    public StateData(String stateName, String state_id) {
        this.stateName = stateName;
        this.state_id = state_id;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }
}
