
package project.sabs.android.com.swarnaorganics.Home;

public class ReferralModel {

   String districtName;
    String district_id;



    @Override
    public String toString() {
        return districtName;
    }

    public ReferralModel(String districtName, String district_id) {
        this.districtName = districtName;
        this.district_id = district_id;
    }

    public String getdistrictName() {
        return districtName;
    }

    public void setdistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getdistrict_id() {
        return district_id;
    }

    public void setdistrict_id(String district_id) {
        this.district_id = district_id;
    }
}
