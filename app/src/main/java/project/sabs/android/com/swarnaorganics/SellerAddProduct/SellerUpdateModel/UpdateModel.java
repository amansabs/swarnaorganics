
package project.sabs.android.com.swarnaorganics.SellerAddProduct.SellerUpdateModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateModel {

    @SerializedName("User Added Product")
    @Expose
    private List<UserAddedProduct> userAddedProduct = null;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;

    public List<UserAddedProduct> getUserAddedProduct() {
        return userAddedProduct;
    }

    public void setUserAddedProduct(List<UserAddedProduct> userAddedProduct) {
        this.userAddedProduct = userAddedProduct;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
