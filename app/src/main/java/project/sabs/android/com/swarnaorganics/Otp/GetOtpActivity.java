package project.sabs.android.com.swarnaorganics.Otp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;

import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.goodiebag.pinview.Pinview;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import project.sabs.android.com.swarnaorganics.Login.LoginActivity;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Register.RegisterActivity;
import project.sabs.android.com.swarnaorganics.Register.SignupModel;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

public class GetOtpActivity extends AppCompatActivity implements IApiResponse{
    RelativeLayout rr_back;
    TextView text_toolbar;
    TextView opt_stillValid;
    private Pinview pinview;
    private String otp ="";
    String mobile;
    String password;
    String usertype;
    CardView card_verify;
    ProgressDialog pd;
    boolean ForgotPass = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_otp);

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        if (getIntent().getExtras() != null) {
            ForgotPass = (getIntent().getExtras().getBoolean("ForgotPass"));
        }

        text_toolbar = (TextView) findViewById(R.id.text_toolbar);
        opt_stillValid = (TextView) findViewById(R.id.opt_stillValid);
        rr_back = (RelativeLayout) findViewById(R.id.rr_back);
        pinview = (Pinview) findViewById(R.id.pinview);
        card_verify = (CardView) findViewById(R.id.card_verify);


        if (ForgotPass){

            mobile = getIntent().getExtras().getString("mobile");
            usertype = getIntent().getExtras().getString("usertype");
            String opt = getIntent().getExtras().getString("Otp");
            if (opt.equalsIgnoreCase("400")){
                opt_stillValid.setVisibility(View.VISIBLE);
            }


        }
        else {
            if( getIntent().getExtras() != null) {

                String opt = getIntent().getExtras().getString("Otp");
                mobile = getIntent().getExtras().getString("mobile");
                password = getIntent().getExtras().getString("password");
                usertype = getIntent().getExtras().getString("usertype");
                if (opt.equalsIgnoreCase("400")){
                    opt_stillValid.setVisibility(View.VISIBLE);
                }
            }
        }


        text_toolbar.setText("Otp Verification");
        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                otp=pinview.getValue();
            }
        });

        card_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Velidation();
            }
        });
    }

    private  void Velidation(){

        if (otp.equalsIgnoreCase("") || otp.length()<4){
            Toast.makeText(this, "Please enter correct otp..", Toast.LENGTH_SHORT).show();
        }

        else {


            if (ForgotPass){
                forgotOtpVerify();
                pd = new ProgressDialog(GetOtpActivity.this,R.style.AppCompatAlertDialogStyle);
                pd.setMessage("loading");
                pd.setCancelable(false);
                pd.show();
            }
            else {
                otpVerify();
                pd = new ProgressDialog(GetOtpActivity.this,R.style.AppCompatAlertDialogStyle);
                pd.setMessage("loading");
                pd.setCancelable(false);
                pd.show();

            }

        }

    }
    private void otpVerify(){

        HashMap<String, String> map = new HashMap<>();
        map.put("OTP",otp);
        map.put("mobile",mobile);
        if (!ForgotPass){
            map.put("password",password);
                    }
        map.put("usertype",usertype);
        ApiRequest apiRequest = new ApiRequest(GetOtpActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.USER_VERIFY, Constants.USER_VERIFY,map, Request.Method.POST);

    }

    private void forgotOtpVerify(){
        HashMap<String, String> map = new HashMap<>();
        map.put("OTP",otp);
        map.put("mobile",mobile);
        map.put("usertype",usertype);
        ApiRequest apiRequest = new ApiRequest(GetOtpActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.FORGOT_OTPVERIFY, Constants.FORGOT_OTPVERIFY,map, Request.Method.POST);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if(tag_json_obj.equalsIgnoreCase(Constants.USER_VERIFY)) {
            if(response != null) {

                VerifyModel finalArray = new Gson().fromJson(response,new TypeToken<VerifyModel>(){}.getType());
                int status=finalArray.getCode();
                if (status == 200){
                    pd.dismiss();
                    String msg        =finalArray.getMessage();
                    // saving value of user id
                    Toast.makeText(this, ""+msg+ msg, Toast.LENGTH_LONG).show();
                    Intent in = new Intent(GetOtpActivity.this, LoginActivity.class);
                    startActivity(in);
                    finish();
                }
                else {
                    pd.dismiss();
                    String msg=finalArray.getMessage();
                    //  Toast.makeText(this, "Something wrong..", Toast.LENGTH_SHORT).show();
                    Toast.makeText(this, " "+msg, Toast.LENGTH_LONG).show();
                }
            }
            else {
                pd.dismiss();
                Toast.makeText(this, "No internet Connection", Toast.LENGTH_LONG).show();
            }
        }
        else  if(tag_json_obj.equalsIgnoreCase(Constants.FORGOT_OTPVERIFY)) {
            if(response != null) {

                VerifyModel finalArray = new Gson().fromJson(response,new TypeToken<VerifyModel>(){}.getType());
                int status=finalArray.getCode();
                if (status == 200){

                    String msg =finalArray.getMessage();
                    pd.dismiss();
                    Toast.makeText(this, ""+msg, Toast.LENGTH_LONG).show();
                    Intent in = new Intent(GetOtpActivity.this, LoginActivity.class);
                    startActivity(in);
                    finish();

                }
                else {

                    pd.dismiss();
                    String msg=finalArray.getMessage();
                    Toast.makeText(this, " "+msg, Toast.LENGTH_LONG).show();

                }
            }
            else {

                pd.dismiss();
                Toast.makeText(this, "No internet Connection", Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(this, "Internet Error", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, ""+error, Toast.LENGTH_LONG).show();
        pd.dismiss();
    }
}
