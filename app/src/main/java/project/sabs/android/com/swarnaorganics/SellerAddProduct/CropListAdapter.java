package project.sabs.android.com.swarnaorganics.SellerAddProduct;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.BuyerAddProduct.BuyerAddProductActivity;
import project.sabs.android.com.swarnaorganics.Home.MainActivity;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Search.CropModel.CropMaster;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class CropListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<CropMaster> modelList;
    public static CropListAdapter objSearchListAdapter;
    // private ArrayList<CropListAdapter> addToCartModelList =new ArrayList<CropListAdapter>();
    String gridview;
    String userType;
    int itemQuentity;
    int count = 0;
    int quantity;

    private OnItemClickListener mItemClickListener;


    public CropListAdapter(Context context, ArrayList<CropMaster> modelList,String userType) {
        this.mContext = context;
        this.modelList = modelList;
        this.gridview = gridview;
        this.userType = userType;

    }


    public void updateList(ArrayList<CropMaster> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_select_crop_list, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final CropMaster model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;

            if (position==0){

              genericViewHolder.img_user_lap.setImageResource(R.drawable.grain_img);
              genericViewHolder.user_name_title.setText(model.getCropMasterName());

            }else if (position==1){

                genericViewHolder.img_user_lap.setImageResource(R.drawable.lentils_img);
                genericViewHolder.user_name_title.setText(model.getCropMasterName());

            }
            else if (position==2) {

                genericViewHolder.img_user_lap.setImageResource(R.drawable.oil_seed_img);
                genericViewHolder.user_name_title.setText(model.getCropMasterName());
            }
            else if(position==3){
                genericViewHolder.img_user_lap.setImageResource(R.drawable.vegitable_img);
                genericViewHolder.user_name_title.setText(model.getCropMasterName());
            }else if(position==4){

                genericViewHolder.img_user_lap.setImageResource(R.drawable.winpage_img);
                genericViewHolder.user_name_title.setText(model.getCropMasterName());

            }else if(position==5){

                genericViewHolder.img_user_lap.setImageResource(R.drawable.ayurvedic_medicine_img);
                genericViewHolder.user_name_title.setText(model.getCropMasterName());

            }else if(position==6){
                genericViewHolder.img_user_lap.setImageResource(R.drawable.gau_utpad_img);
                genericViewHolder.user_name_title.setText(model.getCropMasterName());
            }else if(position==7){

                genericViewHolder.img_user_lap.setImageResource(R.drawable.forest_img);
                genericViewHolder.user_name_title.setText(model.getCropMasterName());

            }else if(position==8){

                genericViewHolder.img_user_lap.setImageResource(R.drawable.tourism_img);
                genericViewHolder.user_name_title.setText(model.getCropMasterName());

            }
            else if(position==9){

                genericViewHolder.img_user_lap.setImageResource(R.drawable.other_img);
                genericViewHolder.user_name_title.setText(model.getCropMasterName());
            }

            setAnimation(holder.itemView, position);
            genericViewHolder.rr_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (userType.equalsIgnoreCase("2")){
                        Intent in = new Intent(mContext,BuyerAddProductActivity.class);
                        in.putExtra("CropID",model.getCropmasterId());
                        mContext.startActivity(in);

                    }
                    else if (userType.equalsIgnoreCase("1")){
                        Intent in = new Intent(mContext,AddProductActivity.class);
                        in.putExtra("CropID",model.getCropmasterId());
                        mContext.startActivity(in);
                    }
                }
            });
        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private CropMaster getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, CropListAdapter model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_user_lap;
        private TextView user_name_title;
        private RelativeLayout rr_product;

        // @BindView(R.id.img_user)
        // ImageView imgUser;
        // @BindView(R.id.item_txt_title)
        // TextView itemTxtTitle;
        // @BindView(R.id.item_txt_message)
        // TextView itemTxtMessage;
        // @BindView(R.id.radio_list)
        // RadioButton itemTxtMessage;
        // @BindView(R.id.check_list)
        // CheckBox itemCheckList;

        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.img_user_lap = (ImageView) itemView.findViewById(R.id.img_user_lap);
            this.user_name_title = (TextView) itemView.findViewById(R.id.user_name_title);
            this.rr_product = (RelativeLayout) itemView.findViewById(R.id.rr_product);



            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

}

