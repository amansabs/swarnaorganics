
package project.sabs.android.com.swarnaorganics.BuyerAddProduct.EditModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAddedProduct {

    @SerializedName("CropDetailId")
    @Expose
    private String cropDetailId;
    @SerializedName("IsOrganic")
    @Expose
    private String isOrganic;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("Unit")
    @Expose
    private String unit;
    @SerializedName("Other")
    @Expose
    private Object other;
    @SerializedName("Comments")
    @Expose
    private Object comments;

    public String getCropDetailId() {
        return cropDetailId;
    }

    public void setCropDetailId(String cropDetailId) {
        this.cropDetailId = cropDetailId;
    }

    public String getIsOrganic() {
        return isOrganic;
    }

    public void setIsOrganic(String isOrganic) {
        this.isOrganic = isOrganic;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Object getComments() {
        return comments;
    }

    public void setComments(Object comments) {
        this.comments = comments;
    }

    public Object getOther() {
        return other;
    }

    public void setOther(Object other) {
        this.other = other;
    }

}
