
package project.sabs.android.com.swarnaorganics.Search.CropModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCropModel {

    @SerializedName("Crop Master")
    @Expose
    private List<CropMaster> cropMaster = null;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;

    public List<CropMaster> getCropMaster() {
        return cropMaster;
    }

    public void setCropMaster(List<CropMaster> cropMaster) {
        this.cropMaster = cropMaster;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
