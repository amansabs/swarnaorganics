package project.sabs.android.com.swarnaorganics.BuyerAddProduct;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.BuyerAddProduct.ProductModel.BProductList;
import project.sabs.android.com.swarnaorganics.DeleteModel.DeleteProductModel;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

import static project.sabs.android.com.swarnaorganics.SellerProduct.SellerProductListActivity.productListInterface;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class BuyerProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IApiResponse {
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<BProductList> modelList;
    public static BuyerProductListAdapter objSearchListAdapter;
    // private ArrayList<BProductList> addToCartModelList =new ArrayList<BProductList>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;
    String userType;
    String userID;
    ProgressDialog pd;

    private OnItemClickListener mItemClickListener;


    public BuyerProductListAdapter(Context context, ArrayList<BProductList> modelList, String userType, String userID) {
        this.mContext = context;
        this.modelList = modelList;
        this.userType = userType;
        this.userID = userID;
        this.gridview = gridview;
        objSearchListAdapter = this;

        pd = new ProgressDialog(mContext,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);

    }

    public void updateList(ArrayList<BProductList> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_buyer_list_itme, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final BProductList model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;
            genericViewHolder.img_product.setImageResource(R.drawable.swarna_logo);

            genericViewHolder.product_name.setText(model.getCropMasterName());
            genericViewHolder.txt_ProductType.setText(model.getSubcrop());
            genericViewHolder.txt_Quentity.setText(model.getQuantity()+" "+model.getUnit());

            String  organic = model.getIsOrganic();
            if (organic.equalsIgnoreCase("1")){
                genericViewHolder.organicValue.setText("Yes");
            }
            else {
                genericViewHolder.organicValue.setText("No");
            }


            genericViewHolder.img_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(mContext, "Edit", Toast.LENGTH_SHORT).show();
                    Intent in = new Intent(mContext, BuyerAddProductActivity.class);
                    in.putExtra("isEdit",true);
                    in.putExtra("CropID",model.getCropMasterId());
                    in.putExtra("productID",model.getId());
                    mContext.startActivity(in);
                    ((Activity)mContext).finish();

                }
            });

            genericViewHolder.img_delete.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View view) {

                    String productId = model.getId();
                    int pos= position;
                    dialog(pos,productId);
                }
            });


            setAnimation(holder.itemView, position);
        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private BProductList getItem(int position) {
        return modelList.get(position);
    }



    public interface OnItemClickListener {
        void onItemClick(View view, int position, BProductList model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_product;
        private ImageView img_edit;
        private ImageView img_delete;


        private TextView product_name;
        private TextView txt_ProductType;
        private TextView txt_Quentity;
        private TextView txt_Price;
        private TextView txtPriceShow;
        private TextView organicValue;


        // @BindView(R.id.img_user)
        // ImageView imgUser;
        // @BindView(R.id.item_txt_title)
        // TextView itemTxtTitle;
        // @BindView(R.id.item_txt_message)
        // TextView itemTxtMessage;
        // @BindView(R.id.radio_list)
        // RadioButton itemTxtMessage;
        // @BindView(R.id.check_list)
        // CheckBox itemCheckList;
        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.img_product = (ImageView) itemView.findViewById(R.id.img_product);
            this.img_edit = (ImageView) itemView.findViewById(R.id.img_edit);
            this.img_delete = (ImageView) itemView.findViewById(R.id.img_delete);

            this.product_name = (TextView) itemView.findViewById(R.id.product_name);
            this.txt_ProductType = (TextView) itemView.findViewById(R.id.txt_ProductType);
            this.txt_Quentity = (TextView) itemView.findViewById(R.id.txt_Quentity);
            this.txt_Price = (TextView) itemView.findViewById(R.id.txt_Price);
            this.organicValue = (TextView) itemView.findViewById(R.id.organicValue);
///            this.txtPriceShow = (TextView) itemView.findViewById(R.id.txtPriceShow);




            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

    private void dialog(final int pos, final String productId){



        final Dialog dialog = new Dialog(mContext);
        // Include dialog.xml file
        dialog.setContentView(R.layout.delete_product_dialog);
        dialog.show();
        dialog.setCancelable(false);
        Button rr_cancel=(Button) dialog.findViewById(R.id.rr_cancel);
        Button rr_delete=(Button) dialog.findViewById(R.id.rr_delete);


        rr_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        rr_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelList.remove(modelList.get(pos));
                notifyDataSetChanged();
                dialog.cancel();
                deletePRoduct(productId);
            }
        });
    }

    private void deletePRoduct(String productId) {
        pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        map.put("usertype",userType);
        map.put("productid",productId);
        ApiRequest apiRequest = new ApiRequest(mContext,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.DELETE_PRODUCT, Constants.DELETE_PRODUCT,map, Request.Method.POST);
    }
    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equalsIgnoreCase(Constants.DELETE_PRODUCT)) {
            // State list api
            String message = "Successfully added";
            if (response != null) {
                DeleteProductModel stateArray = new Gson().fromJson(response, new TypeToken<DeleteProductModel>() {}.getType());

                int code = stateArray.getCode();
                String msg = stateArray.getMessage();
                if (code==200){
                    pd.dismiss();
                    Toast.makeText(mContext, ""+msg, Toast.LENGTH_SHORT).show();
                    productListInterface.refreshActivity();

                }
                else {
                    Toast.makeText(mContext, ""+msg, Toast.LENGTH_SHORT).show();
                }

            }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        Toast.makeText(mContext, ""+error, Toast.LENGTH_SHORT).show();
    }


}

