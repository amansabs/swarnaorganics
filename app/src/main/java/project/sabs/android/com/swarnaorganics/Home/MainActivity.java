package project.sabs.android.com.swarnaorganics.Home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;
import com.azoft.carousellayoutmanager.CenterScrollListener;
import com.google.android.material.navigation.NavigationView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import project.sabs.android.com.swarnaorganics.Aoutus.AboutusActivity;
import project.sabs.android.com.swarnaorganics.ChangePassword.ChangePasswordActivity;
import project.sabs.android.com.swarnaorganics.ContactUs.ContactusActivity;
import project.sabs.android.com.swarnaorganics.Home.CountyModel.Country;
import project.sabs.android.com.swarnaorganics.Home.CountyModel.GetCountryModel;
import project.sabs.android.com.swarnaorganics.Home.DistrictModel.District;
import project.sabs.android.com.swarnaorganics.Home.DistrictModel.GetDistrictModel;
import project.sabs.android.com.swarnaorganics.Home.ReferralModelClass.GetReferralModel;
import project.sabs.android.com.swarnaorganics.Home.ReferralModelClass.SalesUser;
import project.sabs.android.com.swarnaorganics.Home.StateModel.GetStateModel;
import project.sabs.android.com.swarnaorganics.Home.StateModel.State;
import project.sabs.android.com.swarnaorganics.Home.UserProfile.UpdateModel;
import project.sabs.android.com.swarnaorganics.Home.UserProfile.User;
import project.sabs.android.com.swarnaorganics.Home.UserProfile.UserProfileModel;
import project.sabs.android.com.swarnaorganics.Login.LoginActivity;
import project.sabs.android.com.swarnaorganics.OrganicPramotor.OrganicPramotorListActivity;
import project.sabs.android.com.swarnaorganics.Preferences;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.Rate.FeedBackActivity;
import project.sabs.android.com.swarnaorganics.Search.SearchActivity;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.SellerAddProductActivity;
import project.sabs.android.com.swarnaorganics.SellerProduct.SellerProductListActivity;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

import static project.sabs.android.com.swarnaorganics.SellerAddProduct.AddProductActivity.getOrientation;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , IApiResponse {

    private RecyclerView recyclerViewOne;
    ArrayList<BannerModel> bannerList=new ArrayList<BannerModel>();
    public CarouselLayoutManager layoutManager;
    TextView txt_heading;
    HomeSliderAdapter  mAdapterOne;
    boolean end;
    //Country
    ArrayAdapter<Country> dataAdapter;
    ArrayList<Country> country_name=new ArrayList<Country>();
    ArrayList<Country> newCountryList=new ArrayList<Country>();
    Spinner countrySpinner ;
    String countryID ="";
    Boolean IsEdit= false;
    boolean IsRef = false;
    Locale myLocale;
    //State
    Spinner stateSpinner ;
    String stateId="";
    final ArrayList<State> state_name = new ArrayList<State>();
    ArrayAdapter<State> stateDataAdapter = null;
    ArrayList<State> newStateList = new ArrayList<State>();

    //District
    Spinner districtSpinner ;
    String districtId="";
    final ArrayList<District> district_name=new ArrayList<District>();
    ArrayList<District> newDistrict_name=new ArrayList<District>();
    ArrayAdapter<District> cityDataAdapter=null;

    Spinner referral_spinner1;
    final ArrayList<SalesUser> referral_name=new ArrayList<SalesUser>();
    ArrayList<SalesUser> newReferral_name=new ArrayList<SalesUser>();
    ArrayAdapter<SalesUser> referralDataAdapter=null;
    String referralId="";

    MenuItem  nav_add_product,nav_manageProduct,nav_manageProfile,nav_logout,nav_login,nav_changePass;
    String shipId=null;
    String userType = null;
    TextView textViewUserType;
    TextView TxtMail;
    TextView txt_note;
    TextView txtUserName;
    Button cancel_btn,logout_btn,lang_cancel,lang_ok;
    CheckBox english,hindi;
    int isEnglishclick =0;
    int isHindiclick =0;
    WebView webView;
    ProgressDialog pd;


    EditText et_first_name;
    EditText et_address_name;
    EditText et__add_Two;
    EditText et_city;
    EditText et_pincode;
    EditText et_mobile;
    EditText et_email;
    String  userMobile;
    RelativeLayout save_layout;
    ArrayList<User> userProfile=new ArrayList<User>();
    ImageView edit_Profile;

    boolean Update = false;
    boolean IsGetValue = false;


    boolean isState = false;
    boolean isDistrict = false;
    boolean isRefrel = false;

    private static final int MY_CAMERA_REQUEST_CODE = 100;
    CircleImageView user_profile_image;
    CircleImageView mImageView_edit;
    CircleImageView mImageView_cancel;

    //insertImage
    //image from camera
    private Bitmap profile_bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;

    View header;
    Bitmap   bitmap;
    String Image = "";
    String USER_ID;
    String User_city= "";
    ImageView imageView;
    Menu menu;
    TextView id_companyLink;
    RecyclerView recyclerview_advantages;
    public static ArrayList<AdvantagesModel> advantagesModelsList =new ArrayList<AdvantagesModel>();
    AdvantagesListAdapter mAdapter;
    String IS_PROFILECOMPLETE = "0";
    RelativeLayout rr_About;
    RelativeLayout user_profile;
    CardView cc_pramotor;
    String currentVersion;


    Timer timer;
    public int position =0;



    RelativeLayout rr_searchProduct;
    RelativeLayout rr_AddProduct;
    RelativeLayout rr_manageProduct;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint({"SetTextI18n", "NewApi"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String language = Preferences.get(this,Preferences.LANGUAGE);
        //setLocale(language);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.setTitle(getResources().getString(R.string.app_name));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        findViews();

        String id =   Preferences.get(MainActivity.this,Constants.USER_TYPE);

        User_city = Preferences.get(this,Constants.USER_CITY);
        USER_ID = Preferences.get(this,Constants.USER_ID);
        IS_PROFILECOMPLETE= Preferences.get(MainActivity.this,Preferences.IS_PROFILECOMPLETE);


       /*String languagea = Paper.book().read("language");
        if (languagea == null)
        {
            Paper.book().write("language","hi");
            updateView((String)Paper.book().read("language"));
        }

        else if (languagea.equalsIgnoreCase("hi")){
            Paper.book().write("language","hi");
            updateView((String)Paper.book().read("language"));
        }
        else if (languagea.equalsIgnoreCase("en")){
            Paper.book().write("language","en");
            updateView((String)Paper.book().read("language"));
        }*/

        shipId= Preferences.get(MainActivity.this,Preferences.Skip_ID);

        header = navigationView.getHeaderView(0);
        textViewUserType = (TextView) header.findViewById(R.id.textViewUserType);
        TxtMail = (TextView) header.findViewById(R.id.TxtMail);
        txtUserName = (TextView) header.findViewById(R.id.txtUserName);
        imageView = (ImageView) header.findViewById(R.id.imageView);



        if(!Preferences.get(this,Constants.USER_TYPE).equalsIgnoreCase("0")) {
            userType =   Preferences.get(this,Constants.USER_TYPE);
            textViewUserType.setVisibility(View.VISIBLE);
            textViewUserType.setVisibility(View.VISIBLE);
            if (userType.equalsIgnoreCase("1")){
                textViewUserType.setText(getResources().getString(R.string.seller));
            }
            else  if (userType.equalsIgnoreCase("2")){
                textViewUserType.setText(getResources().getString(R.string.buyer));
            }
        }

        else{
            textViewUserType.setVisibility(View.VISIBLE);
            textViewUserType.setText("");
        }



        if (shipId.equalsIgnoreCase("0")){

            if (IS_PROFILECOMPLETE.equalsIgnoreCase("0")){
                showProfile ();

                cc_pramotor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {



                        if (User_city != null && !User_city.equals("") && !User_city.equals("0")){

                            Intent in = new Intent(MainActivity.this, OrganicPramotorListActivity.class);
                            in.putExtra("city",User_city);
                            startActivity(in);
                        }
                        else {
                            Toast.makeText(MainActivity.this, "Please complete your profile", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
            else {
                AboutUS();

                cc_pramotor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {



                        if (User_city != null && !User_city.equals("") && !User_city.equals(0)){

                            Intent in = new Intent(MainActivity.this, OrganicPramotorListActivity.class);
                            in.putExtra("city",User_city);
                            startActivity(in);
                        }
                        else {
                            Toast.makeText(MainActivity.this, "Please complete your profile", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

            }

        }
        else   if (shipId.equalsIgnoreCase("1")){

            AboutUS();

            cc_pramotor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finishAffinity();

                }
            });

        }
        banner();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new AutoScrollTask(), 2000, 5000);



        if(!Preferences.get(this,Preferences.KEY_USER_NAME).equalsIgnoreCase("0")) {
            txtUserName.setVisibility(View.VISIBLE);
            txtUserName.setText(Preferences.get(this, Preferences.KEY_USER_NAME));
        }else{
            txtUserName.setVisibility(View.VISIBLE);
            txtUserName.setText("");
        }
        if(!Preferences.get(this,Preferences.KEY_USER_EMAIL).equalsIgnoreCase("0")) {
            TxtMail.setVisibility(View.VISIBLE);
            TxtMail.setText(Preferences.get(this, Preferences.KEY_USER_EMAIL));
        }else{
            TxtMail.setVisibility(View.VISIBLE);
            TxtMail.setText("");
        }

        menu =navigationView.getMenu();
        nav_add_product = menu.findItem(R.id.nav_add_product);
        nav_manageProduct = menu.findItem(R.id.nav_manageProduct);
        nav_manageProfile = menu.findItem(R.id.nav_manageProfile);
        nav_changePass = menu.findItem(R.id.nav_changePass);
        nav_logout = menu.findItem(R.id.nav_logout);
        nav_login = menu.findItem(R.id.nav_login);

        nav_login.setTitle(getResources().getString(R.string.login));
        nav_logout.setTitle(getResources().getString(R.string.Log_out));
        nav_changePass.setTitle(getResources().getString(R.string.changePass));
        nav_manageProfile.setTitle(getResources().getString(R.string.manageProfile));
        nav_manageProduct.setTitle(getResources().getString(R.string.manageProduct));
        nav_add_product.setTitle(getResources().getString(R.string.addProduct));


        hideMenuItems(nav_add_product,nav_manageProduct,nav_manageProfile,nav_changePass,nav_logout,nav_login);

        String img = Preferences.get(this,Preferences.UserProfile);
        if (!img.equalsIgnoreCase("0") && !img.equalsIgnoreCase("")){

            Bitmap bitmap1 = StringToBitMap(img);
            imageView.setImageBitmap(bitmap1);
        }
        else {
            imageView.setImageResource(R.drawable.place_holder);
        }


// Creates instance of the manager.
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(MainActivity.this);

// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // For a flexible update, use AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                // Request the update.
            }
        });



        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        new GetVersionCode().execute();


        rr_searchProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this,  SearchActivity.class);
                startActivity(in);
            }
        });

        rr_AddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shipId.equalsIgnoreCase("0")){

                    if (IS_PROFILECOMPLETE.equalsIgnoreCase("0")){
                        Toast.makeText(MainActivity.this, "Please complete your profile first", Toast.LENGTH_SHORT).show();
                    }else {
                        Intent in = new Intent(MainActivity.this, SellerAddProductActivity.class);
                        in.putExtra("UserKey",userType);
                        startActivity(in);

                    }
                }
                else {

                    Intent in = new Intent(MainActivity.this,  LoginActivity.class);
                    startActivity(in);
                    finish();
                }
            }
        });
        rr_manageProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shipId.equalsIgnoreCase("0")){

                    if (IS_PROFILECOMPLETE.equalsIgnoreCase("0")){
                        Toast.makeText(MainActivity.this, "Please complete your profile first", Toast.LENGTH_SHORT).show();
                    }else {
                        Intent in = new Intent(MainActivity.this, SellerProductListActivity.class);
                        startActivity(in);
                    }
                }
                else {

                    Intent in = new Intent(MainActivity.this,  LoginActivity.class);
                    startActivity(in);
                    finish();

                }
            }
        });


    }



    public void ToastMethod( ){




    }


    @TargetApi(Build.VERSION_CODES.M)
    private void showProfile (){
        edit_Profile.setVisibility(View.VISIBLE);
        rr_About.setVisibility(View.GONE);
        user_profile.setVisibility(View.VISIBLE);
        save_layout.setVisibility(View.GONE);
        txt_heading.setText(getResources().getString(R.string.edit_profile));
        pd = new ProgressDialog(MainActivity.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage("loading");
        pd.setCancelable(false);

        if( !Preferences.get(this,Constants.USER_TYPE).equalsIgnoreCase(null)) {
            userMobile = Preferences.get(this,Preferences.KEY_MOBILE);
            et_mobile.setText(userMobile);
        }

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        }


        // getCountryListData();
        //  getStateListData();
        //  getDistrictListData();
        //MasterData();
        getUserProfileData(userMobile);
        if (IsEdit){
            //getCountryListData();

        }
        edit_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                IsEdit = false;
                IsRef = false;
                mImageView_edit.setVisibility(View.VISIBLE);
                mImageView_cancel.setVisibility(View.VISIBLE);
                referral_spinner1.setEnabled(true);
                districtSpinner.setEnabled(true);
                stateSpinner.setEnabled(true);
                countrySpinner.setEnabled(true);

                et_first_name.setEnabled(true);
                et_first_name.setClickable(true);

                et_address_name.setEnabled(true);
                et_address_name.setClickable(true);

                et__add_Two.setEnabled(true);
                et__add_Two.setClickable(true);

                et_city.setEnabled(true);
                et_city.setClickable(true);

                et_pincode.setEnabled(true);
                et_pincode.setClickable(true);

                et_email.setEnabled(true);
                et_email.setClickable(true);

                mImageView_edit.setEnabled(true);
                mImageView_edit.setClickable(true);

                mImageView_cancel.setEnabled(true);
                mImageView_cancel.setClickable(true);

                //  mImageView_edit.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.MULTIPLY);

                save_layout.setVisibility(View.VISIBLE);
                edit_Profile.setVisibility(View.GONE);

            }
        });

        save_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    /*referral_spinner1.setEnabled(false);
                    districtSpinner.setEnabled(false);
                    stateSpinner.setEnabled(false);
                    countrySpinner.setEnabled(false);

                    et_first_name.setEnabled(false);
                    et_first_name.setClickable(false);

                    et_address_name.setEnabled(false);
                    et_address_name.setClickable(false);

                    et__add_Two.setEnabled(false);
                    et__add_Two.setClickable(false);

                    et_city.setEnabled(false);
                    et_city.setClickable(false);

                    et_pincode.setEnabled(false);
                    et_pincode.setClickable(false);
                    edit_Profile.setVisibility(View.VISIBLE);*/

                Velidation();
            }
        });
        //refferalSelect();

    }

    private class AutoScrollTask extends TimerTask{
        @Override
        public void run() {
            if(position == bannerList.size()-1){
                end = true;
            } else if (position == 0) {
                end = false;
            }
            if(!end){
                position++;
            } else {
                position--;
            }
            recyclerViewOne.smoothScrollToPosition(position);
        }
    }

    private void AboutUS(){


        txt_heading.setText(getResources().getString(R.string.aboutUS));

        pd = new ProgressDialog(MainActivity.this,R.style.AppCompatAlertDialogStyle);
        pd.setMessage(getResources().getString(R.string.loading));
        pd.setCancelable(false);
        pd.show();
        edit_Profile.setVisibility(View.GONE);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_asset/" + "description_home.html");

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    //do your task
                    pd.dismiss();
                    recyclerview_advantages.setVisibility(View.VISIBLE);
                }
            }
        });
        edit_Profile.setVisibility(View.GONE);
        rr_About.setVisibility(View.VISIBLE);
        user_profile.setVisibility(View.GONE);
        save_layout.setVisibility(View.GONE);
        Advantages();

    }

    private  void Advantages(){

        advantagesModelsList.clear();
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_one));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_two));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_four));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_five));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_six));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_seven));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_eight));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_nine));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_ten));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_eleven));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_twillev));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_fourteen));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_fifteen));
        advantagesModelsList.add(new AdvantagesModel(R.drawable.ad_sixteen));


        mAdapter = new AdvantagesListAdapter(MainActivity.this, advantagesModelsList);
        recyclerview_advantages.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerview_advantages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        /// recyclerview_advantages.setLayoutManager(layoutManager);
        recyclerview_advantages.setNestedScrollingEnabled(false);

        recyclerview_advantages.setAdapter(mAdapter);
    }

    @Override
    protected void onStart() {

        super.onStart();


       /* Locale locale = new Locale("hi");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        String language = Preferences.get(this,Preferences.LANGUAGE);
        // setLocale(language);
    }

    private void findViews() {
        user_profile = (RelativeLayout) findViewById(R.id.user_profile);
        rr_searchProduct = (RelativeLayout) findViewById(R.id.rr_searchProduct);
        rr_AddProduct = (RelativeLayout) findViewById(R.id.rr_AddProduct);
        rr_manageProduct = (RelativeLayout) findViewById(R.id.rr_manageProduct);
        rr_About = (RelativeLayout) findViewById(R.id.about_layout);
        txt_heading =(TextView)findViewById(R.id.txt_heading);
        id_companyLink =(TextView)findViewById(R.id.id_companyLink);
        txt_note =(TextView)findViewById(R.id.txt_note);
        countrySpinner = (Spinner) findViewById(R.id.spinnerCountry);
        stateSpinner = (Spinner) findViewById(R.id.spinnerState);
        districtSpinner = (Spinner) findViewById(R.id.spinnerDistrict);
        referral_spinner1 = (Spinner) findViewById(R.id.referral_spinner1);
        webView = (WebView) findViewById(R.id.simpleWebView);
        edit_Profile = (ImageView) findViewById(R.id.edit_Profile);
        save_layout = (RelativeLayout) findViewById(R.id.save_layout);
        recyclerview_advantages = (RecyclerView) findViewById(R.id.recyclerview_advantages);
        cc_pramotor = (CardView) findViewById(R.id.cc_pramotor);
        et_first_name =(EditText)findViewById(R.id.et_first_name);
        et_address_name =(EditText)findViewById(R.id.et_address_name);
        et__add_Two =(EditText)findViewById(R.id.et__add_Two);
        et_city =(EditText)findViewById(R.id.et_city);
        et_pincode =(EditText)findViewById(R.id.et_pincode);
        et_mobile =(EditText)findViewById(R.id.et_mobile);
        et_email =(EditText)findViewById(R.id.et_email);
        user_profile_image =(CircleImageView)findViewById(R.id.user_profile_image);
        mImageView_edit =(CircleImageView)findViewById(R.id.mImageView_edit);
        mImageView_cancel =(CircleImageView)findViewById(R.id.mImageView_cancel);

        id_companyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.suprajaarya.com/"));
                startActivity(browserIntent);
            }
        });

        stateSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                if(countryID.equalsIgnoreCase("") || countryID.equalsIgnoreCase("0") ){
                    //getStateListData();
                    stateSpinner.setEnabled(false);
                    Toast.makeText(MainActivity.this, "Please select country first", Toast.LENGTH_SHORT).show();

                }
                return false;
            }

        });


        districtSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(countryID.equalsIgnoreCase("") || countryID.equalsIgnoreCase("0")){
                    //getStateListData();
                    districtSpinner.setEnabled(false);
                    Toast.makeText(MainActivity.this, "Please select country first", Toast.LENGTH_SHORT).show();
                }
                else if (stateId.equalsIgnoreCase("")|| stateId.equalsIgnoreCase("0"))

                {
                    districtSpinner.setEnabled(false);
                    Toast.makeText(MainActivity.this, "Please select state", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });


        mImageView_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectImage();

            }
        });

        mImageView_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bitmap = null;

                mImageView_cancel.setVisibility(View.GONE);
                user_profile_image.setImageResource(R.drawable.place_holder);


            }
        });


    }



    public void Velidation(){

        String userName = et_first_name.getText().toString();
        String userAdd = et_address_name.getText().toString();
        String userAddTwo = et__add_Two.getText().toString();
        String userCity = et_city.getText().toString();
        String userEmail = et_email.getText().toString();
        String userPinCode = et_pincode.getText().toString();

      /*  countryID
                districtId
        stateId
                referralId*/



        if (userName.equalsIgnoreCase("")){

            Toast.makeText(this, "Please Enter Name", Toast.LENGTH_SHORT).show();
        }
        else if(countryID.equalsIgnoreCase("") || countryID.equalsIgnoreCase("0")){
            //getStateListData();
            Toast.makeText(MainActivity.this, "Please Select Country First", Toast.LENGTH_SHORT).show();
        }
        else if(stateId.equalsIgnoreCase("") || stateId.equalsIgnoreCase("0")){
            //getStateListData();
            Toast.makeText(MainActivity.this, "Please Select State First", Toast.LENGTH_SHORT).show();
        }
        else if(districtId.equalsIgnoreCase("") || districtId.equalsIgnoreCase("0")){
            //getStateListData();
            Toast.makeText(MainActivity.this, "Please Select District First", Toast.LENGTH_SHORT).show();
        }
        else if (userAdd.equalsIgnoreCase("")){
            Toast.makeText(this, "Please Enter Address1", Toast.LENGTH_SHORT).show();
        }
        else if(userCity.equalsIgnoreCase("")){
            Toast.makeText(this, "Please Enter City", Toast.LENGTH_SHORT).show();
        }else if(userPinCode.length()<6){
            Toast.makeText(this, "Please Enter Correct Pincode", Toast.LENGTH_SHORT).show();
        }
        else if (!userEmail.equalsIgnoreCase("")){

            if(!isValidEmail(userEmail)){

                Toast.makeText(this, "Please Enter Correct Email", Toast.LENGTH_SHORT).show();
            }
            else if(referralId.equalsIgnoreCase("")){
                //getStateListData();
                Toast.makeText(MainActivity.this, "Please select referral", Toast.LENGTH_SHORT).show();
            }
            else {
                updateProfileData(userMobile,userName,countryID,stateId,districtId,userAdd,userCity,userPinCode,referralId,userEmail,userAddTwo);

            }

        }
        else if(referralId.equalsIgnoreCase("")){
            //getStateListData();
            Toast.makeText(MainActivity.this, "Please select referral", Toast.LENGTH_SHORT).show();
        }
        else {
            updateProfileData(userMobile,userName,countryID,stateId,districtId,userAdd,userCity,userPinCode,referralId,userEmail,userAddTwo);

        }

    }
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
    private void hideMenuItems(MenuItem nav_add_product, MenuItem nav_manageProduct, MenuItem nav_manageProfile,MenuItem nav_changePass, MenuItem nav_logout,MenuItem nav_login) {
        if(shipId.equalsIgnoreCase("0")) {
            // User logged in
            nav_add_product.setVisible(true);
            nav_logout.setVisible(true);
            nav_manageProduct.setVisible(true);
            nav_manageProfile.setVisible(true);
            nav_changePass.setVisible(true);
            nav_login.setVisible(false);
        } else if (shipId.equalsIgnoreCase("1")){
            // User not logged in
            nav_add_product.setVisible(false);
            nav_manageProduct.setVisible(false);
            nav_manageProfile.setVisible(false);
            nav_changePass.setVisible(false);
            nav_logout.setVisible(false);
            nav_login.setVisible(true);
        }

    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_language) {


            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.show();
            dialog.setContentView(R.layout.language_dialog_layout);


            lang_cancel = dialog.findViewById(R.id.lang_cancel);
            lang_ok = dialog.findViewById(R.id.lang_ok);
            english = dialog.findViewById(R.id.English_checkbox);
            hindi = dialog.findViewById(R.id.hindi_checkbox);

            cancel_btn=dialog.findViewById(R.id.cancel_button);
            logout_btn=dialog.findViewById(R.id.logout_button);
            String language  = Preferences.get(this,Preferences.LANGUAGE);

            if (language.equalsIgnoreCase("hi")){
                hindi.setChecked(true);
                isEnglishclick = 0;
                isHindiclick = 1;
            }
            else if (language.equalsIgnoreCase("en")){
                english.setChecked(true);
                isEnglishclick = 1;
                isHindiclick = 0;
            }

            lang_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            lang_ok.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (isEnglishclick == 1) {
                        //  setLocale("en");
                        Preferences.save(MainActivity.this,  Preferences.LANGUAGE, "en");
                        dialog.dismiss();
                    } else if (isHindiclick == 1) {
                        //  setLocale("hi");
                        Preferences.save(MainActivity.this, Preferences.LANGUAGE, "language");
                        dialog.dismiss();
                    }

                }
            });

            english.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (english.isChecked()) {
                        isEnglishclick = 1;
                        isHindiclick = 0;
                        english.setChecked(true);
                        hindi.setChecked(false);
                        hindi.setClickable(true);
                        english.setClickable(false);
                    }
                }
            });


            hindi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (hindi.isChecked()) {
                        isEnglishclick = 0;
                        isHindiclick = 1;
                        english.setChecked(false);
                        english.setClickable(true);
                        hindi.setClickable(false);
                        hindi.setChecked(true);

                    }
                }
            });


            return true;
        }*/

        if (id == R.id.action_contact) {

            Intent in = new Intent(MainActivity.this,  ContactusActivity.class);
            startActivity(in);
            return true;
        }
        if (id == R.id.action_about) {

            Intent in = new Intent(MainActivity.this,  AboutusActivity.class);
            startActivity(in);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void setLocale(String lang) {
        Locale  myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        Configuration config = new Configuration();
        config.locale =myLocale;

        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());

        // MainActivity.this.getResources().updateConfiguration(config,MainActivity.this.getResources().getDisplayMetrics());

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_Home) {


            AboutUS();

            referral_spinner1.setEnabled(false);
            districtSpinner.setEnabled(false);
            stateSpinner.setEnabled(false);
            countrySpinner.setEnabled(false);
            et_first_name.setEnabled(false);
            et_first_name.setClickable(false);
            et_address_name.setEnabled(false);
            et_address_name.setClickable(false);
            et__add_Two.setEnabled(false);
            et__add_Two.setClickable(false);
            et_city.setEnabled(false);
            et_city.setClickable(false);
            et_pincode.setEnabled(false);
            et_pincode.setClickable(false);

            et_email.setEnabled(false);
            et_email.setClickable(false);



            mImageView_edit.setVisibility(View.GONE);
            mImageView_edit.setEnabled(false);
            mImageView_edit.setClickable(false);

            mImageView_cancel.setVisibility(View.GONE);
            mImageView_cancel.setEnabled(false);
            mImageView_cancel.setClickable(false);

            // mImageView_edit.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.cb_dark_grey), android.graphics.PorterDuff.Mode.MULTIPLY);


            edit_Profile.setVisibility(View.GONE);
            save_layout.setVisibility(View.GONE);

            // Handle the camera action
        } else if (id == R.id.nav_Search) {

            Intent in = new Intent(MainActivity.this,  SearchActivity.class);
            startActivity(in);

        } else if (id == R.id.nav_add_product) {

            Intent in = new Intent(MainActivity.this, SellerAddProductActivity.class);
            in.putExtra("UserKey",userType);
            startActivity(in);


        } else if (id == R.id.nav_manageProduct) {
            Intent in = new Intent(MainActivity.this, SellerProductListActivity.class);
            startActivity(in);
        } else if (id == R.id.nav_manageProfile) {
            showProfile ();
            referral_spinner1.setEnabled(false);
            districtSpinner.setEnabled(false);
            stateSpinner.setEnabled(false);
            countrySpinner.setEnabled(false);
            et_first_name.setEnabled(false);
            et_first_name.setClickable(false);
            et_address_name.setEnabled(false);
            et_address_name.setClickable(false);
            et__add_Two.setEnabled(false);
            et__add_Two.setClickable(false);
            et_city.setEnabled(false);
            et_city.setClickable(false);
            et_pincode.setEnabled(false);
            et_pincode.setClickable(false);

            et_email.setEnabled(false);
            et_email.setClickable(false);

            mImageView_edit.setVisibility(View.GONE);
            mImageView_edit.setEnabled(false);
            mImageView_edit.setClickable(false);

            mImageView_cancel.setVisibility(View.GONE);
            mImageView_cancel.setEnabled(false);
            mImageView_cancel.setClickable(false);

            // mImageView_edit.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.cb_dark_grey), android.graphics.PorterDuff.Mode.MULTIPLY);


            edit_Profile.setVisibility(View.VISIBLE);
            save_layout.setVisibility(View.GONE);


        } else if (id == R.id.nav_changePass) {
            Intent in = new Intent(MainActivity.this, ChangePasswordActivity.class);
            in.putExtra("UserKey",userMobile);
            startActivity(in);

        } else if (id == R.id.nav_feedback) {
            Intent in = new Intent(MainActivity.this, FeedBackActivity.class);
            in.putExtra("UserKey",userMobile);
            startActivity(in);

        }else if (id == R.id.nav_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "प्रिय उपभोक्ता, अपने जैविक उत्पादों को बेचने और अपने आस पास के क्षेत्र में जैविक उत्पादों को सर्च करने के लिए आज ही स्वर्णा ऑर्गॅनिक्स क्रेता-विक्रेता मंच ऍप को डाउनलोड करें |  : https://play.google.com/store/apps/details?id=project.sabs.android.com.swarnaorganics");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

        } else if (id == R.id.nav_logout) {

            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.show();
            dialog.setContentView(R.layout.logout_dialog);
            cancel_btn=dialog.findViewById(R.id.cancel_button);
            logout_btn=dialog.findViewById(R.id.logout_button);
            cancel_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            logout_btn.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Preferences.clearPreference(MainActivity.this);
                    // Preferences.save(MainActivity.this,  Preferences.LANGUAGE, "hi");
                    Intent intent =new Intent(MainActivity.this,LoginActivity.class);
                    startActivity(intent);
                    dialog.dismiss();
                    finish();


                }
            });
        } else if (id == R.id.nav_login) {

            Intent in = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(in);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void banner(){

        bannerList.clear();
        bannerList.add(new BannerModel("Banner One",1,R.drawable.banner_one));
        bannerList.add(new BannerModel("Banner One",1,R.drawable.banner_two));
        bannerList.add(new BannerModel("Banner One",1,R.drawable.banner_three));
        bannerList.add(new BannerModel("Banner One",1,R.drawable.banner_four));


        recyclerViewOne = (RecyclerView)findViewById(R.id.recycler_view_one);
        recyclerViewOne.setNestedScrollingEnabled(false);
        layoutManager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, true);
        layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
        mAdapterOne = new HomeSliderAdapter(bannerList,MainActivity.this);
        recyclerViewOne.setLayoutManager(layoutManager);
        recyclerViewOne.setHasFixedSize(true);
        recyclerViewOne.setAdapter(mAdapterOne);
        recyclerViewOne.addOnScrollListener(new CenterScrollListener());



    }

    private void getCountryListData() {

        if (!IsEdit){
            // pd.show();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        ApiRequest apiRequest = new ApiRequest(MainActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_COUNTRY_LIST, Constants.GET_COUNTRY_LIST,map, Request.Method.POST);

    }

    private void getStateListData() {
        HashMap<String, String> map = new HashMap<>();
        if (!IsEdit){
            //   pd.show();
        }
        map.put("languageid","1");
        map.put("countryid",countryID);
        ApiRequest apiRequest = new ApiRequest(MainActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_State_LIST, Constants.GET_State_LIST,map, Request.Method.POST);
    }

    private void getDistrictListData() {
        //   pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        map.put("stateid",stateId);
        ApiRequest apiRequest = new ApiRequest(MainActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_DISTRICT_LIST, Constants.GET_DISTRICT_LIST, map, Request.Method.POST);
    }

    private void getReferralListData() {
        //  pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        ApiRequest apiRequest = new ApiRequest(MainActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_REFERRAL_LIST, Constants.GET_REFERRAL_LIST, map, Request.Method.POST);
    }
    private void getUserProfileData(String userMobile) {
        pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("mobile",userMobile);
        map.put("usertype",userType);
        ApiRequest apiRequest = new ApiRequest(MainActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.GET_USER_PROFILE, Constants.GET_USER_PROFILE, map, Request.Method.POST);
    }
    private void updateProfileData(String userMobile,String  userName,String countryID,String stateId,String districtId,String userAdd,String userCity,String pin,String referralId,String userEmail,String userAddTwo) {
        pd.show();
        String   img="";
        if ( bitmap !=null){
            img = getFileDataFromDrawable1(bitmap);

        }




        HashMap<String, String> map = new HashMap<>();

        map.put("mobile",userMobile);
        map.put("usertype",userType);
        map.put("Name",userName);
        map.put("Country",countryID);
        map.put("State",stateId);
        map.put("District",districtId);
        map.put("Address1",userAdd);
        map.put("Email",userEmail);
        map.put("Address2",userAddTwo);
        map.put("City",userCity);
        map.put("Pin",pin);
        map.put("Referral",referralId);
        map.put("profileimage",img);

        map.put("userid",USER_ID);



        ApiRequest apiRequest = new ApiRequest(MainActivity.this,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.UPDATE_PROFILE, Constants.UPDATE_PROFILE, map, Request.Method.POST);


    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {

        //Country
        if(tag_json_obj.equalsIgnoreCase(Constants.GET_COUNTRY_LIST)){

            if(response != null) {
                pd.dismiss();
                GetCountryModel finalArray = new Gson().fromJson(response, new TypeToken<GetCountryModel>() {}.getType());
                newCountryList.clear();
                newCountryList = (ArrayList<Country>) finalArray.getCountries();
                for (int i = 0; i <newCountryList.size() ; i++) {
                    String country_id=newCountryList.get(i).getCountryId();
                    country_name.add(new Country(""+newCountryList.get(i).getCountryId(),""+newCountryList.get(i).getCountryName() ));
                }

                dataAdapter.notifyDataSetChanged();

                if(IsEdit) {
                    for (int i = 0; i < country_name.size(); i++) {
                        String cName = country_name.get(i).getCountryId();
                        if (cName.equalsIgnoreCase(countryID)) {
                            //countryID = country_name.get(i).getId();
                            countrySpinner.setSelection(i);
                            countryID = cName;
                            // dataAdapter.notifyDataSetChanged();
                        }
                    }
                }
                countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                        Object country_item = parent.getItemAtPosition(pos);

                        if (!IsEdit){
                            countryID ="";
                            stateId="";
                            districtId ="";
                            countryID=country_name.get(pos).getCountryId();
                        }



                        if(countryID != null && !countryID.equalsIgnoreCase("0") && !countryID.equalsIgnoreCase("")){
                            state_name.clear();
                            state_name.add(new State("0", "Select"));
                            stateDataAdapter.notifyDataSetChanged();
                            getStateListData();

                            if (!IsEdit) {
                                stateSpinner.setEnabled(true);
                                district_name.clear();
                                district_name.add(new District("0", "Select"));
                                cityDataAdapter.notifyDataSetChanged();
                                getDistrictListData();
                                districtSpinner.setEnabled(true);
                            }

                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

            }
            else{
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        //State
        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_State_LIST)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                GetStateModel stateArray = new Gson().fromJson(response, new TypeToken<GetStateModel>(){}.getType());

                int code = stateArray.getCode();
                if (code == 200){
                    newStateList.clear();
                    newStateList = (ArrayList<State>) stateArray.getStates();
                    for (int i = 0; i <newStateList.size() ; i++) {
                        state_name.add(new State(""+newStateList.get(i).getStateId(),""+newStateList.get(i).getStateName()));
                    }
                    stateDataAdapter.notifyDataSetChanged();

                    if(IsEdit) {
                        for (int i = 0; i <state_name.size() ; i++) {
                            String sName = state_name.get(i).getStateId();
                            if (sName.equalsIgnoreCase(stateId)) {
                                //   stateId = state_name.get(i).getState_id();
                                stateSpinner.setSelection(i);
                            }
                        }
                    }


                    stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                            Object state_item = parent.getItemAtPosition(pos);
                            if (!IsEdit){
                                stateId = "";
                                districtId = "";
                                stateId= state_name.get(pos).getStateId();
                            }


                            if(stateId != null && !stateId.equalsIgnoreCase("0") && !stateId.equalsIgnoreCase("")) {

                                district_name.clear();
                                district_name.add(new District("0", "Select"));
                                cityDataAdapter.notifyDataSetChanged();
                                if (!IsEdit){
                                    districtSpinner.setEnabled(true);
                                }
                                getDistrictListData();


                            }
                        }
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }


            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
        //City
        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_DISTRICT_LIST)){
            // city list api
            if(response != null) {
                GetDistrictModel cityArray = new Gson().fromJson(response, new TypeToken<GetDistrictModel>(){}.getType());
                pd.dismiss();
                int code = cityArray.getCode();
                if (code == 200){

                    newDistrict_name.clear();
                    newDistrict_name = (ArrayList<District>) cityArray.getDistrict();
                    // final ArrayList<CityData> city_name=new ArrayList<CityData>();

                    //  ArrayAdapter<CityData> cityDataAdapter=null;
                    if(newDistrict_name != null) {
                        for (int i = 0; i < newDistrict_name.size(); i++) {
                            district_name.add(new District("" + newDistrict_name.get(i).getDistrictId(), "" + newDistrict_name.get(i).getDistrictName()));
                        }
                    }
                    cityDataAdapter.notifyDataSetChanged();

                    if(IsEdit) {
                        for (int i = 0; i <district_name.size() ; i++) {
                            String ctyName = district_name.get(i).getDistrictId();
                            if (ctyName.equalsIgnoreCase(districtId)) {
                                // cityId = city_name.get(i).getCity_id();
                                districtSpinner.setSelection(i);

                                cityDataAdapter.notifyDataSetChanged();


                            }
                        }
                    }
                    cityDataAdapter.notifyDataSetChanged();
                /*cityDataAdapter = new ArrayAdapter<CityData>(this,android.R.layout.simple_spinner_item, city_name);
                cityDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                citySpinner.setAdapter(cityDataAdapter);*/

                    districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                            Object id_num = parent.getItemIdAtPosition(pos);

                            if (countryID.equalsIgnoreCase("0"))
                            {
                                Toast.makeText(MainActivity.this, "please enter city", Toast.LENGTH_SHORT).show();
                            }
                            if (!IsEdit){
                                districtId= district_name.get(pos).getDistrictId();
                            }

                            // Toast.makeText(MainActivity.this, ""+districtId, Toast.LENGTH_SHORT).show();
                        }
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                }



            }else{
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
        else if(tag_json_obj.equalsIgnoreCase(Constants.GET_REFERRAL_LIST)){

            if(response != null) {
                pd.dismiss();
                GetReferralModel finalArray = new Gson().fromJson(response, new TypeToken<GetReferralModel>() {}.getType());
                newReferral_name.clear();
                newReferral_name = (ArrayList<SalesUser>) finalArray.getSalesUser();

                for (int i = 0; i <newReferral_name.size() ; i++) {
                    String country_id=newReferral_name.get(i).getSalesUserId();
                    referral_name.add(new SalesUser(""+newReferral_name.get(i).getSalesUserId(),""+newReferral_name.get(i).getSalesUserName() ));
                }


                if(IsRef) {
                    for (int i = 0; i <referral_name.size() ; i++) {
                        String cName = referral_name.get(i).getSalesUserId();
                        if (cName.equalsIgnoreCase(referralId)) {
                            //countryID = country_name.get(i).getId();
                            referral_spinner1.setSelection(i);
                        }
                    }
                }
                referralDataAdapter.notifyDataSetChanged();

                referral_spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                        Object country_item = parent.getItemAtPosition(pos);
                        if (!IsRef){
                            referralId=referral_name.get(pos).getSalesUserId();
                        }

                    }
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


            }
            else{
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }
        //State
        else if (tag_json_obj.equalsIgnoreCase(Constants.GET_USER_PROFILE)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                UserProfileModel finalArray = new Gson().fromJson(response, new TypeToken<UserProfileModel>(){}.getType());
                userProfile = (ArrayList<User>) finalArray.getUser();

                int code = finalArray.getCode();
                if (code==200){

                    IsEdit = true;
                    IsRef = true;
                    String IsProfileComplete = finalArray.getUser().get(0).getIsProfileComplete();
                    Preferences.save(MainActivity.this,Preferences.IS_PROFILECOMPLETE,IsProfileComplete);

                    String img = "";

                    if (IsProfileComplete.equalsIgnoreCase("1"))
                    {
                        et_first_name.setText(finalArray.getUser().get(0).getName());
                        et_address_name.setText(finalArray.getUser().get(0).getAddress1());
                        et__add_Two.setText(finalArray.getUser().get(0).getAddress2());
                        et_city.setText(finalArray.getUser().get(0).getCity());
                        et_pincode.setText(finalArray.getUser().get(0).getPinCode());
                        et_email.setText(finalArray.getUser().get(0).getEmail());
                        img = (String) finalArray.getUser().get(0).getPIBytearray();

                        String city = et_city.getText().toString();
                        User_city = city;
                        Preferences.save(MainActivity.this, Constants.USER_CITY, city);

                    }


                    if (finalArray.getUser().get(0).getCountry() == null || finalArray.getUser().get(0).getState() ==null ||  finalArray.getUser().get(0).getDistrict() ==null ||  finalArray.getUser().get(0).getAddress1()==null){

                        nav_manageProduct.setVisible(false);
                        nav_add_product.setVisible(false);
                        txt_note.setVisibility(View.VISIBLE);

                    }
                    else {

                        nav_manageProduct.setVisible(true);
                        nav_add_product.setVisible(true);
                        txt_note.setVisibility(View.GONE);
                    }



                    if (img!=null && !img.equalsIgnoreCase("")){
                        Bitmap bitmap1 = StringToBitMap(img);
                        Preferences.save(MainActivity.this,Preferences.UserProfile, img);
                        user_profile_image.setImageBitmap(bitmap1);
                        bitmap = bitmap1;
                        imageView.setImageBitmap(bitmap1);
                        // Picasso.with(AddProductActivity.this).load(certificate).into(img_certificate);
                    }
                    else {
                        Preferences.save(MainActivity.this,Preferences.UserProfile, "");
                        user_profile_image.setImageResource(R.drawable.place_holder);
                        imageView.setImageResource(R.drawable.place_holder);
                    }

                    et_mobile.setText(finalArray.getUser().get(0).getMobile());
                    // String country_id =
                    // stateId =
                    // districtId =  finalArray.getUser().get(0).getDistrict();
                    // et_email.setText(finalArray.getUser().get(0).get);
                    //Toast.makeText(this, "GetData", Toast.LENGTH_SHORT).show();

                    countryID =finalArray.getUser().get(0).getCountry();

                    if (countryID==null){
                        countryID="0";
                    }
                    else {

                    }
                    stateId = finalArray.getUser().get(0).getState();
                    if (stateId==null){
                        stateId="0";
                    }
                    else {

                    }
                    districtId =  finalArray.getUser().get(0).getDistrict();
                    if (districtId==null){
                        districtId="0";
                    }
                    else {

                    }
                    referralId = finalArray.getUser().get(0).getRefId();

                    if (referralId==null){
                        referralId="0";
                    }
                    else {

                    }

                    MasterData();
                    getReferralListData();

                    getCountryListData();

                    /*for (int i = 0; i <district_name.size() ; i++) {
                        String dName = district_name.get(i).getDistrictId();

                        if (dName.equalsIgnoreCase(dName)) {
                            //countryID = country_name.get(i).getId();
                            districtSpinner.setSelection(i);
                            districtId = dName;
                            cityDataAdapter.notifyDataSetChanged();

                        }

                    }*/

                }

                else {

                    Toast.makeText(this, "please edit your profile", Toast.LENGTH_SHORT).show();
                }



            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        //State
        else if (tag_json_obj.equalsIgnoreCase(Constants.UPDATE_PROFILE)){
            // State list api
            String message="Successfully added";
            if(response != null) {
                pd.dismiss();
                UpdateModel finalArray = new Gson().fromJson(response, new TypeToken<UpdateModel>(){}.getType());


                int code = finalArray.getCode();
                String msg = finalArray.getMessage();
                if (code==200){
                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();

                    IsEdit =true;
                    IsRef = true;

                    getUserProfileData(userMobile);


                    // getReferralListData();
                    // getCountryListData();
                    referral_spinner1.setEnabled(false);
                    districtSpinner.setEnabled(false);
                    stateSpinner.setEnabled(false);
                    countrySpinner.setEnabled(false);
                    et_first_name.setEnabled(false);
                    et_first_name.setClickable(false);
                    et_address_name.setEnabled(false);
                    et_address_name.setClickable(false);
                    et__add_Two.setEnabled(false);
                    et__add_Two.setClickable(false);
                    et_city.setEnabled(false);
                    et_city.setClickable(false);
                    et_pincode.setEnabled(false);
                    et_pincode.setClickable(false);

                    et_email.setEnabled(false);
                    et_email.setClickable(false);

                    mImageView_edit.setVisibility(View.GONE);
                    mImageView_edit.setEnabled(false);
                    mImageView_edit.setClickable(false);

                    mImageView_cancel.setVisibility(View.GONE);
                    mImageView_cancel.setEnabled(false);
                    mImageView_cancel.setClickable(false);

                    // mImageView_edit.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.cb_dark_grey), android.graphics.PorterDuff.Mode.MULTIPLY);


                    edit_Profile.setVisibility(View.VISIBLE);
                    save_layout.setVisibility(View.GONE);

                }

                else {

                    Toast.makeText(this, ""+msg, Toast.LENGTH_SHORT).show();
                }

            }
            else {
                Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }

        else {
            Toast.makeText(this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    private void MasterData(){
        //Country
        countrySpinner.setEnabled(true);
        country_name.clear();
        country_name.add(new Country("0","Select"));
        dataAdapter = new ArrayAdapter<Country>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,country_name);
        // drinkAdapter = new ArrayAdapter<Drink>(getActivity(), android.R.layout.simple_spinner_dropdown_item, drinks);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countrySpinner.setAdapter(dataAdapter);
        countrySpinner.setEnabled(false);

        //GetState
        state_name.clear();
        state_name.add(new State("0","Select"));
        stateDataAdapter = new ArrayAdapter<State>(this,android.R.layout.simple_spinner_item, state_name);
        stateDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stateSpinner.setAdapter(stateDataAdapter);
        stateSpinner.setEnabled(false);

        //district
        district_name.clear();
        district_name.add(new District("0", "Select"));
        cityDataAdapter = new ArrayAdapter<District>(this,android.R.layout.simple_spinner_item, district_name);
        cityDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        districtSpinner.setAdapter(cityDataAdapter);
        districtSpinner.setEnabled(false);
        //Referral
        referral_name.clear();
        referral_name.add(new SalesUser("0","Self"));
        referralDataAdapter = new ArrayAdapter<SalesUser>(this,android.R.layout.simple_spinner_item, referral_name);
        referralDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        referral_spinner1.setAdapter(referralDataAdapter);
        referral_spinner1.setEnabled(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }


    // Select image from camera and gallery
    @SuppressLint("NewApi")
    private void selectImage() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());

            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;

        if (requestCode == PICK_IMAGE_CAMERA) {


            if (data!= null){
                try {
                    Uri selectedImage = data.getData();
                    bitmap = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    // bitmap = getResizedBitmap(bitmap, 200);
                    Log.e("Activity", "Pick from Camera::>>> ");

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                    destination = new File(Environment.getExternalStorageDirectory() + "/" +
                            getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    user_profile_image.setImageBitmap(bitmap);
                    // certi_bitmap = bitmap;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
            }


        } else if (requestCode == PICK_IMAGE_GALLERY) {

            if (data!= null){

                Uri selectedImage = data.getData();
                try {
                    bitmap = scaleImage(this,selectedImage);
                    //  bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);


                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                    //  bitmap = getResizedBitmap(bitmap, 200);

                    Log.e("Activity", "Pick from Gallery::>>> ");

                    user_profile_image.setImageBitmap(bitmap);
                    //certi_bitmap = bitmap;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String getFileDataFromDrawable1(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
        byte[] imaBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imaBytes, Base64.DEFAULT);
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static Bitmap scaleImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > 512 || rotatedHeight > 512) {
            float widthRatio = ((float) rotatedWidth) / ((float) 512);
            float heightRatio = ((float) rotatedHeight) / ((float) 512);
            float maxRatio = Math.max(widthRatio, heightRatio);

            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

        /*
         * if the orientation is not 0 (or -1, which means we don't know), we
         * have to do a rotation.
         */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        }

        String type = context.getContentResolver().getType(photoUri);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (type.equals("image/png")) {
            srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        } else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
            srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }
        byte[] bMapArray = baos.toByteArray();
        baos.close();
        return BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);
    }


    public class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {

            String newVersion = null;

            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + MainActivity.this.getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }


        @Override

        protected void onPostExecute(String onlineVersion) {

            super.onPostExecute(onlineVersion);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {

                if (Float.valueOf(currentVersion ) < Float.valueOf(onlineVersion)) {

                    UpdateAvailable();

                }
                else {

                }


            }

            Log.d("update", "Current version " + "1.6" + "playstore version " + onlineVersion);

        }
    }

    private void UpdateAvailable(){

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();
        dialog.setContentView(R.layout.logout_update_dialog);
        RelativeLayout rr_cancel =dialog.findViewById(R.id.rr_cancel);
        RelativeLayout rr_updateNow =dialog.findViewById(R.id.rr_updateNow);


        rr_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        rr_updateNow.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });


    }

}
