
package project.sabs.android.com.swarnaorganics.OrganicPramotor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dealer {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("OP_Name")
    @Expose
    private String oPName;
    @SerializedName("Dealer_Name")
    @Expose
    private String dealerName;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("Pincode")
    @Expose
    private String pincode;
    @SerializedName("Email")
    @Expose
    private Object email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOPName() {
        return oPName;
    }

    public void setOPName(String oPName) {
        this.oPName = oPName;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

}
