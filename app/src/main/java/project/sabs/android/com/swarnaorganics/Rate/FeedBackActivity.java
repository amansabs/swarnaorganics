package project.sabs.android.com.swarnaorganics.Rate;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import project.sabs.android.com.swarnaorganics.R;


public class FeedBackActivity extends AppCompatActivity {
    private EditText edtEnterTitle;
    private EditText edtOrderId;
    private EditText edtEnterRemark;
    RatingBar feedbackRating;
    private Button bttnFeedback, bttnLater;
    //private AppPref mPref;
    private ImageButton imgBack;
    //List<ReferalPromocodeModel> listReff = new ArrayList<>();
    int Ratings = 1;
    public static boolean isFeedback = false;
    // new
    int IsOnOrder = 0;
    private RelativeLayout relativeLayout;
    private TextView textView3;
    private TextView txtFeeling;
    private ImageView imgEmotions;
    private LinearLayout LLRating;
    private ImageView ratingOne;
    private ImageView ratingTwo;
    private ImageView ratingThree;
    private ImageView ratingFour;
    private ImageView ratingFive;
    RadioButton rdb_cravr, rdb_lastorder;
    TextView txtViewFeedbackScreen;
    RelativeLayout rr_back;
    TextView text_toolbar;
    Button ok_btn,cancel_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        //mPref = new AppPref(FeedBackActivity.this);
        findViews();

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        // rdb_lastorder.setChecked(true);
        rdb_cravr.setChecked(true);

        bttnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String ReviewText = edtEnterRemark.getText().toString().trim();
                //String CustomerId = mPref.getStringValue(AppConstant.USER_ID);
                     if(ReviewText.equalsIgnoreCase("")){
                         Toast.makeText(FeedBackActivity.this, "Let us know how you feel", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(FeedBackActivity.this, getResources().getString(R.string.remark_validation), Toast.LENGTH_SHORT).show();
                }else {
                //callFeedbackApi(CustomerId, String.valueOf(Ratings), ReviewText, IsOnOrder);
                         Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                 "mailto","info@swarnaorganics.com", null));
                         emailIntent.putExtra(Intent.EXTRA_SUBJECT,"Feedback");
                         emailIntent.putExtra(Intent.EXTRA_TEXT, ReviewText);
                         startActivity(Intent.createChooser(emailIntent, "Send email..."));
                         finish();

                     }
            }
        });

    /*    if(TrendingFragment.isfromFragment){
            bttnLater.setVisibility(View.VISIBLE);
        }else {
            bttnLater.setVisibility(View.GONE);
        }

*/        bttnLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SplashScreen.isShowFeedback = false;
                finish();
            }
        });

    }
    @SuppressLint("WrongViewCast")
    private void findViews() {

        bttnFeedback = (Button) findViewById(R.id.bttnFeedback);
        bttnLater = (Button) findViewById(R.id.bttnLater);
        edtEnterRemark = (EditText) findViewById(R.id.edtEnterRemark);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        textView3 = (TextView) findViewById(R.id.textView3);
        txtFeeling = (TextView) findViewById(R.id.txtFeeling);
        imgEmotions = (ImageView) findViewById(R.id.imgEmotions);
        LLRating = (LinearLayout) findViewById(R.id.LL_Rating);
        ratingOne = (ImageView) findViewById(R.id.ratingOne);
        ratingTwo = (ImageView) findViewById(R.id.ratingTwo);
        ratingThree = (ImageView) findViewById(R.id.ratingThree);
        ratingFour = (ImageView) findViewById(R.id.ratingFour);
        ratingFive = (ImageView) findViewById(R.id.ratingFive);
        txtViewFeedbackScreen = (TextView) findViewById(R.id.txtViewFeedbackScreen);


        RadioGroup rdb_FeedbackType = (RadioGroup) findViewById(R.id.rdb_FeedbackType);
        rdb_cravr = (RadioButton) findViewById(R.id.rdb_cravr);
        rdb_lastorder = (RadioButton) findViewById(R.id.rdb_lastorder);

        rr_back = (RelativeLayout) findViewById(R.id.rr_back);

        text_toolbar=findViewById(R.id.text_toolbar);
        text_toolbar.setVisibility(View.VISIBLE);
        text_toolbar.setText("Feedback");
        rr_back.setVisibility(View.VISIBLE);
        rr_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final TextView txtRestaurantName = (TextView) findViewById(R.id.txtRestaurantName);
        //Utility.setGothamMedium(FeedBackActivity.this, txtRestaurantName);

        final TextView txtMsg = (TextView) findViewById(R.id.txtMsg);
        //Utility.setGothamMedium(FeedBackActivity.this, txtMsg);

        //final ImageView imgcall = (ImageView) findViewById(R.id.imgcall);

        //final TextView txtPhone = (TextView) findViewById(R.id.txtPhone);
        //Utility.setGothamMedium(FeedBackActivity.this, txtPhone);

        final TextView txtHelp = (TextView) findViewById(R.id.txtHelp);
        //Utility.setGothamMedium(FeedBackActivity.this, txtHelp);
        rdb_FeedbackType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // IsOnOrder =0 means Feedback On Cravrr
                // IsOnOrder =1 means Feedback on Order
                if (checkedId == R.id.rdb_cravr) {
                    txtRestaurantName.setVisibility(View.GONE);
                    //txtPhone.setVisibility(View.GONE);
                    //imgcall.setVisibility(View.GONE);
                    //changes by deepali
                    txtMsg.setVisibility(View.GONE);
                    txtHelp.setVisibility(View.GONE);
                    txtViewFeedbackScreen.setText("txtCravrrFeedback");
                    IsOnOrder = 0;
                } else if (checkedId == R.id.rdb_lastorder) {
                    //setDataForLastOrder(txtRestaurantName, txtMsg, imgcall, txtPhone, txtHelp);
                    txtViewFeedbackScreen.setText("txtLastOrderFeedback");
                    IsOnOrder = 1;
                }

            }
        });



       /* txtPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + txtPhone.getText().toString()));
                startActivity(intent);
            }
        });*/


     /*   if(LastOrderPhoneNumber != null && !LastOrderPhoneNumber.toString().trim().equalsIgnoreCase("")){
            txtRestaurantName.setVisibility(View.VISIBLE);
            txtPhone.setVisibility(View.VISIBLE);
            imgcall.setVisibility(View.VISIBLE);
            txtMsg.setVisibility(View.VISIBLE);

            txtRestaurantName.setText(LastOrderCompanyName);
            txtPhone.setText(LastOrderPhoneNumber);
        }else{
            txtRestaurantName.setVisibility(View.GONE);
            txtPhone.setVisibility(View.GONE);
            imgcall.setVisibility(View.GONE);
            txtMsg.setVisibility(View.GONE);
        }*/


        //TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
        //Utility.setGothamMedium(FeedBackActivity.this, txtTitle);
        //txtTitle.setText("feedbackRating");


      /*  TextView txtOrderId = (TextView) findViewById(R.id.txtOrderId);
        Utility.setGothamLight(FeedBackActivity.this, txtOrderId);
        txtOrderId.setText("LAST ORDER");*/
        /*ImageButton imgBack = (ImageButton) findViewById(R.id.btnBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SplashScreen.isShowFeedback = false;
                onBackPressed();
            }
        });*/

        ratingOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ratings = 1;
                LLRating.setBackgroundResource(R.drawable.feedback_screen_star_with_bg);
                txtFeeling.setText("Very Bad!");
                imgEmotions.setImageResource(R.drawable.feedback_screen_very_bad);
                ratingOne.setImageResource(R.drawable.feedback_screen_star);
                ratingTwo.setImageResource(R.drawable.star_trans);
                ratingThree.setImageResource(R.drawable.star_trans);
                ratingFour.setImageResource(R.drawable.star_trans);
                ratingFive.setImageResource(R.drawable.star_trans);
            }
        });

        ratingTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ratings = 2;
                LLRating.setBackgroundResource(R.drawable.feedback_screen_star_with_bg_one);
                txtFeeling.setText("Bad!");
                imgEmotions.setImageResource(R.drawable.feedback_screen_bad);
                ratingOne.setImageResource(R.drawable.star_trans);
                ratingTwo.setImageResource(R.drawable.feedback_screen_star);
                ratingThree.setImageResource(R.drawable.star_trans);
                ratingFour.setImageResource(R.drawable.star_trans);
                ratingFive.setImageResource(R.drawable.star_trans);

            }
        });
        ratingThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ratings = 3;
                LLRating.setBackgroundResource(R.drawable.feedback_screen_star_with_bg_two);
                txtFeeling.setText("Ok ok!");
                imgEmotions.setImageResource(R.drawable.feedback_screen_ok);
                ratingOne.setImageResource(R.drawable.star_trans);
                ratingTwo.setImageResource(R.drawable.star_trans);
                ratingThree.setImageResource(R.drawable.feedback_screen_star);
                ratingFour.setImageResource(R.drawable.star_trans);
                ratingFive.setImageResource(R.drawable.star_trans);
            }
        });

        ratingFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ratings = 4;
                LLRating.setBackgroundResource(R.drawable.feedback_screen_star_with_bg_four);
                txtFeeling.setText("Good!");
                imgEmotions.setImageResource(R.drawable.feedback_screen_good);
                ratingOne.setImageResource(R.drawable.star_trans);
                ratingTwo.setImageResource(R.drawable.star_trans);
                ratingThree.setImageResource(R.drawable.star_trans);
                ratingFour.setImageResource(R.drawable.feedback_screen_star);
                ratingFive.setImageResource(R.drawable.star_trans);
            }
        });

        ratingFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ratings = 5;
                LLRating.setBackgroundResource(R.drawable.feedback_screen_star_with_bg_five);
                txtFeeling.setText("Amazing!");
                imgEmotions.setImageResource(R.drawable.feedback_screen_amazing);
                ratingOne.setImageResource(R.drawable.star_trans);
                ratingTwo.setImageResource(R.drawable.star_trans);
                ratingThree.setImageResource(R.drawable.star_trans);
                ratingFour.setImageResource(R.drawable.star_trans);
                ratingFive.setImageResource(R.drawable.feedback_screen_star);
               /* final Dialog newdialog = new Dialog(FeedBackActivity.this);
                newdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                newdialog.setContentView(R.layout.rateus_dialog);
                newdialog.setCancelable(true);
                newdialog.show();*/
                final Dialog newdialog = new Dialog(FeedBackActivity.this);
                newdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                newdialog.setContentView(R.layout.rateus_dialog);
                ok_btn=newdialog.findViewById(R.id.rate_ok);
                cancel_btn=newdialog.findViewById(R.id.rate_cancel);
                newdialog.setCancelable(true);
                newdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                newdialog.show();

                ok_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=project.sabs.android.com.swarnaorganics");
                        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                        likeIng.setPackage("https://play.google.com/store/apps/details?id=project.sabs.android.com.swarnaorganics");
                        try {
                            startActivity(likeIng);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/apps/details?id=project.sabs.android.com.swarnaorganics")));
                        }
                    }
                });
                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        newdialog.dismiss();
                    }
                });

            }
        });

    }



}
