
package project.sabs.android.com.swarnaorganics.Search;

public class SubCropData {

   String SubCropName;
    String SubCrop_id;



    @Override
    public String toString() {
        return SubCropName;
    }

    public SubCropData(String SubCropName, String SubCrop_id) {
        this.SubCropName = SubCropName;
        this.SubCrop_id = SubCrop_id;
    }

    public String getSubCropName() {
        return SubCropName;
    }

    public void setSubCropName(String SubCropName) {
        this.SubCropName = SubCropName;
    }

    public String getSubCrop_id() {
        return SubCrop_id;
    }

    public void setSubCrop_id(String SubCrop_id) {
        this.SubCrop_id = SubCrop_id;
    }
}
