
package project.sabs.android.com.swarnaorganics.Search;

public class CropData {

   String cropName;
    String crop_id;



    @Override
    public String toString() {
        return cropName;
    }

    public CropData(String cropName, String crop_id) {
        this.cropName = cropName;
        this.crop_id = crop_id;
    }

    public String getcropName() {
        return cropName;
    }

    public void setcropName(String cropName) {
        this.cropName = cropName;
    }

    public String getcrop_id() {
        return crop_id;
    }

    public void setcrop_id(String crop_id) {
        this.crop_id = crop_id;
    }
}
