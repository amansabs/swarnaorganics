package project.sabs.android.com.swarnaorganics.Home;

public class BannerModel {

    String Name;
    int id;
    int Image;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }

    public BannerModel(String name, int id, int image) {
        Name = name;

        this.id = id;
        Image = image;
    }

    public BannerModel() {
    }
}
