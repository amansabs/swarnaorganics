package project.sabs.android.com.swarnaorganics.SellerProduct;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import project.sabs.android.com.swarnaorganics.BuyerAddProduct.BuyerAddProductActivity;
import project.sabs.android.com.swarnaorganics.BuyerAddProduct.ProductModel.BProductList;
import project.sabs.android.com.swarnaorganics.DeleteModel.DeleteProductModel;
import project.sabs.android.com.swarnaorganics.R;
import project.sabs.android.com.swarnaorganics.SellerAddProduct.AddProductActivity;
import project.sabs.android.com.swarnaorganics.Volley.ApiRequest;
import project.sabs.android.com.swarnaorganics.Volley.Constants;
import project.sabs.android.com.swarnaorganics.Volley.IApiResponse;

import static project.sabs.android.com.swarnaorganics.SellerProduct.SellerProductListActivity.productListInterface;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class SellerProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IApiResponse{
    private int lastPosition = -1;
    private Context mContext;
    private ArrayList<project.sabs.android.com.swarnaorganics.SellerProduct.ProductList> modelList;
    public static SellerProductListAdapter objSearchListAdapter;
    // private ArrayList<BProductList> addToCartModelList =new ArrayList<BProductList>();
    String gridview;
    int itemQuentity;
    int count = 0;
    int quantity;
    String userType;
    String userID;
    ProgressDialog pd;
    private OnItemClickListener mItemClickListener;

 public SellerProductListAdapter(Context context, ArrayList<project.sabs.android.com.swarnaorganics.SellerProduct.ProductList> modelList, String userType,String userID) {
        this.mContext = context;
        this.modelList = modelList;
        this.modelList = modelList;
        this.userType = userType;
        this.userID = userID;
        this.gridview = gridview;
        objSearchListAdapter = this;

     pd = new ProgressDialog(mContext,R.style.AppCompatAlertDialogStyle);
     pd.setMessage("loading");
     pd.setCancelable(false);
    }

    public void updateList(ArrayList<project.sabs.android.com.swarnaorganics.SellerProduct.ProductList> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_seller_list_itme, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {

            final project.sabs.android.com.swarnaorganics.SellerProduct.ProductList model = getItem(position);
            final ViewHolder genericViewHolder = (ViewHolder) holder;




            if (model.getImagePath() != null){

                //  Picasso.with(mContext).load("https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500").placeholder(R.drawable.swarna_logo);

                Picasso.with(mContext).load("https://swarna-kreta-vikreta.com/"+model.getImagePath()).placeholder(R.drawable.swarna_logo)
                        .error(R.drawable.swarna_logo)
                        .into(  genericViewHolder.img_product);

            }else {
                genericViewHolder.img_product.setImageResource(R.drawable.swarna_logo);

            }

            String OtherCrop = model.getOther();
            if ( OtherCrop != null && !OtherCrop.equalsIgnoreCase("")){
                genericViewHolder.product_name.setText(model.getOther());
            }
            else {
                genericViewHolder.product_name.setText(model.getCropMasterName());
            }

            genericViewHolder.txt_ProductType.setText(model.getSubcrop());
            genericViewHolder.txt_Quentity.setText(model.getQuantity()+" "+model.getUnit());
            genericViewHolder.txt_Price.setText(model.getUnitPrice());
            String  organic = model.getIsOrganic();
            if (organic.equalsIgnoreCase("1")){
                genericViewHolder.organicValue.setText("Yes");
            }
            else {
                genericViewHolder.organicValue.setText("No");
            }
            genericViewHolder.img_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(mContext, "Edit", Toast.LENGTH_SHORT).show();
                    Intent in = new Intent(mContext, AddProductActivity.class);
                    in.putExtra("isEdit",true);
                    in.putExtra("CropID",model.getCropMasterId());
                    in.putExtra("productID",model.getId());
                    mContext.startActivity(in);
                    ((Activity)mContext).finish();
                }
            });

            genericViewHolder.img_delete.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View view) {
                    String productId = model.getId();
                    int pos= position;
                    dialog(pos,productId);
                }
            });


            genericViewHolder.img_product.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View view) {
                    dialog( position);
                }
            });





            setAnimation(holder.itemView, position);
        }
    }
    @Override
    public int getItemCount() {
        return modelList.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private project.sabs.android.com.swarnaorganics.SellerProduct.ProductList getItem(int position) {
        return modelList.get(position);
    }



    public interface OnItemClickListener {
        void onItemClick(View view, int position, BProductList model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_product;
        private ImageView img_edit;
        private ImageView img_delete;


        private TextView product_name;
        private TextView txt_ProductType;
        private TextView txt_Quentity;
        private TextView txt_Price;
        private TextView txtPriceShow;
        private TextView organicValue;


        // @BindView(R.id.img_user)
        // ImageView imgUser;
        // @BindView(R.id.item_txt_title)
        // TextView itemTxtTitle;
        // @BindView(R.id.item_txt_message)
        // TextView itemTxtMessage;
        // @BindView(R.id.radio_list)
        // RadioButton itemTxtMessage;
        // @BindView(R.id.check_list)
        // CheckBox itemCheckList;
        public ViewHolder(final View itemView) {
            super(itemView);

            // ButterKnife.bind(this, itemView);

            this.img_product = (ImageView) itemView.findViewById(R.id.img_product);
            this.img_edit = (ImageView) itemView.findViewById(R.id.img_edit);
            this.img_delete = (ImageView) itemView.findViewById(R.id.img_delete);

            this.product_name = (TextView) itemView.findViewById(R.id.product_name);
            this.txt_ProductType = (TextView) itemView.findViewById(R.id.txt_ProductType);
            this.txt_Quentity = (TextView) itemView.findViewById(R.id.txt_Quentity);
            this.txt_Price = (TextView) itemView.findViewById(R.id.txt_Price);
            this.organicValue = (TextView) itemView.findViewById(R.id.organicValue);
///            this.txtPriceShow = (TextView) itemView.findViewById(R.id.txtPriceShow);




            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });*/

        }

    }

    private void dialog(final int pos ,final  String productId){



        final Dialog dialog = new Dialog(mContext);
        // Include dialog.xml file
        dialog.setContentView(R.layout.delete_product_dialog);
        dialog.show();
        dialog.setCancelable(false);
        Button rr_cancel =(Button) dialog.findViewById(R.id.rr_cancel);
        Button rr_delete =(Button) dialog.findViewById(R.id.rr_delete);


        rr_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        rr_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                modelList.remove(modelList.get(pos));
                notifyDataSetChanged();
                dialog.cancel();
                deletePRoduct(productId);
            }
        });
    }

    private void deletePRoduct(String productId) {
        pd.show();
        HashMap<String, String> map = new HashMap<>();
        map.put("languageid","1");
        map.put("usertype",userType);
        map.put("productid",productId);
        ApiRequest apiRequest = new ApiRequest(mContext,this);
        apiRequest.postRequest(Constants.BASE_URL + Constants.DELETE_PRODUCT, Constants.DELETE_PRODUCT,map, Request.Method.POST);
    }

    @Override
    public void onResultReceived(String response, String tag_json_obj) {
        if (tag_json_obj.equalsIgnoreCase(Constants.DELETE_PRODUCT)) {
            // State list api
            if (response != null) {
                DeleteProductModel stateArray = new Gson().fromJson(response, new TypeToken<DeleteProductModel>() {}.getType());

                int code = stateArray.getCode();
                String msg = stateArray.getMessage();
                if (code==200){
                    pd.dismiss();
                    Toast.makeText(mContext, ""+msg, Toast.LENGTH_SHORT).show();
                    productListInterface.refreshActivity();

                }
                else {
                    Toast.makeText(mContext, ""+msg, Toast.LENGTH_SHORT).show();
                }

            }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(mContext, ""+error, Toast.LENGTH_SHORT).show();
    }

    private void dialog(int pos){

        final Dialog dialog = new Dialog(mContext);
        // Include dialog.xml file
        dialog.setContentView(R.layout.image_dialog);
        dialog.show();
        dialog.setCancelable(false);
        ImageView dialog_img=(ImageView) dialog.findViewById(R.id.dialog_img);
        ImageView cancel_img=(ImageView) dialog.findViewById(R.id.cancel_img);

        Picasso.with(mContext).load("https://swarna-kreta-vikreta.com/"+modelList.get(pos).getImagePath()).placeholder(R.drawable.swarna_logo)
                .error(R.drawable.swarna_logo)
                .into(dialog_img);


        //dialog_img.setImageBitmap(StringToBitMap(modelList.get(pos).getByteArrayOfImage()));
        cancel_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
    }
}

