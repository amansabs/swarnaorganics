package project.sabs.android.com.swarnaorganics.Home;

public class CountryData {

    String Name;

    public CountryData() {
    }

    public CountryData(String name) {
        Name = name;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

}
