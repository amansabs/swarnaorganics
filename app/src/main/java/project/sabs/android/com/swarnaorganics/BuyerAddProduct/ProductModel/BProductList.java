
package project.sabs.android.com.swarnaorganics.BuyerAddProduct.ProductModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BProductList {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("CropMasterName")
    @Expose
    private String cropMasterName;
    @SerializedName("CropMasterId")
    @Expose
    private String cropMasterId;
    @SerializedName("Subcrop")
    @Expose
    private String subcrop;
    @SerializedName("Unit")
    @Expose
    private String unit;
    @SerializedName("IsOrganic")
    @Expose
    private String isOrganic;
    @SerializedName("Quantity")
    @Expose
    private String quantity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCropMasterName() {
        return cropMasterName;
    }

    public void setCropMasterName(String cropMasterName) {
        this.cropMasterName = cropMasterName;
    }

    public String getCropMasterId() {
        return cropMasterId;
    }

    public void setCropMasterId(String cropMasterId) {
        this.cropMasterId = cropMasterId;
    }

    public String getSubcrop() {
        return subcrop;
    }

    public void setSubcrop(String subcrop) {
        this.subcrop = subcrop;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIsOrganic() {
        return isOrganic;
    }

    public void setIsOrganic(String isOrganic) {
        this.isOrganic = isOrganic;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

}
